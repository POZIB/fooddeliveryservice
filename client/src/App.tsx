import { useEffect } from 'react'
import { useLocation, useNavigate } from 'react-router-dom'

import { useActions } from './hooks/useActions'
import { useTypedSelector } from './hooks/useTypedSelector'
import AppRouter from './components/AppRouter'
import Header from './components/Header/Header'
import LogoLoad from './pages/LogoLoad/index'

import './App.css'

function App() {
   const navigate = useNavigate()
   const location = useLocation()
   const { user, isAuthLoading } = useTypedSelector(state => state.auth)
   const { isTypesLoading } = useTypedSelector(state => state.types)
   const { isCategoriesLoading } = useTypedSelector(state => state.categories)
   const { isProductsLoading } = useTypedSelector(state => state.products)
   const { checkAuth, getBasket } = useActions()

   useEffect(() => {
      if (user) {
         getBasket()
      }
   }, [user])

   useEffect(() => {
      checkAuth()
   }, [])

   useEffect(() => {
      if (!user) {
         if (location.pathname.includes('/auth/password_reset')) {
            if (location.pathname.includes('/auth/password_reset/confirmed')) {
               navigate(location.pathname + location.search)
            } else {
               navigate(location.pathname)
            }
         }
      } else if (location.pathname.includes('/auth')) {
         navigate('/home')
      }
   }, [user])

   if (isAuthLoading) {
      return <LogoLoad />
   }

   return (
      <>
         <Header />
         <main>
            <AppRouter />
         </main>
      </>
   )
}

export default App
