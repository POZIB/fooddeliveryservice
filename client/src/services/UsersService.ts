import { AxiosResponse } from 'axios'
import $api from '../http'
import { IUser } from '../models/IUser'

interface IUpdateData {
   firstName: string
   lastName: string
   email: string
}

interface IUpdatePassword {
   old: string
   new: string
   confirm: string
}

export default class UserService {
   static async findUserByEmail(email: string): Promise<AxiosResponse<IUser>> {
      return await $api.get<IUser>('/find', { params: { email } })
   }

   static async updateData(payload: IUpdateData): Promise<AxiosResponse<IUser>> {
      return await $api.put<IUser>('/data', { payload })
   }

   static async updatePassword(
      payload: IUpdatePassword
   ): Promise<AxiosResponse<boolean>> {
      return await $api.put<boolean>('/password', { payload })
   }
}
