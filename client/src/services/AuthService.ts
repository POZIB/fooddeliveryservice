import { AxiosResponse } from 'axios'
import $api from '../http'
import { AuthResponse } from '../models/response/AuthResponse'

export default class AuthService {
   static async login(
      email: string,
      password: string
   ): Promise<AxiosResponse<AuthResponse>> {
      return await $api.post<AuthResponse>('/login', { email, password })
   }

   static async registration(
      email: string,
      password: string
   ): Promise<AxiosResponse<AuthResponse>> {
      return await $api.post<AuthResponse>('/registration', { email, password })
   }

   static async logout(): Promise<AxiosResponse<AuthResponse>> {
      return await $api.post<AuthResponse>('/logout')
   }

   static async refresh(): Promise<AxiosResponse<AuthResponse>> {
      return await $api.get<AuthResponse>(`/refresh`)
   }

   static async sendLinkResetPassword(email: string): Promise<AxiosResponse<boolean>> {
      return await $api.post<boolean>('/password_reset', { email })
   }

   static async resetPassword(
      hash: string,
      password: string,
      password2: string
   ): Promise<AxiosResponse<boolean>> {
      return await $api.put<boolean>('/password_reset', {
         hash,
         password,
         password2,
      })
   }
}
