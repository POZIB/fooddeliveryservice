import { IProductBasket } from '../models/IProductBasket'
import { AxiosResponse } from 'axios'
import $api from '../http'

export default class BasketService {
   static async fetchBasket(): Promise<AxiosResponse<IProductBasket[]>> {
      return await $api.get<IProductBasket[]>('/basket')
   }

   static async add(productId: number): Promise<AxiosResponse<IProductBasket>> {
      return await $api.post<IProductBasket>('/basket', { productId })
   }

   static async editQuantityProduct(
      productId: number,
      quantity: number
   ): Promise<AxiosResponse<IProductBasket>> {
      return await $api.put<IProductBasket>('/basket', {
         productId,
         quantity,
      })
   }

   static async delete(productId: number): Promise<AxiosResponse> {
      return await $api.delete('/basket', { params: { productId } })
   }

   static async clear(): Promise<AxiosResponse> {
      return await $api.post('/basket/clear')
   }
}
