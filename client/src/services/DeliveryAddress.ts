import { AxiosResponse } from 'axios'
import $api from '../http'
import { IDeliveryAddress } from '../models/IDeliveryAddress'

export default class DeliveryAddressService {
   static async fetchAddresses(): Promise<AxiosResponse<IDeliveryAddress[]>> {
      return await $api.get<IDeliveryAddress[]>('/deliveryAddress')
   }

   static async create(address: string): Promise<AxiosResponse<IDeliveryAddress>> {
      return await $api.post<IDeliveryAddress>('/deliveryAddress', {
         address,
      })
   }

   static async edit(info: IDeliveryAddress): Promise<AxiosResponse<IDeliveryAddress>> {
      return await $api.put<IDeliveryAddress>('/deliveryAddress', {
         info,
      })
   }

   static async delete(id: number): Promise<AxiosResponse> {
      return await $api.delete('/deliveryAddress', { params: { id } })
   }
}
