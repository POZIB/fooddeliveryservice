import { AxiosResponse } from 'axios'
import $api from '../http'
import { IType } from '../models/IType'

export default class TypesService {
   static async fetchTypes(): Promise<AxiosResponse<IType[]>> {
      return await $api.get<IType[]>('/types')
   }

   static async create(name: string): Promise<AxiosResponse<IType>> {
      return await $api.post<IType>('/admin/types', { name })
   }

   static async edit(id: number, newName: string): Promise<AxiosResponse<IType>> {
      return await $api.put<IType>('/admin/types', { id, newName })
   }

   static async delete(id: number): Promise<AxiosResponse> {
      return await $api.delete('/admin/types', { params: { id } })
   }
}
