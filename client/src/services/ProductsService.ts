import { IProduct } from './../models/IProduct'
import { ProductsResponse } from './../models/response/ProductsResponse'
import { AxiosResponse } from 'axios'
import $api from '../http'

export default class ProductsService {
   static async fetchProducts(
      typeId?: number | null,
      categoryId?: number | null,
      page?: number | null,
      limit?: number | null
   ): Promise<AxiosResponse<ProductsResponse>> {
      return await $api.get<ProductsResponse>('/products', {
         params: { typeId, categoryId, page, limit },
      })
   }

   static async rating(productId: number, rate: number): Promise<AxiosResponse<number>> {
      return await $api.post<number>('/products/rating', {
         productId,
         rate,
      })
   }

   static async create(product: FormData): Promise<AxiosResponse<IProduct>> {
      return await $api.post<IProduct>('/admin/products', product)
   }

   static async edit(product: FormData): Promise<AxiosResponse<IProduct>> {
      return await $api.put<IProduct>('/admin/products', product)
   }

   static async delete(id: number): Promise<AxiosResponse> {
      return await $api.delete('/admin/products', { params: { id } })
   }
}
