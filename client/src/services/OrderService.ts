import { OrdersResponse } from './../models/response/OrdersResponse'
import { IOrder } from './../models/IOrder'
import { AxiosResponse } from 'axios'
import $api from '../http'

export default class OrderService {
   static async fetchOrders(
      page: number,
      limit: number
   ): Promise<AxiosResponse<OrdersResponse>> {
      return $api.get<OrdersResponse>('/orders', { params: { page, limit } })
   }

   static async createOrder(order: IOrder): Promise<AxiosResponse> {
      return $api.post('/orders', { order })
   }
}
