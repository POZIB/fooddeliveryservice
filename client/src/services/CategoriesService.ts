import { ICategory } from './../models/ICategory'
import { AxiosResponse } from 'axios'
import $api from '../http'

export default class CategoriesService {
   static async fetchCategories(typeId?: number): Promise<AxiosResponse<ICategory[]>> {
      return await $api.get<ICategory[]>('/categories', { params: { typeId } })
   }

   static async create(name: string, typeId: number): Promise<AxiosResponse<ICategory>> {
      return await $api.post<ICategory>('/admin/categories', {
         name,
         typeId,
      })
   }

   static async edit(newCategory: ICategory): Promise<AxiosResponse<ICategory>> {
      return await $api.put<ICategory>('/admin/categories', {
         newCategory,
      })
   }

   static async delete(id: number): Promise<AxiosResponse> {
      return await $api.delete('/admin/categories', { params: { id } })
   }
}
