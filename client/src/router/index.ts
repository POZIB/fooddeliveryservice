import Home from '../pages/Home'
import Account from '../pages/User/Account'
import OrdersUser from '../pages/User/OrdersUser/OrdersUser'
import Statistics from '../Area/Statistics/pages/Statistics'
import Users from '../Area/Admin/pages/Users'
import Roles from '../Area/Admin/pages/Roles'
import Types from '../Area/Admin/pages/Types'
import Categories from '../Area/Admin/pages/Categories'
import Products from '../Area/Admin/pages/Products'
import Order from '../pages/BasketAndOrder/index'
import OrdersManager from '../Area/Manager/pages/Orders'
import OrdersCourier from '../Area/Courier/pages/Orders'
import OrderCourier from '../Area/Courier/pages/AcceptOrder'
import OrderComplete from '../pages/BasketAndOrder/OrderComplete'

export interface IRoute {
   path: string
   element: React.ComponentType
}

export enum RouteNames {
   REDIRECT = '*',
   HOME = '/',
   BASKET = '/basket',
   ACCOUNT_USER = '/account',
   ORDERS_USER = '/orders',
   ORDER_COMPLETE = '/order/complete',
}

export enum AdminRouteNames {
   USERS = '/admin/users',
   ROLES = '/admin/roles',
   TYPES = '/admin/types',
   CATEGORIES = '/admin/categories',
   PRODUCTS = '/admin/products',
   STATISTICS = '/admin/statistics',
}

export enum ManagerRouteNames {
   ORDERS = '/manager/orders',
   STATISTICS = '/manager/statistics',
}

export enum CourierRouteNames {
   ORDERS = '/courier/orders',
   ORDER = '/courier/acceptOrder',
}

export const publicRoutes: IRoute[] = [
   { path: RouteNames.REDIRECT, element: Home },
   { path: RouteNames.BASKET, element: Order },
]

export const privateRoutes: IRoute[] = [
   { path: RouteNames.REDIRECT, element: Home },
   { path: RouteNames.ACCOUNT_USER, element: Account },
   { path: RouteNames.ORDERS_USER, element: OrdersUser },
   { path: RouteNames.BASKET, element: Order },
   { path: RouteNames.ORDER_COMPLETE, element: OrderComplete },
]

export const privateAdminRoutes: IRoute[] = [
   { path: AdminRouteNames.USERS, element: Users },
   { path: AdminRouteNames.ROLES, element: Roles },
   { path: AdminRouteNames.TYPES, element: Types },
   { path: AdminRouteNames.CATEGORIES, element: Categories },
   { path: AdminRouteNames.PRODUCTS, element: Products },
   { path: AdminRouteNames.STATISTICS, element: Statistics },
]

export const privateManagerRoutes: IRoute[] = [
   { path: ManagerRouteNames.ORDERS, element: OrdersManager },
   { path: ManagerRouteNames.STATISTICS, element: Statistics },
]

export const privateCourierRoutes: IRoute[] = [
   { path: CourierRouteNames.ORDERS, element: OrdersCourier },
   { path: CourierRouteNames.ORDER, element: OrderCourier },
]
