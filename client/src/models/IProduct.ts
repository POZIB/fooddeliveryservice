export interface IProduct {
   id: number
   name: string
   price: number
   img: File
   description: string
   composition: string
   weight: string
   rating: number
   typeId: number
   categoryId: number
}
