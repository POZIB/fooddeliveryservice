import { IRole } from './IRole'

export interface IUser {
   id: number
   firstName: string
   lastName: string
   email: string
   roles: IRole[]
   isActivated: boolean
}
