import { IProduct } from './IProduct'

export interface IProductBasket extends IProduct {
   quantity: number
}
