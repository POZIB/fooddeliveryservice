import { IOrder } from './../IOrder'

export interface OrdersResponse {
   rows: IOrder[]
   count: number
}
