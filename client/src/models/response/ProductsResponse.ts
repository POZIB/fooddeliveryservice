import { IProduct } from './../IProduct'
export interface ProductsResponse {
   rows: IProduct[]
   count: number
}
