import { IUser } from './../IUser'

export interface UsersResponse {
   rows: IUser[]
   count: number
}
