export interface IDeliveryAddress {
   [index: string]: number | string | boolean | null | undefined
   id: number
   address: string
   telephoneNumber?: string
   apartment?: string
   storey?: string
   entrance?: string
   intercom: boolean
   comments?: string
   apartmentOrHouse: string
}
