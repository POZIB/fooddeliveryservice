export interface IDeliveryTime {
   immediatelyOrTime: string
   date?: string
   time?: string
}
