export interface ICategory {
   id: number
   name: string
   typeId: number
}
