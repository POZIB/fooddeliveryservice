import { IProduct } from './IProduct'

export interface IOrderProduct extends IProduct {
   quantity?: number
}

export interface IOrder {
   id?: number
   userId?: number
   emailUser?: string
   date?: string
   total?: number
   status?: string
   quantityCutlery: number
   paymentMethod?: string
   timeDelivery: string
   telephone?: string
   addressDelivery?: string
   whereDelivery?: string
   intercom?: string
   comments?: string
   products?: IOrderProduct[]
   courierName?: string
}
