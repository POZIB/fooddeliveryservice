import { EyeInvisibleOutlined, EyeOutlined, CloseCircleOutlined } from '@ant-design/icons'
import React, { FC, useState } from 'react'

import './myInput.css'

interface IProps {
   label?: string
   width?: string
   type?: 'text' | 'number' | 'password' | 'email'
   disabled?: boolean
   value?: any
   allowClear?: boolean
   onChange?: (e: React.ChangeEvent<HTMLInputElement> | string) => void
}

const MyInput: FC<IProps> = props => {
   const [focus, setFocus] = useState<boolean>(false)
   const [isDirty, setIsDirty] = useState<boolean>(false)
   const [isShowPassword, setIsShowPassword] = useState(false)

   function handleChange(e: React.ChangeEvent<HTMLInputElement>) {
      if (props.onChange) props.onChange(e)
      setIsDirty(!!e.target.value)
   }

   function handleClear() {
      setIsDirty(false)
      props.onChange && props.onChange('')
   }

   return (
      <div className='fieldRoot' style={{ width: props.width }}>
         <label className={`inputLabel ${focus || isDirty ? 'focused' : ''}`}>
            {props.label}
         </label>
         <div className={`wrapperInput ${focus || isDirty ? 'focused' : ''}`}>
            <input
               value={props.value}
               disabled={props.disabled}
               type={isShowPassword ? 'text' : props.type}
               onChange={handleChange}
               onFocus={() => !props.disabled && setFocus(true)}
               onBlur={() => !props.disabled && setFocus(false)}
            />
            {props.allowClear && isDirty && (
               <CloseCircleOutlined className='iconAllowClear' onClick={handleClear} />
            )}
            {props.type == 'password' &&
               (isShowPassword ? (
                  <EyeOutlined
                     className='iconEye'
                     onClick={() => setIsShowPassword(false)}
                  />
               ) : (
                  <EyeInvisibleOutlined
                     className='iconEye'
                     onClick={() => setIsShowPassword(true)}
                  />
               ))}
         </div>
      </div>
   )
}

MyInput.defaultProps = {
   width: '100%',
   value: '',
}

export default MyInput
