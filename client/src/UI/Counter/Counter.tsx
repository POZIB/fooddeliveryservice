import React, { FC, useState } from 'react'
import './style.css'

interface IProps {
   quantity: number
   min?: number
   max?: number
   onDecrement: () => void
   onIncrement: () => void
}

const Counter: FC<IProps> = ({
   max = 100,
   min = 1,
   quantity,
   onDecrement,
   onIncrement,
}) => {
   const Decrement = () => {
      if (quantity != min) {
         onDecrement()
      }
   }

   const Increment = () => {
      if (quantity < max) {
         onIncrement()
      }
   }

   return (
      <div className='wrapper-counter'>
         <div
            className={`counter-decrement ${quantity <= min && 'disabled'}`}
            onClick={Decrement}
         ></div>
         <div className='counter-value'>{quantity}</div>
         <div
            className={`counter-increment ${quantity >= max && 'disabled'}`}
            onClick={Increment}
         ></div>
      </div>
   )
}

export default Counter
