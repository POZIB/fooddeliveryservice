import React, { FC, useMemo } from 'react'

import './Toggler.css'

interface IProps {
   id?: string
   name?: string
   defaultValue?: any
   value?: any
   onChange?: (value: string) => void
   children?: any
}

const Toggler: FC<IProps> = props => {
   function renderChildren() {
      return React.Children.map(props.children, child => {
         return React.cloneElement(child, {
            name: props.name,
            onChange: props.onChange,
            checked: props.value == child.props.value,
            disabled: child.props.disabled,
         })
      })
   }

   return (
      <div className='wrapperToggler'>
         <div className='containerToggler'>{renderChildren()}</div>
      </div>
   )
}

export default Toggler
