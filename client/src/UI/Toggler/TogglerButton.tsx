import React, { FC } from 'react'

interface IProps {
   value?: any
   onChange?: (value: any) => void
   checked?: boolean
   disabled?: boolean
}

const TogglerButton: FC<IProps> = props => {
   function handleChange() {
      props.onChange && props.onChange(props.value)
   }

   return (
      <label className={`togglerButton ${props.checked ? 'checked' : ''}`}>
         <input
            type='radio'
            value={props.value}
            checked={props.checked && !props.disabled}
            disabled={props.disabled}
            onChange={() => !props.disabled && handleChange()}
         />
         <span>{props.children}</span>
      </label>
   )
}

TogglerButton.defaultProps = {
   value: '',
   checked: false,
}

export default TogglerButton
