import React, { FC, useState } from 'react'

import './menuBurger.css'

const MenuBurger: FC = () => {
   const [active, setActive] = useState(false)

   return (
      <div
         className={`burgerMenu ${active ? 'active' : ''}`}
         onClick={() => setActive(!active)}
      >
         <span></span>
      </div>
   )
}

export default MenuBurger
