import { InboxOutlined, DeleteOutlined } from '@ant-design/icons'
import React, { FC, useState } from 'react'

import style from './Uploader.module.css'

interface IProps {
   multiple?: boolean
   value?: File[]
   onChange?: (files: File[]) => void
}

const Uploader: FC<IProps> = props => {
   const [drag, setDrag] = useState(false)
   const [defaultFiles, setDefaultFiles] = useState<File[] | []>(props.value || [])
   const [newFileList, setNewFileList] = useState<File[] | []>([])

   function dragStartHandler(e: React.DragEvent<HTMLLabelElement>) {
      e.preventDefault()
      setDrag(true)
   }

   function dragLeaveHandler(e: React.DragEvent<HTMLLabelElement>) {
      e.preventDefault()
      setDrag(false)
   }

   function onDropHandler(e: React.DragEvent<HTMLLabelElement>) {
      e.preventDefault()
      let files = Array.from(e.dataTransfer.files)

      if (props.multiple) {
         setNewFileList(files)
      } else {
         files = files.slice(-1)
         setNewFileList(files)
      }
      setDrag(false)
      setDefaultFiles([])
      props.onChange && props.onChange(files)
   }

   function onSelectFileHandler(e: React.ChangeEvent<HTMLInputElement>) {
      e.preventDefault()
      if (e.currentTarget.files) {
         const files = Array.from(e.currentTarget.files)
         setNewFileList(files)
         props.onChange && props.onChange(files)
      }
      setDefaultFiles([])
   }

   function onFilterNewFileList(file: File) {
      const files = newFileList?.filter((element: File) => element.name !== file.name)
      if (files) {
         setNewFileList(files)
         props.onChange && props.onChange(files)
      } else {
         props.onChange && props.onChange([])
      }
   }

   function onFilterDefaultFiles(file: File) {
      const files = props.value?.filter(element => element !== file)
      if (files) {
         setDefaultFiles(files)
         props.onChange && props.onChange(files)
      } else {
         props.onChange && props.onChange([])
      }
   }

   return (
      <div className={style.wrapperUploader}>
         <label
            className={`${style.dropArea} ${drag && style.active} `}
            onDragStart={e => dragStartHandler(e)}
            onDragEnter={e => dragStartHandler(e)}
            onDragLeave={e => dragLeaveHandler(e)}
            onDragOver={e => dragStartHandler(e)}
            onDrop={e => onDropHandler(e)}
         >
            <input
               type='file'
               multiple={props.multiple}
               onChange={e => onSelectFileHandler(e)}
            />
            <p className={style.uploadIcon}>
               <InboxOutlined />
            </p>
            <p>
               {drag
                  ? 'Отпустите файл'
                  : 'Нажмите или перетащите файл в эту область, чтобы загрузить'}
            </p>
         </label>
         <div className={style.containerFiles}>
            {defaultFiles?.map(file => (
               <div key={String(file)} className={style.itemFile}>
                  <div className={style.wrapperImg}>
                     <img
                        src={String(process.env.REACT_APP_API_URL) + file}
                        alt={file.name}
                     />
                  </div>
                  <DeleteOutlined onClick={() => onFilterDefaultFiles(file)} />
               </div>
            ))}
            {newFileList?.map((file: File) => (
               <div key={file.name} className={style.itemFile}>
                  <div className={style.wrapperImg}>
                     <img src={URL.createObjectURL(file)} alt={file.name} />
                  </div>
                  <DeleteOutlined onClick={() => onFilterNewFileList(file)} />
               </div>
            ))}
         </div>
      </div>
   )
}

Uploader.defaultProps = {
   value: [],
   multiple: false,
}

export default Uploader
