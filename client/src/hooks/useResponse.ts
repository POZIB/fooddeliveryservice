import { useState, useEffect } from 'react'
import { AxiosError, AxiosResponse } from 'axios'

type returnType<T> = [
   T | null,
   boolean,
   string[] | string | null,
   (request: () => Promise<AxiosResponse<T, any>>) => Promise<T>
]
type requestType<T> = (() => Promise<AxiosResponse<T, any>>) | null

export default function <T>(request: requestType<T>): returnType<T> {
   const [data, setData] = useState<T | null>(null)
   const [loading, setLoading] = useState<boolean>(false)
   const [error, setError] = useState<string[] | string | null>(null)

   async function setFetch(request: () => Promise<AxiosResponse<T, any>>): Promise<T> {
      setLoading(true)
      const response = new Promise<T>((resolve, reject) => {
         request()
            .then((response: AxiosResponse<T, any>) => {
               setData(response.data)
               setError(null)
            })
            .catch((error: AxiosError) => {
               setError(error.response?.data.message)
            })
            .finally(() => setLoading(false))
      })

      return response
   }

   useEffect(() => {
      if (request) {
         setFetch(request)
      }
   }, [])

   return [data, loading, error, setFetch]
}
