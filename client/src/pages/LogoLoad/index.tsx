import React from 'react'
import style from './logoStyle.module.css'

import logo from '../../assets/logo.png'

const LogoLoad = () => {
   return (
      <div className={style.wrapper}>
         <div className={style.container}>
            <div className={style.wrapperLogo}>
               <img src={logo} alt='' />
            </div>
            <div className={style.wrapperName}>
               <h1>Faster'S</h1>
            </div>
         </div>
      </div>
   )
}

export default LogoLoad
