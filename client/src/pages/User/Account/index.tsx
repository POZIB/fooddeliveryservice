import React, { FC } from 'react'
import FormData from '../../../components/Account/FormData'
import FormPassword from '../../../components/Account/FormPassword'

import style from './style.module.css'

const Account: FC = () => {
   return (
      <div className={style.wrapper}>
         <h1>Ваши данные</h1>
         <div className={style.content}>
            <div className={style.containerForm}>
               <h2>Сменна данных</h2>
               <FormData />
            </div>
            <div className={style.containerForm}>
               <h2>Сменна пароля</h2>
               <FormPassword />
            </div>
         </div>
      </div>
   )
}

export default Account
