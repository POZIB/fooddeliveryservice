import { Pagination, Select, Spin } from 'antd'
import { LoadingOutlined } from '@ant-design/icons'
import { CSSTransition } from 'react-transition-group'
import moment from 'moment'
import React, { FC, useEffect, useState } from 'react'
import { useTypedSelector } from '../../../hooks/useTypedSelector'
import { IOrder, IOrderProduct } from '../../../models/IOrder'
import OrderService from '../../../services/OrderService'

import style from './ordersStyle.module.css'
import useResponse from '../../../hooks/useResponse'
import { OrdersResponse } from '../../../models/response/OrdersResponse'
import LogoLoad from '../../LogoLoad'

const OrdersUser: FC = () => {
   const [page, setPage] = useState(1)
   const [limit, setLimit] = useState(10)
   const [totalCount, setTotalCount] = useState(0)
   const [orders, loading, error, setOrders] = useResponse<OrdersResponse>(null)

   const { user } = useTypedSelector(state => state.auth)

   useEffect(() => {
      if (user) {
         setOrders(() => OrderService.fetchOrders(page, limit))
      }
   }, [user, page, limit])

   const onSelect = (e: number) => {
      setLimit(e)
      setPage(1)
   }

   if (loading) return <LogoLoad />

   return (
      <div className={style.wrapper}>
         <div className={style.container}>
            <div className={style.top}>
               <h1>Ваши заказы</h1>
               <div className={style.containerSelectLimit}>
                  <p>Заказов на странице</p>
                  <Select
                     className={style.selectLimit}
                     defaultValue={limit}
                     onChange={e => onSelect(e)}
                  >
                     <Select.Option value={10}>10</Select.Option>
                     <Select.Option value={25}>25</Select.Option>
                     <Select.Option value={50}>50</Select.Option>
                     <Select.Option value={-1}>Все</Select.Option>
                  </Select>
               </div>
            </div>
            <div className={style.content}>
               <ul>
                  {orders?.rows.map(order => (
                     <li key={order.id} className={style.cardOrder}>
                        <div className={style.containerInfoOrder}>
                           <div className={style.containerInfoOrderTop}>
                              <div className={style.numberOrder}>
                                 Заказ №{order.id}{' '}
                                 {moment(order.date).format('DD.MM.Y,HH:mm')}
                              </div>
                              <div
                                 className={
                                    order.status == 'В обработке'
                                       ? style.statusInProcessing
                                       : style.statusAccess
                                 }
                              >
                                 {order.status}
                              </div>
                           </div>

                           <div>
                              Сумма заказа: <strong> {order.total}₽</strong>
                              {/* , Оплата {order.paymentMethod} */}
                           </div>

                           <div>
                              Время доставки: <strong> {order.timeDelivery}</strong>
                           </div>
                           <div>
                              Тел: <strong>{order.telephone}</strong>
                           </div>
                           <div>Улица: {order.addressDelivery}</div>
                           <div>Доставка {order.whereDelivery}</div>
                           <div>{order.intercom}</div>
                           {order.comments && <div>Комментарий: {order.comments}</div>}
                        </div>
                        <СontentProduct products={order?.products} />
                     </li>
                  ))}
               </ul>
            </div>
         </div>
         <div className={style.containerBottom}>
            <Pagination
               current={page}
               onChange={e => setPage(e)}
               pageSize={limit}
               total={totalCount}
            />
         </div>
      </div>
   )
}
export default OrdersUser

interface IProps {
   products?: IOrderProduct[]
}

const СontentProduct: FC<IProps> = props => {
   const [isShow, setIsShow] = useState(false)
   const products = props.products

   return (
      <div className={style.rootProducts}>
         <div className={style.containerBtn}>
            <button
               className={`${style.btn} ${isShow ? style.active : ''}`}
               onClick={() => setIsShow(!isShow)}
            >
               {isShow ? 'Скрыть товары' : 'Показать товары'}
            </button>
         </div>
         <CSSTransition
            in={isShow}
            timeout={500}
            classNames={{
               enterActive: style.containerProductsEnterActive,
               enterDone: style.containerProductsEnterDone,
               exitActive: style.containerProductsExitActive,
               exitDone: style.containerProductsExitDone,
            }}
            mountOnEnter
            unmountOnExit
         >
            <ul className={style.containerProducts}>
               {products?.map(product => (
                  <li key={product.id} className={style.itemProduct}>
                     <div className={style.containerImg}>
                        <img
                           src={String(process.env.REACT_APP_API_URL) + product.img}
                           alt={product.name}
                        />
                     </div>
                     <div>{product.quantity}шт.</div>
                     <div>
                        <strong>{product.name}</strong>
                     </div>
                     <div className={style.price}>
                        {product.quantity} X <strong>{product.price}₽</strong>
                     </div>
                  </li>
               ))}
            </ul>
         </CSSTransition>
      </div>
   )
}
