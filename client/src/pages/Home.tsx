import { FC, useEffect, useRef, useState } from 'react'

import { useTypedSelector } from '../hooks/useTypedSelector'
import { useActions } from '../hooks/useActions'
import { IType } from '../models/IType'
import ContainerProductsByType from '../components/Home/ContainerProductsByType'
import Slider from '../components/Home/Slider'
import LogoLoad from './LogoLoad'

const Home: FC = () => {
   const { getProducts, getTypes, getCategories } = useActions()
   const { types, isTypesLoading } = useTypedSelector(state => state.types)
   const { isProductsLoading } = useTypedSelector(state => state.products)
   const { page, limit } = useTypedSelector(state => state.products)
   const { selectType } = useTypedSelector(state => state.types)
   const { selectCategory } = useTypedSelector(state => state.categories)
   const [selectRefType, setSelectRefType] = useState<IType | undefined>(undefined)

   const ref = useRef<HTMLDivElement>(null)

   useEffect(() => {
      getTypes()
      getCategories()
   }, [])

   useEffect(() => {
      getProducts(selectType?.id, selectCategory?.id, page, limit)
   }, [page, limit])

   useEffect(() => {
      if (ref && ref.current) {
         const elementPosition = ref.current.getBoundingClientRect().top
         const offsetPosition = elementPosition + window.pageYOffset - 105
         window.scrollTo({
            top: offsetPosition,
            behavior: 'smooth',
         })
      }
   }, [selectRefType])

   if (isProductsLoading || isTypesLoading) {
      return <LogoLoad />
   }

   return (
      <>
         <div className='wrapperHome'>
            <div className='topSelectorTypes'>
               {types?.map(type => (
                  <a
                     key={type.id}
                     className={`itemSelectorTypes ${
                        selectRefType?.id === type.id ? 'active' : ''
                     }`}
                     onClick={() => setSelectRefType(type)}
                  >
                     {type.name}
                  </a>
               ))}
            </div>
            <div className='slider'>
               <Slider />
            </div>
            <div className='catalog'>
               {types?.map(type => (
                  <div
                     key={type.id}
                     ref={selectRefType === type ? ref : undefined}
                     id={type.name}
                  >
                     <ContainerProductsByType type={type} />
                  </div>
               ))}
            </div>
         </div>
      </>
   )
}

export default Home
