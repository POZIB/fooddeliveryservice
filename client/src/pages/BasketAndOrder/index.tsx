import React, { FC, useEffect, useState } from 'react'
import { CSSTransition, SwitchTransition } from 'react-transition-group'
import { useNavigate } from 'react-router-dom'
import { Button } from 'antd'

import OrderService from '../../services/OrderService'
import { useTypedSelector } from '../../hooks/useTypedSelector'
import { useActions } from '../../hooks/useActions'
import { IOrder } from '../../models/IOrder'

import Basket from '../../components/Basket/Basket'
import Delivery from '../../components/Delivery/Delivery'
import TableDelivery from '../../components/Delivery/TableDelivery/TableDelivery'
import PayOrder from '../../components/PayOrder/PayOrder'
import loginIn from '../../assets/loginIn.png'

import style from './orderStyle.module.css'

const Order: FC = () => {
   const navigate = useNavigate()
   const { user, isAuthLoading } = useTypedSelector(state => state.auth)
   const { productsInBasket } = useTypedSelector(state => state.basket)
   const { selectDeliveryAddress } = useTypedSelector(state => state.deliveryAddress)
   const { deliveryTime } = useTypedSelector(state => state.deliveryTime)
   const { quantityCutlery } = useTypedSelector(state => state.cutlery)
   const { clearBasket } = useActions()

   const [isPay, setIsPay] = useState(false)

   useEffect(() => {
      if (deliveryTime) setIsPay(true)
   }, [deliveryTime])

   const completeTheOrder = async () => {
      if (!deliveryTime || !selectDeliveryAddress || !productsInBasket.length) return

      const order: IOrder = {
         quantityCutlery,
         paymentMethod: 'онлайн картой',
         timeDelivery: `${deliveryTime?.date} к ${deliveryTime?.time}`,
         telephone: selectDeliveryAddress.telephoneNumber,
         addressDelivery: selectDeliveryAddress.address,
         whereDelivery:
            selectDeliveryAddress.apartmentOrHouse === 'Apartment'
               ? `в квартиру №${selectDeliveryAddress.apartment}, Подъезд ${selectDeliveryAddress.entrance}, Этаж ${selectDeliveryAddress.storey}`
               : 'в частный дом',
         intercom:
            selectDeliveryAddress.apartmentOrHouse === 'Apartment'
               ? selectDeliveryAddress.intercom
                  ? 'Домофон работает'
                  : 'Домофон не работает'
               : '',
         comments: selectDeliveryAddress.comments,
      }

      await OrderService.createOrder(order)
      clearBasket()
      navigate('/order/complete')
   }

   if (!user && !isAuthLoading) {
      return (
         <div className={style.rootOrder}>
            <div className={style.containerBasket}>
               <div className={style.containerLoginIn}>
                  <div className={style.title}>
                     <h1>Авторизуйтесь!</h1>
                  </div>
                  <div className={style.containerButtonLogin}>
                     <Button onClick={() => navigate('/auth/login')}>
                        <span>Войти</span>
                     </Button>
                  </div>
                  <div className={style.wrapperImg}>
                     <img src={loginIn} alt='' />
                  </div>
               </div>
            </div>
         </div>
      )
   }

   return (
      <div className={style.rootOrder}>
         <div className={style.wrapperOrder}>
            <div className={style.containerBasket}>
               <Basket />
            </div>

            <SwitchTransition mode='out-in'>
               <CSSTransition
                  key={isPay ? 'Pay' : 'Delivery'}
                  timeout={500}
                  classNames={{
                     enterActive: style.payOrDeliveryEnterAtive,
                     exitActive: style.payOrDeliveryExitActive,
                  }}
                  mountOnEnter
                  unmountOnExit
               >
                  <div className={style.containerPayOrDelivery}>
                     {isPay ? (
                        <>
                           <TableDelivery goBack={setIsPay} />
                           <PayOrder completeTheOrder={completeTheOrder} />
                        </>
                     ) : (
                        <>
                           <Delivery />
                        </>
                     )}
                  </div>
               </CSSTransition>
            </SwitchTransition>
         </div>
      </div>
   )
}

export default Order
