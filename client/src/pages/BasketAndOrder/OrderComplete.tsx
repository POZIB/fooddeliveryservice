import { Button, Result } from 'antd'
import React, { FC } from 'react'
import { useNavigate } from 'react-router-dom'

const OrderComplete: FC = () => {
   const navigate = useNavigate()

   return (
      <Result
         status='success'
         title='Ваш заказ успешно оформлен!'
         subTitle='Вы можете отслеживать статус заказа в вашем личном кибинете'
         extra={[
            <Button key='buy' onClick={() => navigate('/')}>
               На домашнюю страницу
            </Button>,
            <Button key='console' onClick={() => navigate('/orders')}>
               В личный кабинет
            </Button>,
         ]}
      />
   )
}

export default OrderComplete
