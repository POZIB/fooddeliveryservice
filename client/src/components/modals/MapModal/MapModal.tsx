import React, { FC, useCallback, useEffect, useState } from 'react'
import { Button, Input } from 'antd'
import Modal from 'antd/lib/modal/Modal'
import { useJsApiLoader } from '@react-google-maps/api'

import { getBrowserLocation } from '../../../utils/geo'
import { useTypedSelector } from '../../../hooks/useTypedSelector'
import { useActions } from '../../../hooks/useActions'
import PlacesAutocomplete from '../../PlacesAutocomplete/PlacesAutocomplete'
import Map from '../../Map/Map'
import { IDeliveryAddress } from '../../../models/IDeliveryAddress'

import map from '../../../assets/map.png'
import style from './style.module.css'

interface IProps {
   visible: boolean
   onHide: (value: React.SetStateAction<boolean>) => void
}

interface ICenter {
   lat: number
   lng: number
}

const defaultCenter: ICenter = {
   lat: 55.783,
   lng: 49.125,
}

const libraries: any = ['places']

const MapModal: FC<IProps> = props => {
   const [center, setCenter] = useState<ICenter>(defaultCenter)
   const [address, setAddress] = useState<string>('')

   const { user } = useTypedSelector(state => state.auth)
   const { isDeliveryAddressLoading } = useTypedSelector(state => state.deliveryAddress)
   const { createDeliveryAddress, setSelectDeliveryAddress } = useActions()

   const { isLoaded } = useJsApiLoader({
      id: 'google-map-script-1',
      googleMapsApiKey: String(process.env.GOOGLE_MAP_API_KEY),
      libraries,
   })

   const onPlaceSelect = useCallback(coordinates => {
      setCenter(coordinates)
   }, [])

   const determineLocation = () => {
      getBrowserLocation()
         .then(currentLocation => {
            setCenter(currentLocation as ICenter)
         })
         .catch(defaultLocation => {
            setCenter(defaultLocation)
         })
   }

   const onDragMarker = (e: google.maps.MapMouseEvent) => {
      if (e.latLng?.lat() && e.latLng?.lng()) {
         setCenter({ lat: e.latLng?.lat(), lng: e.latLng?.lng() })
      }
   }

   const onClickMap = (e: google.maps.MapMouseEvent) => {
      if (e.latLng?.lat() && e.latLng?.lng())
         setCenter({ lat: e.latLng?.lat(), lng: e.latLng?.lng() })
   }

   const onSave = () => {
      if (user) createDeliveryAddress(address)
      {
         setSelectDeliveryAddress({ address } as IDeliveryAddress)
         props.onHide(false)
      }
   }

   return (
      <Modal
         footer={false}
         visible={props.visible}
         onCancel={() => props.onHide(false)}
         style={{ minWidth: '800px', minHeight: '600px' }}
      >
         <h2 className={style.title}>Улица доставки</h2>
         <div className={style.bar}>
            <Button onClick={determineLocation}>Определить</Button>
            {isLoaded ? (
               <PlacesAutocomplete
                  isLoaded={isLoaded}
                  onSelectAddress={onPlaceSelect}
                  coordAtMarker={center}
                  valueAddress={setAddress}
               />
            ) : (
               <Input
                  value={address}
                  onChange={e => setAddress(e.target.value)}
                  placeholder='Введите адрес'
               />
            )}
            <Button onClick={onSave} loading={isDeliveryAddressLoading}>
               Сохранить
            </Button>
         </div>

         <div className={style.containerMap}>
            {isLoaded ? (
               <Map center={center} onClick={onClickMap} onDragMarker={onDragMarker} />
            ) : (
               <div className={style.contanerNoMap}>
                  <div className={style.title}>
                     <h3>Упс.. Карта по техническим причинам не загрузилась</h3>
                  </div>
                  <div className={style.wrapperImgMap}>
                     <img src={map} alt='' />
                  </div>
               </div>
            )}
         </div>
      </Modal>
   )
}

export default React.memo(MapModal)
