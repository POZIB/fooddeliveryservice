import { Modal, Row } from 'antd'
import React, { FC } from 'react'

interface IProps {
   title?: string
   loading?: boolean
   maskClosable?: boolean
   isOpen: boolean
   onClose: () => void
   onSubmit: () => void
}

const ConfirmModal: FC<IProps> = ({
   title,
   loading,
   isOpen,
   maskClosable = true,
   onClose,
   onSubmit,
   children,
}) => {
   React.useEffect(() => {
      if (!loading) onClose()
   }, [loading])

   return (
      <Modal
         title={title}
         visible={isOpen}
         onCancel={onClose}
         onOk={onSubmit}
         closable={false}
         okText='Да'
         cancelText='Нет'
         destroyOnClose
         maskClosable={maskClosable}
         okButtonProps={{ loading }}
      >
         <Row
            align='middle'
            justify='center'
            style={{
               height: '100px',
            }}
         >
            {children}
         </Row>
      </Modal>
   )
}

export default ConfirmModal
