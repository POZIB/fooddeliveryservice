import { Modal } from 'antd'
import React, { FC } from 'react'

interface IProps {
   title?: string
   loading?: boolean
   isOpen: boolean
   onClose: () => void
   onSubmit: () => void
}

const MainModal: FC<IProps> = ({
   title,
   loading,
   isOpen,
   onClose,
   onSubmit,
   children,
}) => {
   React.useEffect(() => {
      if (!loading) onClose()
   }, [loading])

   return (
      <Modal
         title={title}
         visible={isOpen}
         onCancel={onClose}
         onOk={onSubmit}
         okText='Да'
         cancelText='Отмена'
         okButtonProps={{ loading }}
         destroyOnClose
      >
         {children}
      </Modal>
   )
}

export default MainModal
