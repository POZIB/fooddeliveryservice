import { Modal } from 'antd'
import React, { FC } from 'react'
interface IProps {
   title?: string
   loading?: boolean
   centered?: boolean
   isOpen: boolean
   onClose: () => void
}
const FormModal: FC<IProps> = ({
   title,
   isOpen,
   centered = false,
   onClose,
   loading,
   children,
}) => {
   React.useEffect(() => {
      if (!loading) onClose()
   }, [loading])

   return (
      <Modal
         centered={centered}
         title={title}
         visible={isOpen}
         onCancel={onClose}
         footer={false}
         destroyOnClose
      >
         {children}
      </Modal>
   )
}

export default FormModal
