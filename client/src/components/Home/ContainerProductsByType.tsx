import { FC, useState, useEffect } from 'react'
import { Divider } from 'antd'
import { CloseOutlined } from '@ant-design/icons'

import { useTypedSelector } from '../../hooks/useTypedSelector'
import { ICategory } from '../../models/ICategory'
import { IType } from '../../models/IType'
import Product from '../Product/Product'

interface IProps {
   type: IType
}

const ContainerProductsByType: FC<IProps> = ({ type }) => {
   const [selectCategory, setSelectCategory] = useState<ICategory | null>(null)
   const { categories } = useTypedSelector(state => state.categories)
   const { products } = useTypedSelector(state => state.products)

   return (
      <div>
         <div className='titleType'>
            <h1>{type.name}</h1>
         </div>
         <div className='sectionSelectorCategories'>
            {selectCategory && (
               <div
                  className={`itemSelectorCategories`}
                  onClick={() => setSelectCategory(null)}
               >
                  <CloseOutlined />
               </div>
            )}
            {categories
               .filter(category => category.typeId == type.id)
               .map(category => (
                  <div
                     key={category.id}
                     className={`itemSelectorCategories ${
                        category.id == selectCategory?.id ? 'active' : ''
                     }`}
                     onClick={() => setSelectCategory(category)}
                  >
                     {category.name}
                  </div>
               ))}
         </div>
         <div>
            <div className='containerProducts'>
               {products
                  ?.filter(product =>
                     selectCategory
                        ? product.categoryId == selectCategory?.id
                        : product.typeId == type.id
                  )
                  .map(product => (
                     <Product product={product} key={product.id} />
                  ))}
            </div>
         </div>

         <Divider />
      </div>
   )
}

export default ContainerProductsByType
