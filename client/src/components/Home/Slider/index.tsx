import React, { useEffect, useState } from 'react'
import { Carousel, Rate } from 'antd'
import { useTypedSelector } from '../../../hooks/useTypedSelector'

import style from './index.module.css'

const Slider = () => {
   const { products } = useTypedSelector(state => state.products)
   const [bestProducts, setBestProducts] = useState(
      products.filter(item => item.rating > 3.5)
   )

   return (
      <div className={style.wrapper}>
         <div className={style.container}>
            <div className={style.title}>Лучшее предложение недели!</div>
            <Carousel autoplay>
               {bestProducts.map((item, index) => (
                  <div key={item.id}>
                     <div className={style.containerImages}>
                        <div className={style.containerProduct}>
                           <div className={style.wrapperImg}>
                              <img
                                 src={
                                    String(process.env.REACT_APP_API_URL) +
                                    bestProducts[index].img
                                 }
                                 alt=''
                              />
                           </div>
                           <div className={style.containerDescription}>
                              <span className={style.name}>
                                 {bestProducts[index].name}
                              </span>
                              <div className={style.containerRate}>
                                 <Rate
                                    disabled
                                    value={bestProducts[index].rating}
                                    style={{ fontSize: '16px' }}
                                 />
                                 <span className={style.price}>
                                    {bestProducts[index].price}₽
                                 </span>
                              </div>
                           </div>
                        </div>

                        {bestProducts[index + 1] && (
                           <div className={style.containerProduct}>
                              <div className={style.wrapperImg}>
                                 <img
                                    src={
                                       String(process.env.REACT_APP_API_URL) +
                                       bestProducts[index + 1].img
                                    }
                                    alt=''
                                 />
                              </div>
                              <div className={style.containerDescription}>
                                 <span className={style.name}>
                                    {bestProducts[index + 1].name}
                                 </span>
                                 <div className={style.containerRate}>
                                    <Rate
                                       disabled
                                       value={bestProducts[index + 1].rating}
                                       style={{ fontSize: '16px' }}
                                    />
                                    <span className={style.price}>
                                       {bestProducts[index + 1].price}₽
                                    </span>
                                 </div>
                              </div>
                           </div>
                        )}
                     </div>
                  </div>
               ))}
            </Carousel>
         </div>
      </div>
   )
}

export default Slider
