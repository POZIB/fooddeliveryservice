import React, { FC, useCallback, useRef, useState } from 'react'
import { GoogleMap, Marker } from '@react-google-maps/api'

const containerStyle = {
   with: '800px',
   height: '450px',
}

interface IProps {
   center: google.maps.LatLng | google.maps.LatLngLiteral
   onDragMarker: (e: google.maps.MapMouseEvent) => void
   onClick: (e: google.maps.MapMouseEvent) => void
}

const defaultOptions = {
   panControl: true,
   zoomControl: true,
   mapTypeControl: false,
   scaleControl: false,
   streetViewControl: false,
   rotateControl: false,
   clickableIcons: false,
   keyboardShortcuts: false,
   scrollwheel: true,
   disableDoubleClickZoom: false,
   fullscreenControl: false,
}

const Map: FC<IProps> = ({ center, onDragMarker, onClick }) => {
   const mapRef = useRef(undefined)

   const onLoad = useCallback(map => {
      const bounds = new window.google.maps.LatLngBounds()
      mapRef.current = map
   }, [])

   const onUnmount = useCallback(map => {
      mapRef.current = undefined
   }, [])

   return (
      <>
         <GoogleMap
            mapContainerStyle={containerStyle}
            center={center}
            zoom={12.5}
            options={defaultOptions}
            onLoad={onLoad}
            onUnmount={onUnmount}
            onClick={onClick}
         >
            <Marker position={center} onDragEnd={onDragMarker} draggable={true} />
         </GoogleMap>
      </>
   )
}

export default Map
