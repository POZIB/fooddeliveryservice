import { FC, useEffect } from 'react'
import usePlacesAutocomplete, { getGeocode, getLatLng } from 'use-places-autocomplete'
import useOnclickOutside from 'react-cool-onclickoutside'
import style from './style.module.css'

interface IProps {
   isLoaded: boolean
   valueAddress: (value: React.SetStateAction<string>) => void
   onSelectAddress: (value: { lat: number; lng: number }) => void
   coordAtMarker: { lat: number; lng: number }
}

const defaultCenter = {
   lat: 55.783,
   lng: 49.125,
}

const defaultBounds = {
   north: defaultCenter.lat + 0.15,
   south: defaultCenter.lat - 0.15,
   east: defaultCenter.lng + 0.15,
   west: defaultCenter.lng - 0.15,
}

const PlacesAutoСomplete: FC<IProps> = ({
   isLoaded,
   valueAddress,
   onSelectAddress,
   coordAtMarker,
}) => {
   const {
      ready,
      value,
      suggestions: { status, data },
      setValue,
      init,
      clearSuggestions,
   } = usePlacesAutocomplete({
      requestOptions: {
         bounds: defaultBounds,
         location: new google.maps.LatLng(55.783, 49.125),
         radius: 10000,
         componentRestrictions: { country: 'ru' },
         types: ['address'],
      },
      debounce: 300,
   })

   const ref = useOnclickOutside(() => {
      clearSuggestions()
   })

   const handleInput = (e: React.ChangeEvent<HTMLInputElement>) => {
      setValue(e.target.value)
   }

   const handleSelect =
      ({ description }: google.maps.places.AutocompletePrediction) =>
      () => {
         clearSuggestions()

         getGeocode({
            address: description,
            componentRestrictions: { country: 'ru' },
            location: new google.maps.LatLng(55.783, 49.125),
            bounds: defaultBounds,
         })
            .then(results => getLatLng(results[0]))
            .then(({ lat, lng }) => {
               onSelectAddress({ lat, lng })
            })
            .catch(error => {
               console.log('😱 Error: ', error)
            })
      }

   const renderSuggestions = () =>
      data.map(suggestion => {
         const {
            place_id,
            structured_formatting: { main_text, secondary_text },
         } = suggestion

         return (
            <li key={place_id} onClick={handleSelect(suggestion)}>
               <strong>{main_text}</strong> <small>{secondary_text}</small>
            </li>
         )
      })

   useEffect(() => {
      getGeocode({ location: coordAtMarker }).then(result => {
         setValue(
            result[1].address_components[1].long_name +
               ', ' +
               result[1].address_components[0].long_name,
            false
         )
      })
   }, [coordAtMarker])

   useEffect(() => {
      valueAddress(value)
   }, [value])

   useEffect(() => {
      if (isLoaded) init()
   }, [isLoaded, init])

   return (
      <div ref={ref} className={style.container}>
         <input
            className={style.input}
            value={value}
            onChange={handleInput}
            disabled={!ready}
            placeholder='Введите адрес'
         />
         {status === 'OK' && <ul>{renderSuggestions()}</ul>}
      </div>
   )
}

export default PlacesAutoСomplete
