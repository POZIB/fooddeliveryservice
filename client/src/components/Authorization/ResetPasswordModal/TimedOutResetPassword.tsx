import React from 'react'
import { Button } from 'antd'
import { useNavigate } from 'react-router-dom'

import style from '../auth.module.css'

const TimedOutResetPassword = () => {
   const navigate = useNavigate()

   return (
      <div className={style.wrapperAuth}>
         <h2 className={style.top}> Время сброса пароля истекло. Повторите попытку</h2>
         <div className={style.containerTimedOut}>
            <Button onClick={() => navigate('/auth/password_reset')}>
               Повторить попытку
            </Button>
            <Button onClick={() => navigate('/home')}> Закрыть</Button>
         </div>
      </div>
   )
}

export default TimedOutResetPassword
