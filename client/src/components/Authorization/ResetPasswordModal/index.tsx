import React, { useState } from 'react'
import { Button, Form, Modal } from 'antd'
import { useNavigate, useSearchParams } from 'react-router-dom'

import FormResetPassword from './FormResetPassword'
import CompleteResetPassword from './CompleteResetPassword'
import TimedOutResetPassword from './TimedOutResetPassword'

const maskStyle: React.CSSProperties = {
   backgroundColor: 'rgba(55,55,55,.85)',
   backdropFilter: 'blur(4px)',
   transform: 'translate3d(0,0,0)',
}

const ResetPasswordModal = () => {
   const navigate = useNavigate()
   const [searchParams] = useSearchParams()
   const [isComplete, setIsComplete] = useState(false)

   const handleClose = () => {
      navigate('/home')
   }

   return (
      <Modal
         className='myModal'
         visible={true}
         onCancel={handleClose}
         footer={false}
         maskStyle={maskStyle}
         centered
         destroyOnClose
      >
         <div className='wrapperAuth'>
            {isComplete ? (
               <CompleteResetPassword />
            ) : (
               <>
                  {searchParams.get('hash') !== 'expired' ? (
                     <FormResetPassword onComplete={() => setIsComplete(true)} />
                  ) : (
                     <TimedOutResetPassword />
                  )}
               </>
            )}
         </div>
      </Modal>
   )
}

export default ResetPasswordModal
