import React, { useState } from 'react'
import { Button, Form, Modal } from 'antd'
import { useSearchParams } from 'react-router-dom'
import { AxiosError } from 'axios'

import AuthService from '../../../services/AuthService'
import MyInput from '../../../UI/MyInput/MyInput'
import { rules } from '../../../utils/rules'

import style from '../auth.module.css'

interface IProps {
   onComplete: () => void
}

const FormResetPassword: React.FC<IProps> = ({ onComplete }) => {
   const [searchParams] = useSearchParams()
   const [loading, setLoading] = useState(false)
   const [error, setError] = useState('')

   const onSubmit = async (data: { password: string; password2: string }) => {
      try {
         const hash = searchParams.get('hash')
         if (!hash) return

         if (hash) {
            setLoading(true)
            const res = await AuthService.resetPassword(
               hash,
               data.password,
               data.password2
            )
            if (res.data) {
               onComplete()
            }
         }
      } catch (error: Error | unknown) {
         const err = error as AxiosError<{ message: string }>
         setError(err.response?.data.message || '')
      } finally {
         setLoading(false)
      }
   }

   return (
      <div className={style.wrapperAuth}>
         <h2 className={style.top}>Сброс пароля</h2>
         <span className={style.subTitle}>
            Надежные пароли содержат цифры, буквы и знаки препинания.
         </span>
         <Form
            className={style.form}
            initialValues={{ remember: true }}
            onFinish={onSubmit}
         >
            <Form.Item
               className={style.row}
               name='password'
               rules={[
                  rules.required('Введите новый пароль'),
                  { message: 'Неверный формат' },
               ]}
            >
               <MyInput label='Новый пароль' allowClear type='password' />
            </Form.Item>
            <Form.Item
               className={style.row}
               name='password2'
               rules={[
                  rules.required('Повторите новый пароль'),
                  { message: 'Неверный формат' },
                  ({ getFieldValue }) => ({
                     validator(_, value) {
                        if (!value || getFieldValue('password') === value) {
                           return Promise.resolve()
                        }
                        return Promise.reject(new Error('Пароли не совпадают!'))
                     },
                  }),
               ]}
            >
               <MyInput label='Повторите новый пароль' allowClear type='password' />
            </Form.Item>
            <div className={style.errors}>{error}</div>
            <div className={style.containerButton}>
               <Button htmlType='submit' loading={loading}>
                  Сбросить
               </Button>
            </div>
         </Form>
      </div>
   )
}

export default FormResetPassword
