import React from 'react'
import { Button } from 'antd'
import { useNavigate } from 'react-router-dom'

import completeImg from '../../../assets/complete.png'

import style from '../auth.module.css'

const CompleteResetPassword = () => {
   const navigate = useNavigate()

   return (
      <div className={style.wrapperAuth}>
         <h2 className={style.top}> Пароль испешно изменен!</h2>
         <span className={style.subTitle}>
            Вы успешно сменили пароль. Войдите в аккаунт.
         </span>
         <div className={style.containerCompleteReset}>
            <div className={style.containerImg}>
               <img src={completeImg} alt='' />
            </div>

            <Button onClick={() => navigate('/auth/login')}> Войти</Button>
         </div>
      </div>
   )
}

export default CompleteResetPassword
