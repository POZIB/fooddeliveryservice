import React, { useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import { Button, Form, Modal } from 'antd'

import { useActions } from '../../hooks/useActions'
import { useTypedSelector } from '../../hooks/useTypedSelector'
import MyInput from '../../UI/MyInput/MyInput'
import { rules } from '../../utils/rules'

import style from './auth.module.css'

const maskStyle: React.CSSProperties = {
   backgroundColor: 'rgba(55,55,55,.85)',
   backdropFilter: 'blur(4px)',
   transform: 'translate3d(0,0,0)',
}

const RegisterModal = () => {
   const navigate = useNavigate()
   const { registration, setAuthError } = useActions()
   const { isAuthLoading, authError, user } = useTypedSelector(state => state.auth)

   useEffect(() => {
      if (user) {
         navigate('/home')
      }
   }, [user])

   const onSubmit = (values: any) => {
      if (values) {
         registration(values.email, values.password)
      }
   }
   return (
      <Modal
         className='myModal'
         visible={true}
         onCancel={() => navigate(-1)}
         footer={false}
         maskStyle={maskStyle}
         centered
         destroyOnClose
         afterClose={() => setAuthError('')}
      >
         <div className='wrapperAuth'>
            <div className={style.wrapperAuth}>
               <h2 className={style.top}>Регистрация</h2>
               <Form
                  className={style.form}
                  initialValues={{ remember: true }}
                  onFinish={onSubmit}
               >
                  <Form.Item
                     className={style.row}
                     name='email'
                     rules={[
                        rules.required('Введите email'),
                        { type: 'email', message: 'Неверный формат' },
                     ]}
                  >
                     <MyInput label='Email' allowClear />
                  </Form.Item>

                  <Form.Item
                     className={style.row}
                     name='password'
                     rules={[
                        rules.required('Введите пароль'),
                        { min: 6, message: 'Минимальная длинна пароля 6 символов' },
                     ]}
                  >
                     <MyInput label='Пароль' type='password' allowClear />
                  </Form.Item>
                  <div className={style.errors}>{authError}</div>
                  <div className={style.containerButton}>
                     <Button htmlType='submit' loading={isAuthLoading}>
                        Зарегистроваться
                     </Button>
                  </div>
                  <div className={style.bottom}>
                     <a onClick={() => navigate('/auth/login')}>Войти</a>
                     <a onClick={() => navigate('/auth/password_reset')}>
                        Забыли пароль?
                     </a>
                  </div>
               </Form>
            </div>
         </div>
      </Modal>
   )
}

export default RegisterModal
