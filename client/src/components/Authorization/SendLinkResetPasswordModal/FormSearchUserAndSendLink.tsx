import { Button, Form } from 'antd'
import React, { useState } from 'react'
import AuthService from '../../../services/AuthService'
import MyInput from '../../../UI/MyInput/MyInput'
import { rules } from '../../../utils/rules'

import style from '../auth.module.css'

interface IProps {
   onFound: () => void
}

const FormSearchUserAndSendLink: React.FC<IProps> = ({ onFound }) => {
   const [loading, setLoading] = useState(false)
   const [error, setError] = useState('')

   const onSubmit = async (data: { email: string }) => {
      try {
         setLoading(true)
         const res = await AuthService.sendLinkResetPassword(data.email)
         if (res.data) {
            onFound()
         } else {
            setError('Мы не смогли найти вашу учетную запись.')
         }
      } catch (error) {
         setError('Что то пошло не так. Повторите попытку.')
      } finally {
         setLoading(false)
      }
   }

   return (
      <div className={style.wrapperAuth}>
         <h2 className={style.top}>Найдите свою учетную запись</h2>
         <Form
            className={style.form}
            initialValues={{ remember: true }}
            onFinish={onSubmit}
         >
            <Form.Item
               className={style.row}
               name='email'
               rules={[
                  rules.required('Введите email'),
                  { type: 'email', message: 'Неверный формат' },
               ]}
            >
               <MyInput label='Email' allowClear />
            </Form.Item>
            <div className={style.errors}>{error}</div>
            <div className={style.containerButton}>
               <Button htmlType='submit' loading={loading}>
                  Найти
               </Button>
            </div>
         </Form>
      </div>
   )
}

export default FormSearchUserAndSendLink
