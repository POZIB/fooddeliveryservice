import React from 'react'
import { useNavigate } from 'react-router-dom'
import { Modal } from 'antd'

import CompleteSendLink from './CompleteSendLink'
import FormSearchUserAndSendLink from './FormSearchUserAndSendLink'

const maskStyle: React.CSSProperties = {
   backgroundColor: 'rgba(55,55,55,.85)',
   backdropFilter: 'blur(4px)',
   transform: 'translate3d(0,0,0)',
}

const SendLinkResetPasswordModal = () => {
   const navigate = useNavigate()
   const [isFound, setIsFound] = React.useState(false)

   const handleClose = () => {
      navigate('/home')
   }

   return (
      <Modal
         className='myModal'
         visible={true}
         onCancel={handleClose}
         footer={false}
         maskStyle={maskStyle}
         centered
         destroyOnClose
      >
         {isFound ? (
            <CompleteSendLink />
         ) : (
            <FormSearchUserAndSendLink onFound={() => setIsFound(true)} />
         )}
      </Modal>
   )
}

export default SendLinkResetPasswordModal
