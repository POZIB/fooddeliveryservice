import React from 'react'
import { useNavigate } from 'react-router-dom'
import { Button } from 'antd'

import style from '../auth.module.css'

const CompleteSendLink = () => {
   const naviage = useNavigate()

   return (
      <div className={style.wrapperAuth}>
         <h2 className={style.top}>Проверьте электронную почту</h2>
         <div className={style.bodyCompleteSendLink}>
            <div>Вы получите ссылку, перейдя по которой, вы сможете сбросить пароль.</div>
            <div>
               <Button onClick={() => naviage('/home')}>Закрыть</Button>
            </div>
         </div>
      </div>
   )
}

export default CompleteSendLink
