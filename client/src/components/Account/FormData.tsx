import React, { useState } from 'react'
import { Button, Form, Input, message } from 'antd'

import { useTypedSelector } from '../../hooks/useTypedSelector'
import { useActions } from '../../hooks/useActions'
import UserService from '../../services/UsersService'

import style from './style.module.css'

const FormData = () => {
   const [form] = Form.useForm()
   const { user } = useTypedSelector(state => state.auth)
   const { setUser } = useActions()
   const [loading, setLoading] = useState(false)

   const onSubmit = async (value: {
      firstName: string
      lastName: string
      email: string
   }) => {
      if (value.firstName || value.lastName || value.email) {
         try {
            setLoading(true)

            const res = await UserService.updateData(value)
            setUser({ ...user, ...res.data })
            message.success('Данные успешно изменены!')
            form.resetFields()
         } catch (error) {
            message.error('Произошла ошибка при изменении данных! Попробуйте снова.')
         } finally {
            setLoading(false)
         }
      }
   }

   return (
      <Form className={style.form} form={form} onFinish={onSubmit} layout='vertical'>
         <Form.Item className={style.row} name='firstName' label='Имя'>
            <Input allowClear placeholder={user?.firstName} />
         </Form.Item>
         <Form.Item className={style.row} name='lastName' label='Фамилия'>
            <Input allowClear placeholder={user?.lastName} />
         </Form.Item>
         <Form.Item className={style.row} name='email' label='Email'>
            <Input allowClear type='email' placeholder={user?.email} />
         </Form.Item>
         <div className={style.containerButton}>
            <Button htmlType='submit' loading={loading} type='primary'>
               Изменить данные
            </Button>
         </div>
      </Form>
   )
}

export default FormData
