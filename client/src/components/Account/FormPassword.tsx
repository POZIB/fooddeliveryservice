import React, { useState } from 'react'
import { AxiosError } from 'axios'
import { Button, Form, Input, message } from 'antd'

import UserService from '../../services/UsersService'

import style from './style.module.css'
import { rules } from '../../utils/rules'

const FormPassword = () => {
   const [form] = Form.useForm()
   const [loading, setLoading] = useState(false)

   const onSubmit = async (value: { old: string; new: string; confirm: string }) => {
      try {
         setLoading(true)

         const res = await UserService.updatePassword(value)
         if (res.data) {
            message.success('Пароль успешно сменен!')
            form.resetFields()
         }
      } catch (error) {
         const err = error as AxiosError
         if (err.response?.data.message) {
            message.error(err.response?.data.message)
         } else {
            message.error('Произошла ошибка при смене пароля! Попробуйте снова.')
         }
      } finally {
         setLoading(false)
      }
   }

   return (
      <Form className={style.form} form={form} onFinish={onSubmit} layout='vertical'>
         <Form.Item
            name='old'
            label='Текущий пароль'
            rules={[rules.required('Введите текущий пароль')]}
         >
            <Input.Password allowClear />
         </Form.Item>
         <Form.Item
            name='new'
            label='Новый пароль'
            rules={[
               rules.required('Введите новый пароль'),
               { min: 6, message: 'Минимальная длинна пароля 6 символов' },
            ]}
         >
            <Input.Password allowClear />
         </Form.Item>
         <Form.Item
            name='confirm'
            label='Подтверждение'
            dependencies={['new']}
            hasFeedback
            rules={[
               rules.required('Введите еще раз пароль'),
               ({ getFieldValue }) => ({
                  validator(_, value) {
                     if (!value || getFieldValue('new') === value) {
                        return Promise.resolve()
                     }
                     return Promise.reject(new Error('Пароли не совпадают!'))
                  },
               }),
            ]}
         >
            <Input.Password type='password' allowClear />
         </Form.Item>
         <div className={style.containerButton}>
            <Button htmlType='submit' loading={loading} type='primary'>
               Сменить пароль
            </Button>
         </div>
      </Form>
   )
}

export default FormPassword
