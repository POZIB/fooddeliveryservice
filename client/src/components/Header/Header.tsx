import React, { FC, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { Spin } from 'antd'
import { ShoppingCartOutlined, UserOutlined, LoadingOutlined } from '@ant-design/icons'

import { useTypedSelector } from '../../hooks/useTypedSelector'
import { useActions } from '../../hooks/useActions'
import MenuBurger from '../../UI/MenuBurger/MenuBurger'
import logo from '../../assets/logo.png'

import './header.css'

const Navbar: FC = () => {
   const navigate = useNavigate()
   const { logout } = useActions()
   const { sumBasket, isBasketLoading } = useTypedSelector(state => state.basket)
   const { isAuth, user } = useTypedSelector(state => state.auth)

   const userPopupContent = (
      <div className='headerPopupContent'>
         {isAuth ? (
            <ul className='listHeaderPopup'>
               <li onClick={() => navigate('/account')}>Мои данные</li>
               <li onClick={() => navigate('/orders')}>Мои заказы</li>
               <li onClick={() => logout()}>Выйти</li>
            </ul>
         ) : (
            <ul className='listHeaderPopup'>
               <li onClick={() => navigate('/auth/login')}>Войти</li>
            </ul>
         )}
      </div>
   )

   const managmentPopupContent = (
      <div className='headerPopupContent'>
         {user?.roles?.find(role => role.value === 'ADMIN') && (
            <ul className='listHeaderPopup'>
               <li onClick={() => navigate('/admin/statistics')}>Статистика</li>
               <li onClick={() => navigate('/admin/users')}>Пользователи</li>
               <li onClick={() => navigate('/admin/roles')}>Роли пользователей</li>
               <li onClick={() => navigate('/admin/types')}>Типы продукции</li>
               <li onClick={() => navigate('/admin/categories')}>Категории продукции</li>
               <li onClick={() => navigate('/admin/products')}>Продукция</li>
            </ul>
         )}
         {user?.roles?.find(role => role.value === 'MANAGER') && (
            <ul className='listHeaderPopup'>
               <li onClick={() => navigate('/manager/statistics')}>Статистика</li>
               <li onClick={() => navigate('/manager/orders')}>Заказы</li>
            </ul>
         )}

         {user?.roles?.find(role => role.value === 'COURIER') && (
            <ul className='listHeaderPopup'>
               <li onClick={() => navigate('/courier/acceptOrder')}>Принятый заказ</li>
               <li onClick={() => navigate('/courier/orders')}>Заказы</li>
            </ul>
         )}
      </div>
   )

   return (
      <header>
         <div className='headerContent'>
            <div className='headerLeftBlock'>
               {/* <MenuBurger /> */}
               <div className='headerWrapperLogo' onClick={() => navigate('/')}>
                  <img src={logo} alt='' />
               </div>
               <h1 className='headerTitle' onClick={() => navigate('/')}>
                  Faster'S
               </h1>
            </div>
            <div className='headerRightBlock'>
               {user?.roles?.find(role => role.value !== 'CLIENT') && (
                  <div className='managment'>
                     <UserOutlined className='managmentIcon' />
                     <div className='nameRoleUser'>
                        {user?.roles.map(
                           role => role.value !== 'CLIENT' && role.value + '/'
                        )}
                     </div>
                     <div className='headerPopup'>{managmentPopupContent}</div>
                  </div>
               )}

               <div className='auth' onClick={() => !isAuth && navigate('/auth/login')}>
                  <UserOutlined className='authIcon' />
                  <div className='authNameUser'>{isAuth ? user?.email : 'войти'}</div>
                  {isAuth && <div className='headerPopup'>{userPopupContent}</div>}
               </div>
               <div className='cart' onClick={() => navigate('/basket')}>
                  <ShoppingCartOutlined className='cartIcon' />
                  {isBasketLoading ? (
                     <div className='spinCartSum'>
                        <Spin indicator={<LoadingOutlined spin />} delay={0} />
                     </div>
                  ) : (
                     sumBasket != 0 && <div className='cartSum'>{sumBasket}₽</div>
                  )}
               </div>
            </div>
         </div>
      </header>
   )
}

export default Navbar
