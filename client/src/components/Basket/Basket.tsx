import React, { FC, useEffect, useState } from 'react'
import { Spin } from 'antd'
import { DeleteOutlined, LoadingOutlined } from '@ant-design/icons'
import { useActions } from '../../hooks/useActions'
import { useTypedSelector } from '../../hooks/useTypedSelector'
import { IProductBasket } from '../../models/IProductBasket'
import Counter from '../../UI/Counter/Counter'

import cart from '../../assets/cart.png'
import basketStyle from './basketStyle.module.css'

const Basket: FC = () => {
   const { productsInBasket, sumBasket, isBasketLoading } = useTypedSelector(
      state => state.basket
   )
   const { quantityCutlery } = useTypedSelector(state => state.cutlery)
   const { editQuantityProduct, deleteFromBasket, clearBasket, setQuantityCutlery } =
      useActions()

   const DecrementProduct = (product: IProductBasket) => {
      editQuantityProduct(product.id, product.quantity - 1)
   }

   const IncrementProduct = (product: IProductBasket) => {
      editQuantityProduct(product.id, product.quantity + 1)
   }

   const RemoveProduct = (product: IProductBasket) => {
      deleteFromBasket(product.id)
   }

   const ClearBasket = () => {
      clearBasket()
   }

   return (
      <div className={basketStyle.wrapper}>
         <div className={basketStyle.topBasket}>
            <h1>Ваш заказ</h1>
            {!!productsInBasket.length && (
               <button className={basketStyle.btnClearBasket} onClick={ClearBasket}>
                  <DeleteOutlined />
                  Очистить корзину
               </button>
            )}
         </div>

         <div className={basketStyle.cutlery}>
            <div className={basketStyle.iconCutlery}>Количество приборов</div>
            <Counter
               quantity={quantityCutlery}
               max={15}
               onDecrement={() => setQuantityCutlery(quantityCutlery - 1)}
               onIncrement={() => setQuantityCutlery(quantityCutlery + 1)}
            />
         </div>
         {
            <div className={basketStyle.allPrice}>
               {!!productsInBasket.length ? (
                  <>
                     На общую сумму <span>{sumBasket}₽</span>
                  </>
               ) : isBasketLoading ? (
                  <Spin
                     className='root__spiner'
                     tip='Подождите...'
                     indicator={<LoadingOutlined style={{ fontSize: 24 }} spin />}
                  />
               ) : (
                  <>Ваша корзина пуста</>
               )}
            </div>
         }

         {!isBasketLoading && !!!productsInBasket.length && (
            <div className={basketStyle.containerEmtyCart}>
               <img src={cart} alt='' />
            </div>
         )}
         <>
            {productsInBasket.map(product => (
               <div key={product.id} className={basketStyle.card}>
                  <div className={basketStyle.cardContent}>
                     <div className={basketStyle.img}>
                        <img
                           src={String(process.env.REACT_APP_API_URL) + product.img}
                           alt={product.name}
                        />
                     </div>
                     <div className={basketStyle.titleAndQuantityProduct}>
                        <div className={basketStyle.title}>
                           <h3>{product.name}</h3>
                           <span>{product.weight}г.</span>
                           <span className={basketStyle.composition}>
                              {product.composition}
                           </span>
                        </div>

                        <div className={basketStyle.quantityProduct}>
                           <Counter
                              quantity={product.quantity}
                              onDecrement={() => DecrementProduct(product)}
                              onIncrement={() => IncrementProduct(product)}
                           />
                           <span>1шт {product.price}&#x20bd;</span>
                        </div>
                     </div>
                     <div className={basketStyle.price}>
                        {product.quantity * product.price}&#x20bd;
                     </div>

                     <div className={basketStyle.btnRemoveProduct}>
                        <DeleteOutlined onClick={() => RemoveProduct(product)} />
                     </div>
                  </div>
               </div>
            ))}
         </>
      </div>
   )
}

export default Basket
