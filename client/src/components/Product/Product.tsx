import { Button, Rate } from 'antd'
import { FC, useEffect, useMemo, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { ShoppingCartOutlined } from '@ant-design/icons'

import { useActions } from '../../hooks/useActions'
import { useTypedSelector } from '../../hooks/useTypedSelector'
import { IProduct } from '../../models/IProduct'
import { IProductBasket } from '../../models/IProductBasket'
import Counter from '../../UI/Counter/Counter'

import style from './productStyle.module.css'

interface IProps {
   product: IProduct
}

const Product: FC<IProps> = props => {
   const navigate = useNavigate()
   const { addInBasket, editQuantityProduct, changeRaiting } = useActions()
   const { user } = useTypedSelector(state => state.auth)
   const { productsInBasket } = useTypedSelector(state => state.basket)
   const [selectRate, setSelectRate] = useState(false)
   const [productInBasket, setProductInBasket] = useState<IProductBasket>()

   useEffect(() => {
      setProductInBasket(productsInBasket.find(item => item.id == props.product.id))
   }, [productsInBasket])

   const changeRating = async (productId: number, rating: number) => {
      if (user && !selectRate) {
         changeRaiting(productId, rating)
         setSelectRate(true)
      }
   }

   const addToBasket = (productId: number) => {
      if (user) {
         addInBasket(productId)
      } else {
         navigate('/auth/login')
      }
   }

   const DecrementProduct = () => {
      if (user && productInBasket) {
         editQuantityProduct(productInBasket.id, productInBasket.quantity - 1)
      }
   }

   const IncrementProduct = () => {
      if (user && productInBasket) {
         editQuantityProduct(productInBasket.id, productInBasket.quantity + 1)
      }
   }

   const product = props.product
   return (
      <div className={style.card}>
         <div className={style.wrapperImg}>
            <img
               alt={product.name}
               src={String(process.env.REACT_APP_API_URL) + product.img}
               loading='lazy'
            />
         </div>
         <div className={style.cardContent}>
            <div className={style.contentTitle}>
               <h3>{product.name}</h3>
            </div>
            <div className={style.contentBody}>
               <div className={style.weight}>{product.weight}г.</div>
            </div>
            <div className={style.containerDescription}>
               <div className={style.content__hover}>
                  <h3 className={style.contentTitle}>{product.name}</h3>
                  <div>{product.weight}г.</div>
                  <div>{product.description}</div>
                  <div>{product.composition}</div>
               </div>
            </div>
         </div>

         <div className={style.cardBottom}>
            <div className={style.bottomRate}>
               <Rate
                  allowHalf
                  value={product.rating}
                  onChange={rate => changeRating(product.id, rate)}
               />
            </div>
            <div className={style.bottomPriceBtn}>
               <h3 className={style.price}>{product.price}₽</h3>
               <div className={style.containerBtn}>
                  {productInBasket ? (
                     <Counter
                        min={0}
                        quantity={productInBasket.quantity}
                        onIncrement={IncrementProduct}
                        onDecrement={DecrementProduct}
                     />
                  ) : (
                     <Button
                        icon={<ShoppingCartOutlined className={style.iconButton} />}
                        onClick={() => addToBasket(product.id)}
                     >
                        <span className={style.priceInButton}>{product.price}₽</span>
                        <span>В корзину</span>
                     </Button>
                  )}
               </div>
            </div>
         </div>
      </div>
   )
}

export default Product
