import { FC, useEffect, useMemo, useState } from 'react'
import moment from 'moment'
import { Button, Form, Radio, Row, Select } from 'antd'

import { useActions } from '../../../hooks/useActions'
import { rules } from '../../../utils/rules'
import { IDeliveryTime } from '../../../models/IDeliveryTime'
import Toggler from '../../../UI/Toggler/Toggler'
import TogglerButton from '../../../UI/Toggler/TogglerButton'

import style from './selectTimeStyle.module.css'

interface FieldData {
   name: string | number | (string | number)[]
   value?: string | number | boolean
   touched?: boolean
   validating?: boolean
   errors?: string[]
}

const startTheWorkingDay = 9
const endTheWorkingDay = '21:00'

const Time: FC = () => {
   const [datesToTime, setDatesToTime] = useState<string[]>([])
   const [timesToTime, setTimesToTime] = useState<string[]>([])
   const [fields, setFields] = useState<FieldData[]>([])

   const { setDeliveryTime } = useActions()

   const isImmediately = useMemo(() => {
      if (
         moment().isBetween(
            moment(startTheWorkingDay, 'h'),
            moment(endTheWorkingDay, 'h')
         )
      ) {
         setFields([...fields, { name: 'immediatelyOrTime', value: 'immediately' }])
         return true
      }
      return false
   }, [])

   useEffect(() => {
      const result: string[] = []
      if (!moment().isSameOrAfter(moment(endTheWorkingDay, 'h'), 'h')) {
         result.push(moment().format('DD.MM.Y'))
      }
      result.push(moment().add(1, 'd').format('DD.MM.Y'))
      result.push(moment().add(2, 'd').format('DD.MM.Y'))

      setDatesToTime(result)
   }, [])

   const selectTimeByDate = (date: string) => {
      const result: string[] = []
      if (date == moment().format('DD.MM.Y')) {
         if (moment().isSameOrAfter(moment(endTheWorkingDay, 'h'), 'h')) {
            setTimesToTime(result)
            return
         }
         for (let i = 0; ; i++) {
            result.push(
               moment()
                  .startOf('h')
                  .add((i + 2) * 30, 'm')
                  .format('H:mm')
            )
            if (result[i] == endTheWorkingDay) break
         }
      } else {
         for (let i = 0; ; i++) {
            result.push(
               moment()
                  .hour(startTheWorkingDay)
                  .startOf('h')
                  .add(i * 30, 'm')
                  .format('H:mm')
            )
            if (result[i] == endTheWorkingDay) break
         }
      }
      setTimesToTime(result)
   }

   function errorsFields() {
      if (fields.filter(field => field.errors?.length).length) {
         const smsErrors = fields
            .filter(field => field.errors?.length)
            .map(er => er.errors)
            .map((sms, index) => <p key={index}>{sms?.[0]}</p>)
         return smsErrors
      }
   }

   const onFinish = (data: IDeliveryTime) => {
      if (data.immediatelyOrTime === 'immediately') {
         data.date = 'сегодня'
         data.time = moment().add(40, 'm').format('H:mm').toString()
         setDeliveryTime(data)
      } else {
         setDeliveryTime(data)
      }
   }
   const onFinishFailed = (data: any) => {
      setDeliveryTime()
   }

   return (
      <div className={style.container}>
         <h2 className={style.containerTop}>Время доставки</h2>
         <Form
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            fields={fields}
            onFieldsChange={(_, allFields) => {
               setFields(allFields)
            }}
            validateTrigger='onChange'
         >
            <Form.Item
               name='immediatelyOrTime'
               rules={[rules.required('Выберите доставку сразу или ко времени')]}
               noStyle
            >
               <Toggler>
                  <TogglerButton value='immediately' disabled={!isImmediately}>
                     Доставка по готовности
                  </TogglerButton>
                  <TogglerButton value='toTime'>Ко времени</TogglerButton>
               </Toggler>
            </Form.Item>
            <Row className={style.containerTextOrSelect}>
               {fields.find(f => f.name == 'immediatelyOrTime')?.value == 'toTime' ? (
                  <Row className={style.rowDataAndTime}>
                     <Form.Item
                        name='date'
                        rules={[rules.required('Выберите дату')]}
                        noStyle
                     >
                        <Select
                           className={style.select}
                           placeholder='Дата'
                           onChange={date => selectTimeByDate(date)}
                        >
                           {datesToTime.map(date => (
                              <Select.Option key={date}>{date}</Select.Option>
                           ))}
                        </Select>
                     </Form.Item>
                     <Form.Item
                        name='time'
                        rules={[rules.required('Выберите время')]}
                        noStyle
                     >
                        <Select className={style.select} placeholder='Время'>
                           {timesToTime.map(time => (
                              <Select.Option key={time}>{time}</Select.Option>
                           ))}
                        </Select>
                     </Form.Item>
                  </Row>
               ) : (
                  <Row className={style.roweStimatedDeliveryTime}>
                     {isImmediately ? (
                        <p>
                           Примерное время для доставки:{' '}
                           {moment().add(40, 'm').format('H:mm')}
                        </p>
                     ) : (
                        <div className={style.smsNotImmediatelyDelicery}>
                           <p>На сегодня доставки нет</p>
                           <p>
                              Но вы можете выбрать доставку к определенному времени на
                              другой день :)
                           </p>
                        </div>
                     )}
                  </Row>
               )}
            </Row>
            <div className={style.containerBtn}>
               <div className='wrapper-messageErrors'>
                  <div className='toolTip' hidden={!!!errorsFields()}>
                     {errorsFields()}
                  </div>
                  <Button htmlType='submit'>К оплате</Button>
               </div>
            </div>
         </Form>
      </div>
   )
}

export default Time
