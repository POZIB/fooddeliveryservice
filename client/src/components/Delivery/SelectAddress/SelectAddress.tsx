import { FC, useEffect, useMemo, useState } from 'react'
import { Button, Checkbox, Form, Input, Select } from 'antd'
import { HomeOutlined } from '@ant-design/icons'
import MaskedInput from 'antd-mask-input'

import { useTypedSelector } from '../../../hooks/useTypedSelector'
import { useActions } from '../../../hooks/useActions'
import MapModal from '../../modals/MapModal/MapModal'
import { IDeliveryAddress } from '../../../models/IDeliveryAddress'
import Toggler from '../../../UI/Toggler/Toggler'
import TogglerButton from '../../../UI/Toggler/TogglerButton'

import style from './selectAddressStyle.module.css'

interface FieldData {
   name: string | number | (string | number)[]
   value?: string | number | boolean | null | undefined
   touched?: boolean
   validating?: boolean
   errors?: string[]
}

interface IProps {
   setIsCompletedAddress: (value: React.SetStateAction<boolean>) => void
}

const defaultFields: FieldData[] = [
   { name: 'apartmentOrHouse', value: 'Apartment' },
   { name: 'intercom', value: true },
]

const Address: FC<IProps> = props => {
   const [visibleModal, setVisibleModal] = useState(false)
   const [fields, setFields] = useState<FieldData[]>(defaultFields)
   const { user } = useTypedSelector(state => state.auth)
   const { deliveryAddressess, selectDeliveryAddress } = useTypedSelector(
      state => state.deliveryAddress
   )
   const { getDeliveryAddressess, editDeliveryAddress, setSelectDeliveryAddress } =
      useActions()

   useEffect(() => {
      if (user) {
         getDeliveryAddressess()
      }
   }, [user])

   useEffect(() => {
      setFields(pred => [
         ...pred,
         {
            name: 'address',
            value: selectDeliveryAddress?.id ?? deliveryAddressess[0]?.id,
         },
      ])

      if (deliveryAddressess || selectDeliveryAddress) {
         onSelectDeliveryAddress(selectDeliveryAddress?.id || deliveryAddressess[0]?.id)
      }
   }, [user, deliveryAddressess.length])

   const onSelectDeliveryAddress = (addressID?: number) => {
      props.setIsCompletedAddress(false)

      if (!addressID) return
      const address = deliveryAddressess.find(address => address.id === addressID)
      address && setSelectDeliveryAddress(address)

      let arrayFeild: FieldData[] = []
      for (let key in address) {
         arrayFeild.push({ name: key, value: address[key] })
      }
      setFields(arrayFeild)
   }

   const onFinish = () => {
      if (!selectDeliveryAddress) return

      let newFieldsAddress: IDeliveryAddress = { ...selectDeliveryAddress }

      for (let key of fields) {
         if (key) {
            newFieldsAddress[String(key.name)] = key.value
         }
      }

      if (
         selectDeliveryAddress &&
         !compareOldFieldsWithNewFields(newFieldsAddress, selectDeliveryAddress)
      ) {
         if (user) editDeliveryAddress(newFieldsAddress)
         setSelectDeliveryAddress(newFieldsAddress)
      }
      props.setIsCompletedAddress(true)
   }

   function compareOldFieldsWithNewFields(
      newFields: IDeliveryAddress,
      oldFields: IDeliveryAddress
   ) {
      for (let key in newFields) {
         if (newFields[key] !== oldFields[key]) return false
      }
      return true
   }

   const onFinishFailed = (info: any) => {
      props.setIsCompletedAddress(false)
   }

   const errorsFields = useMemo(() => {
      if (fields.filter(field => field.errors?.length).length) {
         props.setIsCompletedAddress(false)

         const smsErrors = fields
            .filter(field => field.errors?.length)
            .map(er => er.errors)
            .map((sms, index) => <p key={index}>{sms?.[0]}</p>)
         return smsErrors
      }
   }, [fields])

   return (
      <div className={style.container}>
         <MapModal visible={visibleModal} onHide={() => setVisibleModal(false)} />
         <div className={style.header}>
            <HomeOutlined />
            <h1>Доставка</h1>
         </div>
         <Form
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            fields={fields}
            onFieldsChange={(changed, allFields) => {
               setFields(allFields)
            }}
         >
            <Form.Item
               name='address'
               rules={[{ required: true, message: 'Выберите адрес' }]}
               noStyle
            >
               <Select
                  className={style.selectAddress}
                  placeholder='Адрес доставки'
                  onChange={e => onSelectDeliveryAddress(e)}
                  dropdownRender={menu => (
                     <>
                        <div
                           className={style.addAddress}
                           onClick={() => setVisibleModal(true)}
                        >
                           Добавьте адрес
                        </div>
                        {menu}
                     </>
                  )}
               >
                  {deliveryAddressess.map(address => (
                     <Select.Option key={address.id} value={address.id}>
                        {address.address}
                     </Select.Option>
                  ))}
               </Select>
            </Form.Item>
            <Form.Item
               name='telephoneNumber'
               noStyle
               rules={[
                  { required: true, message: 'Введите номер телефона' },
                  {
                     validator: (_, value) =>
                        String(value?.replace(/_/g, '')).length === 18
                           ? Promise.resolve()
                           : Promise.reject(new Error('Неверная длинна номера телефона')),
                  },
               ]}
            >
               <MaskedInput mask='+7 (111) 111 11 11' className={style.itemForm} />
            </Form.Item>

            <Form.Item name='apartmentOrHouse' noStyle>
               <Toggler>
                  <TogglerButton value='House'>Частный дом</TogglerButton>
                  <TogglerButton value='Apartment'>Квартира</TogglerButton>
               </Toggler>
            </Form.Item>
            {fields.find(f => f.name == 'apartmentOrHouse')?.value == 'Apartment' && (
               <>
                  <div className={style.rowDataApartament}>
                     <Form.Item
                        name='apartment'
                        rules={[{ required: true, message: 'Введите номер квартиры' }]}
                        noStyle
                     >
                        <Input placeholder='Квартира' style={{ borderRadius: 10 }} />
                     </Form.Item>
                     <Form.Item
                        name='entrance'
                        rules={[{ required: true, message: 'Введите номер подъезда' }]}
                        noStyle
                     >
                        <Input placeholder='Подъезд' style={{ borderRadius: 10 }} />
                     </Form.Item>
                     <Form.Item
                        name='storey'
                        rules={[{ required: true, message: 'Введите номер этажа' }]}
                        noStyle
                     >
                        <Input placeholder='Этаж' style={{ borderRadius: 10 }} />
                     </Form.Item>
                  </div>
                  <Form.Item name='intercom' valuePropName='checked' noStyle>
                     <Checkbox className={style.intercom}>Домофон работает</Checkbox>
                  </Form.Item>
               </>
            )}

            <Form.Item name='comments'>
               <Input.TextArea
                  autoSize={{ minRows: 2, maxRows: 4 }}
                  className={style.itemForm}
                  placeholder='Комментарий к заказу'
               />
            </Form.Item>
            <div className={style.containerBtn}>
               <div className='wrapper-messageErrors'>
                  <div className='toolTip' hidden={!!!errorsFields}>
                     {errorsFields}
                  </div>
                  <Button htmlType='submit'>Выбрать время</Button>
               </div>
            </div>
         </Form>
      </div>
   )
}

export default Address
