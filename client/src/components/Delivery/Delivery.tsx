import { CSSTransition } from 'react-transition-group'
import { FC, useState } from 'react'
import SelectAddress from './SelectAddress/SelectAddress'
import SelectTime from './SelectTime/SelectTime'
import './deliveryStyle.css'

const Delivery: FC = () => {
   const [isCompletedAddress, setIsCompletedAddress] = useState(false)

   return (
      <>
         <section>
            <SelectAddress setIsCompletedAddress={setIsCompletedAddress} />
         </section>
         <CSSTransition
            in={isCompletedAddress}
            timeout={700}
            classNames='time'
            mountOnEnter
            unmountOnExit
         >
            <section className='time'>
               <SelectTime />
            </section>
         </CSSTransition>
      </>
   )
}

export default Delivery
