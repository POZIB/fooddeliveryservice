import { Button } from 'antd'
import React, { FC } from 'react'
import { useTypedSelector } from '../../../hooks/useTypedSelector'

import style from './tableDStyle.module.css'

interface IProps {
   goBack: (value: React.SetStateAction<boolean>) => void
}

const TableDelivery: FC<IProps> = props => {
   const { selectDeliveryAddress } = useTypedSelector(state => state.deliveryAddress)
   const { deliveryTime } = useTypedSelector(state => state.deliveryTime)

   if (!selectDeliveryAddress) return <></>

   return (
      <div className={style.container}>
         <h1 className={style.containerTop}>Информация о доставке</h1>
         <div className={style.field}>{selectDeliveryAddress.address}</div>
         <div className={style.field}>{selectDeliveryAddress.telephoneNumber}</div>
         <div className={style.field}>
            {`Доставка в
            ${
               selectDeliveryAddress.apartmentOrHouse === 'Apartment'
                  ? ` квартиру №${selectDeliveryAddress.apartment}`
                  : ' частный дом'
            }`}
         </div>
         {selectDeliveryAddress.apartmentOrHouse === 'Apartment' && (
            <>
               <div className={style.containerApartment}>
                  <div className={style.field}>
                     {`Подъезд ${selectDeliveryAddress.entrance}`}
                  </div>
                  <div
                     className={style.field}
                  >{`Этаж ${selectDeliveryAddress.storey}`}</div>
               </div>
               <div className={style.field}>
                  {`Домофон ${selectDeliveryAddress.intercom ? '' : 'не '} работает`}
               </div>
            </>
         )}

         {selectDeliveryAddress.comments && (
            <div className={style.field}>{selectDeliveryAddress.comments}</div>
         )}

         <div className={style.field}>
            {`Доставка ${deliveryTime?.date} к ${deliveryTime?.time}`}
         </div>
         <div className={style.bottom}>
            <Button onClick={() => props.goBack(prev => !prev)}>Изменить</Button>
         </div>
      </div>
   )
}

export default TableDelivery
