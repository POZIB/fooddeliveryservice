import { Button } from 'antd'
import React, { FC, useState } from 'react'
import { useTypedSelector } from '../../hooks/useTypedSelector'
import style from './payOrderStyle.module.css'

interface IProps {
   completeTheOrder: () => void
}

const PayOrder: FC<IProps> = props => {
   const [isGoPay, setIsGoPay] = useState(false)
   const { sumBasket, productsInBasket } = useTypedSelector(state => state.basket)

   const handlerPay = () => {
      setIsGoPay(true)
      props.completeTheOrder()
   }

   return (
      <div className={style.container}>
         <h1 className={style.containerTop}>Оплата</h1>
         <div className={style.row}>
            <span>Сумма заказа</span>
            <span>{sumBasket} ₽</span>
         </div>
         {/* <div className={style.row}>
            <span>Стоимость доставки</span>
            <span>{costDelivery} ₽</span>
         </div> */}
         {/* <div>
            <div className={style.row}>
               <span>Скидка</span>
               <span>{50} ₽</span>
            </div>
            <div className={style.fieldCoupon}>
               <input
                  type='text'
                  placeholder='Купон'
                  value={coupon}
                  onChange={e => setCoupon(e.target.value)}
               />
               <button onClick={applyCoupon}>Применить</button>
            </div>
         </div> */}
         <div className={style.wrapperLine}>
            <div className={style.line} />
         </div>

         <div className={style.totalPrice}>
            <span>Итого</span>
            <span>{sumBasket} ₽</span>
         </div>

         <div className={style.rowBtn}>
            <Button
               disabled={!!!productsInBasket.length}
               onClick={handlerPay}
               loading={isGoPay}
            >
               Оплатить
            </Button>
         </div>
      </div>
   )
}

export default PayOrder
