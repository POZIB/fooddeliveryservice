import React, { FC } from 'react'
import { Route, Routes } from 'react-router-dom'
import { useTypedSelector } from '../hooks/useTypedSelector'
import {
   privateAdminRoutes,
   privateCourierRoutes,
   privateManagerRoutes,
   privateRoutes,
   publicRoutes,
} from '../router'
import LoginModal from './Authorization/LoginModal'
import RegisterModal from './Authorization/RegisterModal'
import ResetPasswordModal from './Authorization/ResetPasswordModal'
import SendLinkResetPasswordModal from './Authorization/SendLinkResetPasswordModal'

const AppRouter: FC = () => {
   const { user, isAuth } = useTypedSelector(state => state.auth)

   return (
      <Routes>
         {isAuth
            ? privateRoutes.map(route => (
                 <Route path={route.path} element={<route.element />} key={route.path} />
              ))
            : publicRoutes.map(route => (
                 <Route path={route.path} element={<route.element />} key={route.path} />
              ))}

         {user?.roles?.find(role => role.value === 'ADMIN') &&
            privateAdminRoutes.map(route => (
               <Route path={route.path} element={<route.element />} key={route.path} />
            ))}

         {user?.roles?.find(role => role.value === 'MANAGER') &&
            privateManagerRoutes.map(route => (
               <Route path={route.path} element={<route.element />} key={route.path} />
            ))}

         {user?.roles?.find(role => role.value === 'COURIER') &&
            privateCourierRoutes.map(route => (
               <Route path={route.path} element={<route.element />} key={route.path} />
            ))}

         <Route path='/auth/login' element={<LoginModal />} />
         <Route path='/auth/register' element={<RegisterModal />} />
         <Route path='/auth/password_reset' element={<SendLinkResetPasswordModal />} />
         <Route path='/auth/password_reset/confirmed' element={<ResetPasswordModal />} />
      </Routes>
   )
}

export default AppRouter
