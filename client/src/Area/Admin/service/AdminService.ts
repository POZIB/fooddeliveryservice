import { AxiosResponse } from 'axios'
import $api from '../../../http'
import { UsersResponse } from '../../../models/response/UserResponse'
import { IRole } from '../../../models/IRole'

export default class AdminService {
   static async fetchUsers(
      page: number,
      limit: number,
      role?: string
   ): Promise<AxiosResponse<UsersResponse>> {
      return await $api.get<UsersResponse>('/admin/users', {
         params: { page, limit, role },
      })
   }

   static async editRoleUser(
      email: string,
      roles: IRole[]
   ): Promise<AxiosResponse<string[]>> {
      return await $api.put<string[]>('/admin/users/role', { email, roles })
   }

   static async fetchRoles(): Promise<AxiosResponse<string[]>> {
      return $api.get<string[]>('/admin/roles')
   }

   static async createRole(role: string): Promise<AxiosResponse<string>> {
      return await $api.post<string>('/admin/roles', { role })
   }

   static async deleteRole(role: string): Promise<AxiosResponse> {
      return await $api.delete('/admin/roles', { params: { role } })
   }
}
