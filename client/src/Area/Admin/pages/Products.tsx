import React, { FC, useEffect, useState } from 'react'
import { Button, Modal, Pagination, Select } from 'antd'
import { EditOutlined, DeleteOutlined } from '@ant-design/icons'
import style from '../adminStyle.module.css'

import { useActions } from '../../../hooks/useActions'
import { useTypedSelector } from '../../../hooks/useTypedSelector'
import { IProduct } from '../../../models/IProduct'
import { IType } from '../../../models/IType'
import { ICategory } from '../../../models/ICategory'
import FormModal from '../../../components/modals/FormModal/FormModal'
import ConfirmModal from '../../../components/modals/ConfirmModal/ConfirmModal'
import FormProduct from '../components/ProductForm'

const Products: FC = () => {
   const [isOpenCreateModal, setIsOpenCreateModal] = useState(false)
   const [isOpenChangeModal, setIsOpenChangeModal] = useState(false)
   const [isOpenDeleteModal, setIsOpenDeleteModal] = useState(false)
   const [selectType, setSelectType] = useState<IType | null>(null)
   const [selectCategory, setSelectCategory] = useState<ICategory | null>(null)
   const [selectProduct, setSelectProduct] = useState<IProduct | undefined>(undefined)

   const { types } = useTypedSelector(state => state.types)
   const { categories } = useTypedSelector(state => state.categories)
   const {
      products,
      isProductsLoading,
      productsError,
      totalCount,
      page,
      limit,
      isSubmit,
   } = useTypedSelector(state => state.products)
   const {
      createProduct,
      editProduct,
      deleteProduct,
      setPage,
      setLimit,
      setIsSubmitProduct,
   } = useActions()

   const handleSetPage = (e: number) => {
      setPage(e)
   }

   useEffect(() => {
      if (isSubmit) {
         setIsOpenCreateModal(false)
         setIsOpenChangeModal(false)
         setIsOpenDeleteModal(false)
         setIsSubmitProduct(false)
      }
   }, [isSubmit])

   useEffect(() => {
      setLimit(10)
      if (productsError) {
         Modal.error({
            title: 'Вниманиe!',
            content: <div>{productsError}</div>,
         })
      }
      return () => {
         setLimit(undefined)
      }
   }, [productsError])

   const onDeleteProduct = () => {
      if (selectProduct) {
         deleteProduct(selectProduct?.id)
      }
   }

   const onOpenDeleteModal = (product: IProduct) => {
      setSelectProduct(product)
      setIsOpenDeleteModal(true)
   }

   const onChangeProduct = (form?: FormData) => {
      if (form) {
         editProduct(form)
      } else {
         setIsOpenChangeModal(false)
      }
   }

   const onOpenChangeModal = (product: IProduct) => {
      setSelectProduct(product)
      setIsOpenChangeModal(true)
   }

   const onSubmitCreate = (form?: FormData) => {
      if (form) {
         createProduct(form)
      }
   }

   return (
      <div className={style.layout}>
         <div className={style.container}>
            <div className={style.containerTop}>
               <div className={style.blockTypes}>
                  <div
                     className={style.typeItem}
                     onClick={() => {
                        setSelectCategory(null)
                        setSelectType(null)
                     }}
                  >
                     Все
                  </div>
                  {types
                     ?.filter(types =>
                        selectCategory ? types.id == selectCategory.typeId : types
                     )
                     .map(type => (
                        <span
                           key={type.id}
                           className={`${style.typeItem} ${
                              type.id === selectType?.id ? style.active : ''
                           }`}
                           onClick={() => setSelectType(type)}
                        >
                           {type.name}
                        </span>
                     ))}
                  <Button onClick={() => setIsOpenCreateModal(true)} type='primary'>
                     Создать продукт
                  </Button>
               </div>
               <div className={style.wrapperLimit}>
                  <Select
                     className={style.selectLimit}
                     defaultValue={10}
                     onChange={e => setLimit(e)}
                  >
                     <Select.Option value={10}>10</Select.Option>
                     <Select.Option value={25}>25</Select.Option>
                     <Select.Option value={50}>50</Select.Option>
                     <Select.Option value={null}>Все</Select.Option>
                  </Select>
               </div>
            </div>

            <section className={style.containerMain}>
               <aside className={style.containerRightBlock}>
                  <div className={style.blockCategories}>
                     {categories
                        ?.filter(categories =>
                           selectType ? categories.typeId == selectType?.id : categories
                        )
                        .map(category => (
                           <span
                              key={category.id}
                              className={`${style.categoryItem} ${
                                 category.id === selectCategory?.id ? style.active : ''
                              }`}
                              onClick={() => setSelectCategory(category)}
                           >
                              {category.name}
                           </span>
                        ))}
                  </div>
               </aside>
               <main className={style.containerContent}>
                  <div className={style.contentProducts}>
                     {products
                        ?.filter(products =>
                           selectType ? products.typeId == selectType.id : products
                        )
                        .filter(products =>
                           selectCategory
                              ? products.categoryId == selectCategory.id
                              : products
                        )
                        .map(product => (
                           <div key={product.id} className={style.card}>
                              <div className={style.cardContent}>
                                 <div className={style.wrapperImg}>
                                    <img
                                       src={
                                          String(process.env.REACT_APP_API_URL) +
                                          product.img
                                       }
                                       alt={product.name}
                                    />
                                 </div>
                                 <div className={style.title}>{product.name}</div>
                                 <div>{product.weight}г.</div>
                                 <div className={style.composition}>
                                    {product.composition}
                                 </div>
                                 <div className={style.price}>{product.price}₽</div>
                              </div>
                              <div className={style.cardButtom}>
                                 <EditOutlined
                                    onClick={() => onOpenChangeModal(product)}
                                 />
                                 <DeleteOutlined
                                    onClick={() => onOpenDeleteModal(product)}
                                 />
                              </div>
                           </div>
                        ))}
                  </div>
               </main>
            </section>
         </div>
         <section className={style.containerBottom}>
            <Pagination
               current={page || 1}
               onChange={e => handleSetPage(e)}
               pageSize={limit || -1}
               total={totalCount || 0}
            />
         </section>
         <FormModal
            title='Создание продукта'
            centered
            isOpen={isOpenCreateModal}
            onClose={() => setIsOpenCreateModal(false)}
         >
            <FormProduct
               types={types}
               categories={categories}
               loading={isProductsLoading}
               onFinish={onSubmitCreate}
            />
         </FormModal>
         <FormModal
            title='Изменение продукта'
            centered
            isOpen={isOpenChangeModal}
            onClose={() => setIsOpenChangeModal(false)}
         >
            <FormProduct
               types={types}
               categories={categories}
               product={selectProduct}
               loading={isProductsLoading}
               onFinish={onChangeProduct}
            />
         </FormModal>
         <ConfirmModal
            isOpen={isOpenDeleteModal}
            loading={isProductsLoading}
            onClose={() => setIsOpenDeleteModal(false)}
            onSubmit={onDeleteProduct}
         >
            <div
               style={{
                  fontSize: '18px',
                  display: 'flex',
                  flexDirection: 'column',
                  alignItems: 'center',
               }}
            >
               <div>
                  Вы точно хотите удалить '<strong> {selectProduct?.name}</strong>'?
               </div>
               <div style={{ width: 120, height: 120 }}>
                  <img
                     src={String(process.env.REACT_APP_API_URL) + selectProduct?.img}
                     alt={selectProduct?.name}
                  />
               </div>
            </div>
         </ConfirmModal>
      </div>
   )
}

export default Products
