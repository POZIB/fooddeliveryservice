import { Button, Form, Input, Row } from 'antd'
import React, { FC, useState } from 'react'
import { useActions } from '../../../hooks/useActions'
import { useTypedSelector } from '../../../hooks/useTypedSelector'
import { IType } from '../../../models/IType'
import { rules } from '../../../utils/rules'
import ConfirmModal from '../../../components/modals/ConfirmModal/ConfirmModal'
import MainModal from '../../../components/modals/MainModal/MainModal'

import style from '../adminStyle.module.css'

const Types: FC = () => {
   const [form] = Form.useForm()
   const [isOpenChangeModal, setIsOpenChangeModal] = useState(false)
   const [isOpenDeleteModal, setIsDeleteModal] = useState(false)
   const [selectType, setSelectType] = useState<IType | null>(null)
   const { types, typesError, isTypesLoading } = useTypedSelector(state => state.types)
   const { createType, deleteType, editType } = useActions()

   const onOpenDeleteModal = (type: IType) => {
      setSelectType(type)
      setIsDeleteModal(true)
   }

   const onSubmitDeleteModal = () => {
      if (selectType) {
         deleteType(selectType.id)
         setIsDeleteModal(false)
      }
   }

   const onSubmitChangeModal = () => {
      if (selectType) {
         if (types?.find(type => type.id === selectType.id)?.name !== selectType.name) {
            editType(selectType?.id, selectType?.name)
            setIsOpenChangeModal(false)
         }
      }
   }

   const onOpenChangeModal = (type: IType) => {
      setSelectType(type)
      setIsOpenChangeModal(true)
   }

   const onCreate = (values: any) => {
      if (values.name) {
         createType(values.name)
         form.resetFields()
      }
   }

   return (
      <div className={style.wrapper}>
         <h1 className={style.title}>Типы продуктов</h1>
         <Form
            form={form}
            onFinish={onCreate}
            validateTrigger='onSubmit'
            className={style.containerForm}
         >
            <div className={style.errorServer}>{typesError}</div>
            <div className={style.rowInputWithBtn}>
               <Form.Item name='name' rules={[rules.required()]}>
                  <Input allowClear placeholder='Название' />
               </Form.Item>
               <Form.Item>
                  <Button type='primary' htmlType='submit' loading={isTypesLoading}>
                     Создать
                  </Button>
               </Form.Item>
            </div>
         </Form>

         <div>
            {types.map((type, index) => (
               <div key={type.id} className={style.row}>
                  {index + 1}. {type.name}
                  <Row wrap={false}>
                     <Button
                        style={{ marginRight: '10px' }}
                        onClick={() => onOpenChangeModal(type)}
                     >
                        Изменить
                     </Button>
                     <Button danger onClick={() => onOpenDeleteModal(type)}>
                        Удалить
                     </Button>
                  </Row>
               </div>
            ))}
         </div>
         <MainModal
            title={`Изменение названия`}
            isOpen={isOpenChangeModal}
            onClose={() => setIsOpenChangeModal(false)}
            onSubmit={onSubmitChangeModal}
            loading={isTypesLoading}
         >
            <Form layout='vertical'>
               <Form.Item label='Название'>
                  <Input
                     placeholder='Название типа'
                     value={selectType?.name}
                     onChange={e =>
                        setSelectType(
                           prev => prev && { id: prev.id, name: e.target.value }
                        )
                     }
                  />
               </Form.Item>
            </Form>
         </MainModal>
         <ConfirmModal
            isOpen={isOpenDeleteModal}
            onClose={() => setIsDeleteModal(false)}
            onSubmit={onSubmitDeleteModal}
            loading={isTypesLoading}
         >
            <div style={{ fontSize: '18px' }}>
               Вы точно хотите удалить тип '<strong>{selectType?.name}'?</strong>
            </div>
         </ConfirmModal>
      </div>
   )
}

export default Types
