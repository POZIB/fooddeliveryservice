import { Button, Form, Input, Row, Select } from 'antd'
import React, { FC, useState } from 'react'
import ConfirmModal from '../../../components/modals/ConfirmModal/ConfirmModal'
import MainModal from '../../../components/modals/MainModal/MainModal'
import { useActions } from '../../../hooks/useActions'
import { useTypedSelector } from '../../../hooks/useTypedSelector'
import { ICategory } from '../../../models/ICategory'
import { rules } from '../../../utils/rules'
import style from '../adminStyle.module.css'

const Categories: FC = () => {
   const [form] = Form.useForm()
   const [isOpenChangeModal, setIsOpenChangeModal] = useState(false)
   const [isOpenDeleteModal, setIsDeleteModal] = useState(false)
   const [selectCategory, setSelectCategory] = useState<ICategory | null>(null)

   const { categories, isCategoriesLoading, categoryError } = useTypedSelector(
      state => state.categories
   )
   const { types } = useTypedSelector(state => state.types)
   const { createCategory, editCategory, deleteCategory } = useActions()

   const onOpenDeleteModal = (category: ICategory) => {
      setSelectCategory(category)
      setIsDeleteModal(true)
   }

   const onSubmitDeleteModal = () => {
      if (selectCategory) {
         deleteCategory(selectCategory.id)
         setIsDeleteModal(false)
      }
   }

   const onOpenChangeModal = (category: ICategory) => {
      setSelectCategory(category)
      setIsOpenChangeModal(true)
   }

   const onSubmitChangeModal = () => {
      const _category = categories.find(cat => cat.id == selectCategory?.id)
      if (selectCategory) {
         if (
            _category?.name !== selectCategory.name ||
            _category.typeId !== selectCategory.typeId
         ) {
            editCategory(selectCategory)
            setIsOpenChangeModal(false)
         }
      }
   }

   const onCreate = (values: any) => {
      if (values.name && values.type) {
         createCategory(values.name, values.type)
         form.resetFields()
      }
   }

   return (
      <div className={style.wrapper}>
         <h1 className={style.title}>Категории продуктов</h1>
         <Form
            form={form}
            onFinish={onCreate}
            validateTrigger='onSubmit'
            className={style.containerForm}
         >
            <div className={style.errorServer}>{categoryError}</div>
            <div className={style.rowInputWithBtn}>
               <Form.Item name='name' rules={[rules.required()]}>
                  <Input allowClear placeholder='Название' />
               </Form.Item>
               <Form.Item
                  name='type'
                  rules={[rules.required()]}
                  style={{ width: '140px' }}
               >
                  <Select placeholder='Тип категории'>
                     {types.map(type => (
                        <Select.Option key={type.id} value={type.id}>
                           {type.name}
                        </Select.Option>
                     ))}
                  </Select>
               </Form.Item>
               <Form.Item>
                  <Button htmlType='submit' loading={isCategoriesLoading}>
                     Создать
                  </Button>
               </Form.Item>
            </div>
         </Form>

         <div>
            {categories.map((category, index) => (
               <div key={category.id} className={style.row}>
                  {index + 1}. {category.name} {' - '}
                  {types.find(type => type.id == category.typeId)?.name}
                  <Row wrap={false}>
                     <Button
                        style={{ marginRight: '10px' }}
                        onClick={() => onOpenChangeModal(category)}
                     >
                        Изменить
                     </Button>
                     <Button danger onClick={() => onOpenDeleteModal(category)}>
                        Удалить
                     </Button>
                  </Row>
               </div>
            ))}
         </div>
         <MainModal
            title={`Изменение названия`}
            isOpen={isOpenChangeModal}
            onClose={() => setIsOpenChangeModal(false)}
            onSubmit={onSubmitChangeModal}
            loading={isCategoriesLoading}
         >
            <Form layout='inline' colon={false}>
               <Form.Item
                  label='Название'
                  labelAlign='left'
                  style={{
                     display: 'flex',
                     flexDirection: 'column',
                     marginRight: '10px',
                  }}
               >
                  <Input
                     value={selectCategory?.name}
                     onChange={e =>
                        setSelectCategory(
                           prev =>
                              prev && {
                                 id: prev.id,
                                 typeId: prev.typeId,
                                 name: e.target.value,
                              }
                        )
                     }
                  />
               </Form.Item>
               <Form.Item
                  label='Тип категории'
                  labelAlign='left'
                  style={{
                     display: 'flex',
                     flexDirection: 'column',
                  }}
               >
                  <Select
                     placeholder='Тип категории'
                     value={selectCategory?.typeId}
                     onChange={e =>
                        setSelectCategory(
                           prev =>
                              prev && {
                                 id: prev.id,
                                 typeId: e,
                                 name: prev.name,
                              }
                        )
                     }
                  >
                     {types.map(type => (
                        <Select.Option key={type.id} value={type.id}>
                           {type.name}
                        </Select.Option>
                     ))}
                  </Select>
               </Form.Item>
            </Form>
         </MainModal>
         <ConfirmModal
            isOpen={isOpenDeleteModal}
            onClose={() => setIsDeleteModal(false)}
            onSubmit={onSubmitDeleteModal}
            loading={isCategoriesLoading}
         >
            <div style={{ fontSize: '18px' }}>
               Вы точно хотите удалить категорию '
               <strong>{selectCategory?.name}'?</strong>
            </div>
         </ConfirmModal>
      </div>
   )
}

export default Categories
