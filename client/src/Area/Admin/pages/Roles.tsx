import React, { FC, useState, useEffect } from 'react'
import { Button, Form, Input } from 'antd'
import { Link } from 'react-router-dom'

import AdminService from '../service/AdminService'
import { rules } from '../../../utils/rules'
import ConfirmModal from '../../../components/modals/ConfirmModal/ConfirmModal'

import style from '../adminStyle.module.css'
import { AxiosError } from 'axios'

const Roles: FC = () => {
   const [isOpenDeleteModal, setIsDeleteModal] = useState(false)
   const [selectRole, setSelectRoles] = useState<string>('')
   const [newRole, setNewRole] = useState('')
   const [roles, setRoles] = useState<string[]>([])
   const [loading, setLoading] = useState(false)
   const [error, setError] = useState('')

   useEffect(() => {
      AdminService.fetchRoles()
         .then(res => setRoles(res.data))
         .catch((error: AxiosError) => setError(error.response?.data.message))
   }, [])

   const onSubmitDeleteModal = async () => {
      try {
         setLoading(true)
         await AdminService.deleteRole(selectRole)
         setRoles(prev => prev.filter(item => item !== selectRole))
         setSelectRoles('')
         setIsDeleteModal(false)
      } catch (error) {
         const err = error as AxiosError
         setError(err.response?.data.message)
         console.log(err.response?.data.message)
      } finally {
         setLoading(false)
      }
   }

   const onSubmitCreate = async () => {
      try {
         setLoading(true)
         const res = await AdminService.createRole(newRole)
         setRoles(prev => [...prev, res.data])
         setNewRole('')
      } catch (error) {
         const err = error as AxiosError
         setError(err.response?.data.message)
      } finally {
         setLoading(false)
      }
   }

   return (
      <div className={style.wrapper}>
         <h1 className={style.title}>Роли пользователей</h1>
         <div>
            <Form
               onFinish={onSubmitCreate}
               validateTrigger='onSubmit'
               className={style.containerForm}
            >
               <div className={style.errorServer}>{error}</div>
               <div className={style.rowInputWithBtn}>
                  <Form.Item rules={[rules.required()]}>
                     <Input
                        allowClear
                        placeholder='Название роли'
                        value={newRole}
                        onChange={e => setNewRole(e.target.value)}
                     />
                  </Form.Item>
                  <Form.Item>
                     <Button type='primary' htmlType='submit' loading={loading}>
                        Добавить
                     </Button>
                  </Form.Item>
               </div>
            </Form>
         </div>
         <div>
            {roles?.map((role, index) => (
               <div key={role} className={style.row}>
                  <div>
                     {index + 1}. {role}
                  </div>
                  <Button
                     danger
                     type='primary'
                     onClick={() => {
                        setSelectRoles(role)
                        setIsDeleteModal(true)
                     }}
                  >
                     Удалить
                  </Button>
               </div>
            ))}
            <ConfirmModal
               isOpen={isOpenDeleteModal}
               loading={loading}
               maskClosable={false}
               onClose={() => setIsDeleteModal(false)}
               onSubmit={onSubmitDeleteModal}
            >
               <div style={{ fontSize: '18px' }}>
                  Вы точно хотите удалить роль '<strong>{selectRole}'?</strong>
               </div>
            </ConfirmModal>
         </div>

         <Link to='/admin/users'>Cписок пользователей</Link>
      </div>
   )
}

export default Roles
