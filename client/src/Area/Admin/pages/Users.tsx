import { Button, Checkbox, Pagination, Radio, RadioChangeEvent, Row, Spin } from 'antd'
import { LoadingOutlined } from '@ant-design/icons'
import { FC, useEffect, useState } from 'react'
import useResponse from '../../../hooks/useResponse'
import AdminService from '../service/AdminService'
import { IUser } from '../../../models/IUser'
import { UsersResponse } from '../../../models/response/UserResponse'
import MainModal from '../../../components/modals/MainModal/MainModal'

import style from '../adminStyle.module.css'

const defaultValueSort = 'CLIENT'

const Users: FC = () => {
   const [page, setPage] = useState(1)
   const [limit, setLimit] = useState(10)
   const [isOpenChangeModal, setIsOpenChangeModal] = useState(false)
   const [selectUser, setSelectUser] = useState<IUser>({} as IUser)
   const [inputsSort, setInputsSort] = useState<string>(defaultValueSort)
   const [roles, , , setRoles] = useResponse(() => AdminService.fetchRoles())
   const [users, loading, error, setUsers] = useResponse<UsersResponse>(null)

   useEffect(() => {
      setUsers(() => AdminService.fetchUsers(page, limit, inputsSort))
   }, [page, limit])

   const handlePage = (page: number) => {
      setPage(page)
   }

   const handleChangeLimitAndPage = (currentPage: number, limit: number) => {
      setPage(currentPage)
      setLimit(limit)
   }

   const onOpenModal = (user: IUser) => {
      setSelectUser(user)
      setIsOpenChangeModal(true)
   }

   const handleCheckBoxChange = (role: string) => {
      let newArr = [...selectUser.roles, { value: role }]
      if (selectUser.roles.find(r => r.value === role)) {
         newArr = newArr.filter(r => r.value !== role)
      }
      setSelectUser(prev => ({ ...prev, roles: newArr }))
   }

   const submitModal = async () => {
      await AdminService.editRoleUser(selectUser.email, selectUser.roles).then(() => {
         setUsers(() => AdminService.fetchUsers(page, limit, inputsSort))
         setIsOpenChangeModal(false)
      })
   }

   const handleChangeSort = (e: RadioChangeEvent) => {
      setInputsSort(e.target.value)
      setUsers(() => AdminService.fetchUsers(page, limit, e.target.value))
   }

   return (
      <div className={style.layout}>
         <h1 className={style.title}>Все пользователи</h1>
         <div className={style.sortUsers}>
            <Radio.Group onChange={handleChangeSort} defaultValue={defaultValueSort}>
               <Radio value='CLIENT'>Клиенты</Radio>
               <Radio value='MANAGER'>Менеджеры</Radio>
               <Radio value='COURIER'>Курьеры</Radio>
            </Radio.Group>
         </div>
         <div className={style.container}>
            <div className={style.errorServer}>{error}</div>
            <div className={style.content}>
               {loading && (
                  <div>
                     <Spin
                        className='root__spiner'
                        tip='Подождите...'
                        indicator={<LoadingOutlined style={{ fontSize: 24 }} spin />}
                        delay={500}
                     />
                  </div>
               )}
               {users?.rows?.map((user, index) => (
                  <Row
                     wrap={false}
                     justify='space-between'
                     align='middle'
                     style={{ padding: '10px 5px' }}
                     key={user.email}
                  >
                     <div key={user.email}>
                        {index + 1}. {user.email}
                     </div>
                     <Button type='primary' onClick={() => onOpenModal(user)}>
                        Изменить права
                     </Button>
                  </Row>
               ))}
            </div>
         </div>
         <div className={style.buttom}>
            <Pagination
               pageSizeOptions={['10', '30', '50']}
               total={users?.count}
               showTotal={total => `Всего ${total}`}
               defaultPageSize={limit}
               current={page}
               showSizeChanger
               onChange={handlePage}
               onShowSizeChange={handleChangeLimitAndPage}
               locale={{ items_per_page: '' }}
            />
         </div>
         <MainModal
            title={`Изменение ролей для ${selectUser.email}`}
            isOpen={isOpenChangeModal}
            loading={loading}
            onClose={() => setIsOpenChangeModal(false)}
            onSubmit={submitModal}
         >
            {roles?.map(role => (
               <div key={role}>
                  <Checkbox
                     checked={!!selectUser.roles?.find(r => r.value === role)}
                     onChange={() => handleCheckBoxChange(role)}
                  >
                     {role}
                  </Checkbox>
               </div>
            ))}
         </MainModal>
      </div>
   )
}

export default Users
