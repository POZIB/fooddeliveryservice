import { FC, useEffect, useState } from 'react'
import { Button, Cascader, Form, Input, Row } from 'antd'
import TextArea from 'antd/lib/input/TextArea'

import { useTypedSelector } from '../../../hooks/useTypedSelector'
import { rules } from '../../../utils/rules'
import { IProduct } from '../../../models/IProduct'
import { ICategory } from '../../../models/ICategory'
import { IType } from '../../../models/IType'
import Uploader from '../../../UI/Uploader/Uploader'

interface IProps {
   types: IType[]
   categories: ICategory[]
   product?: IProduct
   loading?: boolean
   onFinish: (form?: FormData) => void
}

const FormProduct: FC<IProps> = ({ types, categories, product, onFinish }) => {
   const [form] = Form.useForm()
   const { isSubmit } = useTypedSelector(state => state.products)
   const [loading, setLoading] = useState(false)

   useEffect(() => {
      if (isSubmit) {
         form.resetFields()
      }
   }, [isSubmit])

   const optionsCascader = types.map(type => ({
      value: type.id,
      label: type.name,
      children: categories
         .filter(category => category.typeId == type.id)
         .map(category => ({
            label: category.name,
            value: category.id,
         })),
   }))

   const isProductEqualNewValues = (_old?: IProduct, values?: any) => {
      if (!_old || !values) return true
      return (
         product?.name != values.name ||
         product?.price != values.price ||
         product?.description != values.description ||
         product?.composition != values.composition ||
         product?.weight != values.weight ||
         product?.img != values.img ||
         product?.typeId != values.typeCategory[0] ||
         product?.categoryId != values.typeCategory[1]
      )
   }

   const finish = (values: any) => {
      if (isProductEqualNewValues()) {
         setLoading(true)
         const formData = new FormData()
         formData.append('id', String(product?.id))
         formData.append('name', values.name)
         formData.append('price', values.price)
         formData.append('description', values.description)
         formData.append('composition', values.composition)
         formData.append('weight', values.weight)
         formData.append('img', values.img[0])
         formData.append('typeId', values.typeCategory[0])
         formData.append('categoryId', values.typeCategory[1])

         onFinish(formData)
      } else {
         onFinish()
      }
   }

   const initialValues = {
      name: product?.name,
      price: product?.price,
      description: product?.description || '',
      composition: product?.composition || '',
      weight: product?.weight || '',
      img: product?.img && [product?.img],
      typeCategory: [product?.typeId, product?.categoryId],
   }

   return (
      <Form
         form={form}
         onFinish={finish}
         layout='vertical'
         validateTrigger='onSubmit'
         initialValues={initialValues}
      >
         <Form.Item name='name' label='Название' rules={[rules.required()]}>
            <Input allowClear placeholder='Название' />
         </Form.Item>
         <Row justify='space-between'>
            <Form.Item name='price' label='Цена' rules={[rules.required()]}>
               <Input allowClear type='number' placeholder='Цена' />
            </Form.Item>
            <Form.Item name='weight' label='Вес'>
               <Input allowClear placeholder='Вес' />
            </Form.Item>
         </Row>
         <Form.Item name='description' label='Описание'>
            <TextArea allowClear placeholder='Описание' />
         </Form.Item>
         <Form.Item name='composition' label='Состав'>
            <TextArea allowClear placeholder='Состав' />
         </Form.Item>

         <Form.Item
            name='typeCategory'
            label='Тип и категория'
            rules={[
               rules.required(),
               {
                  validator: (_, value) =>
                     value?.length < 2
                        ? Promise.reject(new Error('выберите все элементы'))
                        : Promise.resolve(),
               },
            ]}
         >
            <Cascader
               placeholder='Категория'
               options={optionsCascader}
               expandTrigger='hover'
            />
         </Form.Item>
         <Form.Item name='img'>
            <Uploader
               multiple={false}
               // defaultFileList={product?.img ? [product?.img] : null}
               // outFiles={(files: File[]) => form.setFieldsValue({ img: files[0] })}
            />
         </Form.Item>
         <Form.Item>
            <Row justify='end'>
               <Button type='primary' htmlType='submit' loading={loading}>
                  Отправить
               </Button>
            </Row>
         </Form.Item>
      </Form>
   )
}

export default FormProduct
