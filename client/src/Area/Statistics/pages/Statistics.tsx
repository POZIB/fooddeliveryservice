import React, { FC } from 'react'
import StatisticTodaysOrders from '../components/StatisticTodaysOrders'
import StatisticTodaysCouriers from '../components/StatisticTodaysCouriers'
import StatisticOrders from '../components/StatisticOrders'
import StatisticCouriers from '../components/StatisticCouriers'

import style from './statistics.module.css'

const Statistics: FC = () => {
   return (
      <div className={style.layout}>
         <h1>Статистика</h1>
         <div className={style.container}>
            <div className={style.containerTop}>
               <section className={style.sectionTodays}>
                  <div className={style.cardTodays}>
                     <StatisticTodaysOrders />
                  </div>
                  <div className={style.cardTodays}>
                     <StatisticTodaysCouriers />
                  </div>
               </section>
               <section className={style.sectionGraphics}>
                  <div className={style.cardGraphics}>
                     <StatisticOrders />
                  </div>
                  <div className={style.cardGraphics}>
                     <StatisticCouriers />
                  </div>
               </section>
            </div>
         </div>
      </div>
   )
}

export default Statistics
