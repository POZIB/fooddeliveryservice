import { AxiosResponse } from 'axios'
import $api from '../../../http'

import { ICourier } from '../models/ICourier'
import { IStatisticCourier } from '../models/IStatisticCourier'
import { IStatisticOrder } from '../models/IStatisticOrder'

export default class StatisticService {
   static async fetchStatisticOrders(
      dateInterval?: [string, string],
      dateTrunc?: string
   ): Promise<AxiosResponse<IStatisticOrder[]>> {
      return await $api.get<IStatisticOrder[]>('/statistics/orders', {
         params: { dateInterval, dateTrunc },
      })
   }

   static async fetchStatisticTodaysOrders(): Promise<AxiosResponse<IStatisticOrder>> {
      return await $api.get<IStatisticOrder>('/statistics/todaysOrders')
   }

   static async fetchCouriers(): Promise<AxiosResponse<ICourier[]>> {
      return await $api.get<ICourier[]>('/statistics/couriers')
   }

   static async fetchStatisticCourierById(
      id: number
   ): Promise<AxiosResponse<IStatisticCourier[]>> {
      return await $api.get<IStatisticCourier[]>('/statistics/courier', {
         params: { courierId: id },
      })
   }

   static async fetchStatisticTodaysCouriers(): Promise<
      AxiosResponse<IStatisticCourier[]>
   > {
      return await $api.get<IStatisticCourier[]>('/statistics/todaysCouriers')
   }
}
