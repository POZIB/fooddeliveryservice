export interface IStatisticOrder {
   date: string
   count: number
   total: number
}
