export interface IStatisticCourier {
   name: string
   date?: string
   ordersCount: number
}
