import React, { FC, useEffect, useState } from 'react'
import {
   BarChart,
   Bar,
   XAxis,
   YAxis,
   CartesianGrid,
   Tooltip,
   Legend,
   ResponsiveContainer,
} from 'recharts'
import { DatePicker, Row, Select, Col } from 'antd'

import useResponse from '../../../hooks/useResponse'
import StatisticService from '../services/StatisticService'

const StatisticOrders: FC = () => {
   const [orders, , , setOrders] = useResponse(() =>
      StatisticService.fetchStatisticOrders()
   )
   const [dateInterval, setDateInterval] = useState<[string, string]>()
   const [dateTrunc, setDataTrunc] = useState<string>('day')

   useEffect(() => {
      setOrders(() => StatisticService.fetchStatisticOrders(dateInterval, dateTrunc))
   }, [dateInterval, dateTrunc])

   const handleChangeIntervalDates = (_: any, value: [string, string]) => {
      setDateInterval(value)
   }

   const handlerChangeDataTrunc = (value: string) => {
      setDataTrunc(value)
   }

   return (
      <>
         <Row>
            <Col span={8}>
               <h5>График выручки и заказов</h5>
               <DatePicker.RangePicker
                  format='YYYY-MM-DD'
                  placeholder={['Начальная дата', 'Конечная дата']}
                  onChange={handleChangeIntervalDates}
                  style={{ margin: '0 14px' }}
               />
            </Col>
            <Col span={8} offset={8}>
               <p>Группировать по:</p>
               <Select
                  placeholder=''
                  bordered={false}
                  style={{ width: 125, fontSize: 16 }}
                  value={dateTrunc}
                  onSelect={handlerChangeDataTrunc}
               >
                  <Select.Option value={'day'}>Дням</Select.Option>
                  <Select.Option value={'week'}>Неделям</Select.Option>
                  <Select.Option value={'month'}>Месяцам</Select.Option>
                  <Select.Option value={'year'}>Годам</Select.Option>
               </Select>
            </Col>
         </Row>
         <div>
            <ResponsiveContainer height={250}>
               <BarChart
                  data={orders || []}
                  margin={{
                     top: 20,
                     right: 30,
                     left: 20,
                     bottom: 5,
                  }}
               >
                  <CartesianGrid strokeDasharray='3 3' />
                  <XAxis dataKey='date' fontSize={14} />
                  <YAxis fontSize={16} />
                  <Tooltip itemStyle={{ fontSize: 16 }} labelStyle={{ fontSize: 17 }} />
                  <Legend wrapperStyle={{ fontSize: 17 }} />
                  <Bar dataKey='total' stackId='a' name='Выручка' fill='#8884d8' />
                  <Bar
                     dataKey='count'
                     stackId='a'
                     name='Кол-во заказов'
                     fill='#82ca9d'
                  ></Bar>
               </BarChart>
            </ResponsiveContainer>
         </div>
      </>
   )
}

export default StatisticOrders
