import React, { FC, useEffect, useState } from 'react'
import { Select } from 'antd'
import { ICourier } from '../models/ICourier'

interface IProps {
   couriers?: ICourier[] | null
   onSelectCourier: (courier: ICourier) => void
}

const ListCouriers: FC<IProps> = props => {
   const [selectCourier, setSelectCourier] = useState<ICourier | null>(null)

   useEffect(() => {
      if (selectCourier) {
         props.onSelectCourier(selectCourier)
      }
   }, [selectCourier])

   const handlerChange = (value: number) => {
      const courier = props.couriers?.find(c => c.id === value)
      setSelectCourier(courier || null)
   }

   return (
      <div>
         <Select
            placeholder='Выберите курьера'
            bordered={false}
            style={{ width: 185, fontSize: 16 }}
            value={selectCourier?.id}
            onSelect={handlerChange}
         >
            {props.couriers?.map(
               courier =>
                  courier.firstName && (
                     <Select.Option key={courier.id} value={courier.id}>
                        <div>
                           {courier.firstName} {courier.lastName}
                        </div>
                     </Select.Option>
                  )
            )}
         </Select>
      </div>
   )
}

export default ListCouriers
