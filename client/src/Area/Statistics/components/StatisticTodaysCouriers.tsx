import React, { FC } from 'react'
import { Spin } from 'antd'
import { LoadingOutlined } from '@ant-design/icons'
import { PieChart, Pie, Cell, ResponsiveContainer, Tooltip } from 'recharts'

import useResponse from '../../../hooks/useResponse'
import StatisticService from '../services/StatisticService'

import style from './statistics.module.css'

const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042']

const StatisticTodaysCouriers: FC = () => {
   const [statistics, loading] = useResponse(() =>
      StatisticService.fetchStatisticTodaysCouriers()
   )

   if (loading) {
      return (
         <Spin
            className='root__spiner'
            tip='Подождите...'
            indicator={<LoadingOutlined style={{ fontSize: 104 }} spin />}
            delay={500}
         />
      )
   }

   if (!statistics?.length) {
      return <div>Сегодня ни один курьер не выполнил заказ</div>
   }

   return (
      <div>
         <ResponsiveContainer height={190} width='100%'>
            <PieChart>
               <Pie
                  data={statistics || []}
                  cx='50%'
                  cy='50%'
                  outerRadius={60}
                  fontSize={14}
                  labelLine={false}
                  textAnchor='start'
                  label={({ name, value }) => ` ${value}шт`}
                  fill='#8884d8'
                  dataKey='ordersCount'
                  isAnimationActive={false}
               >
                  {statistics?.map((entry, index) => (
                     <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
                  ))}
               </Pie>
               <Tooltip />
            </PieChart>
         </ResponsiveContainer>

         <div className={style.containerCourierNames}>
            {statistics?.map((st, index) => (
               <div key={st.name} className={style.nameCourier}>
                  <p>{st.name}</p>
                  <div
                     className={style.colorCourier}
                     style={{
                        backgroundColor: COLORS[index % COLORS.length],
                     }}
                  ></div>
               </div>
            ))}
         </div>
      </div>
   )
}

export default StatisticTodaysCouriers
