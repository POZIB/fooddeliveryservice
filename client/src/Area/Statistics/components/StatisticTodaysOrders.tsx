import React, { FC } from 'react'
import { Spin } from 'antd'
import { LoadingOutlined } from '@ant-design/icons'

import useResponse from '../../../hooks/useResponse'
import StatisticService from '../services/StatisticService'

const StatisticTodaysOrders: FC = () => {
   const [todeysOrders, loading] = useResponse(() =>
      StatisticService.fetchStatisticTodaysOrders()
   )

   if (loading) {
      return (
         <Spin
            className='root__spiner'
            tip='Подождите...'
            indicator={<LoadingOutlined style={{ fontSize: 104 }} spin />}
            delay={500}
         />
      )
   }

   if (!todeysOrders && !loading) {
      return (
         <div>
            <div>Сегодня еще не </div>
            выполнено ни одного заказа
         </div>
      )
   }

   return (
      <div>
         <div>
            <p>
               Сегодня выполнено заказов: <strong> {todeysOrders?.count}шт</strong>
            </p>
            <p>
               на сумму <strong> {todeysOrders?.total}р</strong>
            </p>
         </div>
      </div>
   )
}

export default StatisticTodaysOrders
