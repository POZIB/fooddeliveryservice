import React, { FC, useEffect, useState } from 'react'
import {
   BarChart,
   Bar,
   XAxis,
   YAxis,
   CartesianGrid,
   Tooltip,
   Legend,
   ResponsiveContainer,
   LabelList,
} from 'recharts'

import StatisticService from '../services/StatisticService'
import useResponse from '../../../hooks/useResponse'
import { ICourier } from '../models/ICourier'
import { IStatisticCourier } from '../models/IStatisticCourier'
import ListCouriers from '../components/ListCouriers'

import style from './statistics.module.css'

const StatisticCouriers: FC = () => {
   const [couriers, , loading, setCouriers] = useResponse(() =>
      StatisticService.fetchCouriers()
   )
   const [statisticCourier, , , setStatisticCourier] =
      useResponse<IStatisticCourier[]>(null)
   const [selectCourier, setSelectCourier] = useState<ICourier | null>(null)

   useEffect(() => {
      if (selectCourier)
         setStatisticCourier(() =>
            StatisticService.fetchStatisticCourierById(selectCourier.id)
         )
   }, [selectCourier])

   const handleSelectCourier = (courier: ICourier) => {
      setSelectCourier(courier)
   }

   return (
      <>
         <h5>График статистики работы курьера</h5>
         <div className={style.contaierCouriersGraphic}>
            <div>
               <ListCouriers couriers={couriers} onSelectCourier={handleSelectCourier} />
            </div>
            <div>
               <ResponsiveContainer height={250}>
                  <BarChart
                     data={statisticCourier || []}
                     margin={{
                        top: 20,
                        right: 30,
                        left: 20,
                        bottom: 5,
                     }}
                  >
                     <CartesianGrid strokeDasharray='3 3' />
                     <XAxis dataKey='date' fontSize={14} />
                     <YAxis fontSize={16} />
                     <Tooltip
                        itemStyle={{ fontSize: 16 }}
                        labelStyle={{ fontSize: 17 }}
                     />
                     <Legend wrapperStyle={{ fontSize: 17 }} />
                     <Bar
                        dataKey='ordersCount'
                        name='Кол-во заказов'
                        fill='#8884d8'
                        style={{ fontSize: 14 }}
                     >
                        <LabelList fontSize={18} dataKey='ordersCount' fill='#fff' />
                     </Bar>
                  </BarChart>
               </ResponsiveContainer>
            </div>
         </div>
      </>
   )
}

export default StatisticCouriers
