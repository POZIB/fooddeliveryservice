import React, { FC, useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { Link } from 'react-router-dom'
import moment from 'moment'
import { Spin, Button } from 'antd'
import { LoadingOutlined } from '@ant-design/icons'

import CourierService from '../service/CourierService'
import { IOrder } from '../../../models/IOrder'

import style from '../courierStyle.module.css'
import { AxiosError } from 'axios'

const AcceptedOrder: FC = () => {
   const navigate = useNavigate()
   const [order, setOrder] = useState<IOrder | undefined>(undefined)
   const [loading, setLoading] = useState(false)
   const [error, setError] = useState('')

   useEffect(() => {
      fetchOrder()
   }, [])

   const fetchOrder = async () => {
      try {
         setLoading(true)
         const res = await CourierService.fetchAcceptOrder()
         setOrder(res.data)
      } catch (error) {
         const err = error as AxiosError
         setError(err.response?.data.message)
      } finally {
         setLoading(false)
      }
   }

   const handleClick = async (orderId?: number, status?: string) => {
      if (orderId) {
         await CourierService.changeStatusOrder(orderId, status).then(response => {
            if (response.data) navigate('/courier/orders')
         })
      }
   }

   if (error) {
      return <div>{error}</div>
   }

   if (loading) {
      return (
         <Spin
            className='root__spiner'
            tip='Подождите...'
            indicator={<LoadingOutlined style={{ fontSize: 84 }} spin />}
            delay={200}
         />
      )
   }

   return (
      <div className={style.wrapperOrders}>
         <div className={style.title}>Доставляемый заказ</div>
         {!order && !loading && (
            <div className={style.emptyAcceptOrder}>
               Принятых заказов нет!
               <div>
                  <Link to='/courier/orders'>К списку заказов</Link>
               </div>
            </div>
         )}
         {order && !loading && (
            <div className={style.containerOrder}>
               <div className={style.infoOrder}>
                  <div>
                     <div>
                        №{order?.id} {moment(order?.date).format('DD.MM.Y H:mm')}{' '}
                     </div>
                     <div>
                        {order?.whereDelivery} {order?.addressDelivery}
                     </div>
                     <div>Доставка {order?.timeDelivery}</div>
                     <div>Тел. {order?.telephone}</div>
                  </div>
                  <div className={style.statusOrder}>
                     <Button onClick={() => handleClick(order?.id, 'Доставленный')}>
                        Доставлен
                     </Button>
                  </div>
               </div>
               <div className={style.productsOrder}>
                  <div>Столовых приборов {order?.quantityCutlery}шт.</div>
                  {order?.products?.map(detail => (
                     <div key={detail.id} className={style.detailProductOrder}>
                        {detail.quantity} x {detail.name} {detail.price}₽
                     </div>
                  ))}
               </div>
            </div>
         )}
      </div>
   )
}

export default AcceptedOrder
