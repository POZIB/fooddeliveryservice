import React, { FC, useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { Spin } from 'antd'
import { LoadingOutlined } from '@ant-design/icons'

import socket from '../../../socket'
import CourierService from '../service/CourierService'
import { IOrder } from '../../../models/IOrder'
import Order from '../components/Order'

import style from '../courierStyle.module.css'

const Orders: FC = () => {
   const navigate = useNavigate()
   const [orders, setOrders] = useState<IOrder[]>([])
   const [loading, setLoading] = useState(false)

   useEffect(() => {
      socket.on('COURIER:ORDER:NEW', (data: IOrder) => {
         setOrders(prev => [data, ...prev])
      })
      socket.on('COURIER:ORDER:CHANGE_STATUS', (data: IOrder) => {
         setOrders(prev => prev.filter(item => item.id !== data.id))
      })

      return () => {
         socket.removeListener('COURIER:ORDER:NEW')
         socket.removeListener('COURIER:ORDER:CHANGE_STATUS')
      }
   }, [orders])

   useEffect(() => {
      fetchOrders()
   }, [])

   const fetchOrders = async () => {
      setLoading(true)
      const res = await CourierService.fetchOrders()
      setOrders(res.data.rows)
      setLoading(false)
   }

   useEffect(() => {
      if (!loading) {
         if (orders?.find(item => item.status === 'В доставке')) {
            navigate('/courier/acceptOrder')
         }
      }
   }, [orders])

   return (
      <div className={style.wrapperOrders}>
         <div className={style.title}>Заказы готовые к доставке</div>
         {loading && (
            <Spin
               className='root__spiner'
               tip='Подождите...'
               indicator={<LoadingOutlined style={{ fontSize: 84 }} spin />}
               delay={200}
            />
         )}
         {!orders?.length && !loading ? (
            <div className={style.emptyOrders}>Пока готовых заказов нет</div>
         ) : (
            <div>
               {orders?.map(order => (
                  <Order key={order.id} order={order} />
               ))}
            </div>
         )}
      </div>
   )
}

export default Orders
