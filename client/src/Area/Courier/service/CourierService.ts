import { AxiosResponse } from 'axios'
import $api from '../../../http'
import { IOrder } from '../../../models/IOrder'
import { OrdersResponse } from '../../../models/response/OrdersResponse'

export default class CourierService {
   static async fetchOrders(
      page?: number,
      limit?: number
   ): Promise<AxiosResponse<OrdersResponse>> {
      return await $api.get<OrdersResponse>('/courier/orders', {
         params: { page, limit },
      })
   }

   static async fetchAcceptOrder(): Promise<AxiosResponse<IOrder>> {
      return await $api.get<IOrder>('/courier/orders/takenOrder')
   }

   static async changeStatusOrder(
      id: number,
      status?: string
   ): Promise<AxiosResponse<IOrder>> {
      return await $api.put<IOrder>('/courier/orders/acceptanceOrder', {
         id,
         status,
      })
   }
}
