import React, { FC } from 'react'
import { useNavigate } from 'react-router-dom'
import moment from 'moment'
import { Button } from 'antd'

import CourierService from '../service/CourierService'
import { IOrder } from '../../../models/IOrder'

import style from '../courierStyle.module.css'

interface IProps {
   order: IOrder
}

const Order: FC<IProps> = ({ order }) => {
   const navigate = useNavigate()

   const handlerStatusOrder = async (orderId?: number, status?: string) => {
      if (orderId) {
         try {
            const res = await CourierService.changeStatusOrder(orderId, status)
            if (res.data) {
               navigate('/courier/acceptOrder')
            }
         } catch (error) {}
      }
   }

   return (
      <div key={order.id} className={style.containerOrder}>
         <div className={style.infoOrder}>
            <div className={style.info}>
               <div>
                  №{order.id} {moment(order.date).format('DD.MM.Y H:mm')}
               </div>
               <div>
                  {order.whereDelivery} {order.addressDelivery}
               </div>
               <div>Доставка {order.timeDelivery}</div>
               <div>Тел. {order.telephone}</div>
            </div>
            <div className={style.statusOrder}>
               <div>{order.status}</div>
               <div>
                  {order.status === 'В доставке' ? (
                     <Button onClick={() => handlerStatusOrder(order.id, 'Доставленный')}>
                        Доставлено
                     </Button>
                  ) : (
                     <Button onClick={() => handlerStatusOrder(order.id, 'В доставке')}>
                        Принять в доставку
                     </Button>
                  )}
               </div>
            </div>
         </div>
         <div className={style.productsOrder}>
            <div>Столовых приборов {order.quantityCutlery}шт.</div>
            {order.products?.map(detail => (
               <div key={detail.id} className={style.detailProductOrder}>
                  {detail.quantity} x {detail.name} {detail.price}₽
               </div>
            ))}
         </div>
      </div>
   )
}

export default Order
