import React, { FC, useEffect, useState } from 'react'
import { Pagination, Spin } from 'antd'
import { LoadingOutlined } from '@ant-design/icons'

import socket from '../../../socket'
import ManagerService from '../service/ManagerService'
import { IOrder } from '../../../models/IOrder'
import SortOrders from '../components/SortOrders'
import Order from '../components/Order'

import style from '../managerStyle.module.css'
import { useSearchParams } from 'react-router-dom'

interface inputsSort {
   emailUser?: string
   statuses?: string[]
   date?: string[]
}

const Orders: FC = () => {
   const [searchParams, setSearchParams] = useSearchParams()
   const [valuesSort, setValuesSort] = useState<inputsSort>({ statuses: ['В обработке'] })
   const [page, setPage] = useState(1)
   const [limit, setLimit] = useState(10)
   const [totalCount, setTotalCount] = useState(0)
   const [orders, setOrders] = useState<IOrder[]>([])
   const [loading, setLoading] = useState(false)

   useEffect(() => {
      socket.on('MANAGER:ORDER:NEW', (data: IOrder) => {
         if (data.status && valuesSort.statuses?.includes(data.status)) {
            setOrders(prev => [data, ...prev])
            setTotalCount(prev => prev + 1)
         }
      })

      return () => {
         socket.removeListener('MANAGER:ORDER:NEW')
      }
   }, [valuesSort])

   useEffect(() => {
      socket.on('MANAGER:ORDER:CHANGE_STATUS', (data: IOrder) => {
         if (valuesSort.statuses?.includes(String(data.status))) {
            if (orders.find(item => item.id === data.id)) {
               setOrders(prev => prev.map(item => (item.id === data.id ? data : item)))
            } else {
               setOrders(prev => [data, ...prev])
               setTotalCount(prev => prev + 1)
            }
         } else {
            setOrders(prev => prev.filter(item => item.id !== data.id))
            setTotalCount(prev => prev - 1)
         }
      })

      return () => {
         socket.removeListener('MANAGER:ORDER:CHANGE_STATUS')
      }
   }, [valuesSort, orders])

   useEffect(() => {
      fetchOrders(1, limit)
      setPage(1)
   }, [valuesSort, limit])

   useEffect(() => {
      setValuesSort({
         emailUser: searchParams.get('emailUser') || undefined,
         statuses: searchParams.get('statuses')?.split(',').length
            ? searchParams.get('statuses')?.split(',')
            : ['В обработке'],
         date: searchParams.get('date')?.split(',').length
            ? searchParams.get('date')?.split(',')
            : undefined,
      })
   }, [searchParams])

   const fetchOrders = async (page: number, limit: number) => {
      try {
         setLoading(true)

         const res = await ManagerService.fetchOrders(
            valuesSort?.emailUser,
            valuesSort?.statuses,
            valuesSort?.date,
            page,
            limit
         )
         setOrders(res.data.rows)
         setTotalCount(res.data.count)
      } catch (error) {
      } finally {
         setLoading(false)
      }
   }

   const onChangePage = (page: number) => {
      fetchOrders(page, limit)
      setPage(page)
   }

   const handleChangeOrder = (order: IOrder) => {
      const _orders = orders.flatMap(item => {
         if (item.id === order.id) {
            if (valuesSort.statuses?.includes(String(order.status))) {
               return order
            } else {
               setTotalCount(prev => prev - 1)
               return []
            }
         } else {
            return item
         }
      })
      setOrders(_orders)
   }

   return (
      <div className={style.wrapperManagerOrders}>
         <SortOrders />

         {loading && (
            <Spin
               className='root__spiner'
               tip='Подождите...'
               indicator={<LoadingOutlined style={{ fontSize: 84 }} spin />}
               delay={500}
            ></Spin>
         )}

         <div className={style.containerOrders}>
            {!loading &&
               orders?.map(order => (
                  <Order key={order.id} order={order} onChange={handleChangeOrder} />
               ))}
         </div>
         <div className={style.blockPagination}>
            <Pagination
               current={page}
               onChange={onChangePage}
               pageSize={limit}
               total={totalCount}
            />
         </div>
      </div>
   )
}

export default Orders
