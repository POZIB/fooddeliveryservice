import { AxiosResponse } from 'axios'
import $api from '../../../http'
import { IOrder } from './../../../models/IOrder'
import { OrdersResponse } from './../../../models/response/OrdersResponse'

export default class ManagerService {
   static async fetchOrders(
      emailUser?: string,
      statuses?: Array<string>,
      dateInterval?: string[],
      page?: number,
      limit?: number
   ): Promise<AxiosResponse<OrdersResponse>> {
      return await $api.get<OrdersResponse>('/manager/orders', {
         params: { emailUser, statuses, dateInterval, page, limit },
      })
   }

   static async changeStatusOrder(
      id: number,
      status: string
   ): Promise<AxiosResponse<IOrder>> {
      return await $api.post<IOrder>('/manager/orders/changeStatus', { id, status })
   }
}
