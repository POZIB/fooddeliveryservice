import React, { FC, useEffect, useState } from 'react'
import moment from 'moment'
import 'moment/locale/ru'
import locale from 'antd/es/date-picker/locale/ru_RU'
import { CheckboxValueType } from 'antd/lib/checkbox/Group'
import { Button, Checkbox, DatePicker, Form, Input } from 'antd'

import style from '../managerStyle.module.css'
import { useSearchParams } from 'react-router-dom'

interface inputsSort {
   emailUser?: string
   statuses?: string[]
   date?: string[]
}

const defaultStatuses = ['В обработке']

const SortOrders: FC = props => {
   const [searchParams, setSearchParams] = useSearchParams({
      statuses: defaultStatuses,
   })
   const [inputsSort, setInputsSort] = useState<inputsSort>({
      emailUser: searchParams.get('emailUser') || undefined,
      statuses: searchParams.get('statuses')?.split(',').length
         ? searchParams.get('statuses')?.split(',')
         : defaultStatuses,
      date: searchParams.get('date')?.split(',').length
         ? searchParams.get('date')?.split(',')
         : undefined,
   })

   const handleChangeEmail = (event: React.ChangeEvent<HTMLInputElement>) => {
      setInputsSort(prev => ({
         ...prev,
         emailUser: event.target.value,
      }))
   }

   const handleChangeStatuses = (values: CheckboxValueType[]) => {
      setInputsSort(prev => ({
         ...prev,
         statuses: values as string[],
         date: prev?.date,
      }))
   }

   const handleChangeDates = (_: any, stringValue: [string, string]) => {
      setInputsSort(prev => ({
         ...prev,
         date:
            stringValue[0] && stringValue[1]
               ? [stringValue[0], stringValue[1]]
               : undefined,
      }))
   }

   const onSubmit = () => {
      if (!inputsSort) return

      const params = {} as any
      if (inputsSort.emailUser) params.emailUser = inputsSort.emailUser
      if (inputsSort.statuses) params.statuses = inputsSort.statuses?.join(',')
      if (inputsSort.date) params.date = inputsSort.date?.join(',')
      setSearchParams(params)
   }

   return (
      <div className={style.containerSortOrders}>
         <Form onFinish={onSubmit}>
            <div className={style.contentSortOrders}>
               <div className={style.inputsSort}>
                  <div className={style.emailUserAndDateOrders}>
                     <Form.Item noStyle>
                        <Input
                           allowClear
                           placeholder='Почта клиента'
                           value={inputsSort?.emailUser}
                           onChange={handleChangeEmail}
                        />
                     </Form.Item>
                     <Form.Item noStyle>
                        <DatePicker.RangePicker
                           locale={locale}
                           format='YYYY-MM-DD'
                           placeholder={['Начальная дата', 'Конечная дата']}
                           onChange={handleChangeDates}
                           value={
                              inputsSort.date
                                 ? [
                                      moment(inputsSort.date?.[0]),
                                      moment(inputsSort.date?.[1]),
                                   ]
                                 : undefined
                           }
                        />
                     </Form.Item>
                  </div>
                  <div>
                     <Form.Item noStyle>
                        <Checkbox.Group
                           onChange={handleChangeStatuses}
                           defaultValue={defaultStatuses}
                           value={inputsSort?.statuses}
                        >
                           <Checkbox value='В обработке'>В обработке</Checkbox>
                           {/* <Checkbox value='Готовится'>Готовятся</Checkbox> */}
                           <Checkbox value='Готов для доставки'>
                              Готовые для доставки
                           </Checkbox>
                           <Checkbox value='В доставке'>В доставке</Checkbox>
                           <Checkbox value='Доставленный'>Доставленные</Checkbox>
                           <Checkbox value='Отмененный'>Отмененные</Checkbox>
                        </Checkbox.Group>
                     </Form.Item>
                  </div>
               </div>

               <Form.Item noStyle>
                  <Button type='primary' htmlType='submit'>
                     Применить
                  </Button>
               </Form.Item>
            </div>
         </Form>
      </div>
   )
}

export default SortOrders
