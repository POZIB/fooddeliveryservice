import React, { FC, useState } from 'react'
import moment from 'moment'
import cn from 'classnames'
import { Button, Dropdown, Menu, Space } from 'antd'
import { DownOutlined } from '@ant-design/icons'

import ManagerService from '../service/ManagerService'
import { IOrder } from '../../../models/IOrder'

import style from '../managerStyle.module.css'

interface IPropsOrder {
   order: IOrder
   onChange: (value: IOrder) => void
}

const Order: FC<IPropsOrder> = ({ order, onChange }) => {
   const statusOrder = cn(style.statusOrder, {
      [style.statusInProcessing]: order.status === 'В обработке',
      [style.statusReadyDelivery]: order.status === 'Готов для доставки',
      [style.statusInDelivery]: order.status === 'В доставке',
      [style.statusDelivered]: order.status === 'Доставленный',
      [style.statusСanceled]: order.status === 'Отмененный',
   })

   const handleMenuClick = async (status: any, id?: number) => {
      if (!id || !status.key) return
      try {
         const res = await ManagerService.changeStatusOrder(id, status?.key)
         onChange(res.data)
      } catch (error) {}
   }

   return (
      <div className={style.containerOrder}>
         <div className={style.infoOrder}>
            <div className={style.info}>
               <div>
                  №{order.id} {moment(order.date).format('DD.MM.Y H:mm')}{' '}
                  {order.emailUser}
               </div>
               <div>
                  <div>
                     {order.whereDelivery} {order.addressDelivery}
                  </div>
                  <div>Доставка {order.timeDelivery}</div>
                  <div>Тел. {order.telephone}</div>
               </div>
               Итоговая сумма <strong>{order.total}₽ </strong>
            </div>
            <div className={style.rightBlock}>
               <div className={statusOrder}>{order.status}</div>
               {order.status !== 'Доставленный' && order.status !== 'Отмененный' && (
                  <Dropdown
                     overlay={
                        <Menu onClick={status => handleMenuClick(status, order?.id)}>
                           {order.status !== 'Готов для доставки' &&
                              order.status !== 'В доставке' && (
                                 <Menu.Item key={'Готов для доставки'}>
                                    Готов для доставки
                                 </Menu.Item>
                              )}
                           {order.status !== 'Доставленный' && (
                              <Menu.Item key={'Доставленный'}>Доставленный</Menu.Item>
                           )}
                           <Menu.Item key={'Отмененный'}>Отменить</Menu.Item>
                        </Menu>
                     }
                  >
                     <Button>
                        <Space>
                           Изменить статус
                           <DownOutlined />
                        </Space>
                     </Button>
                  </Dropdown>
               )}
               <div>Курьер: {order.courierName ? order.courierName : 'Не назначен'}</div>
            </div>
         </div>
         <div className={style.productsOrder}>
            <div>Столовых приборов {order.quantityCutlery}шт.</div>
            {order.products?.map(detail => (
               <div key={detail.id} className={style.detailProductOrder}>
                  {detail.quantity} x {detail.name} {detail.price}₽
               </div>
            ))}
         </div>
      </div>
   )
}

export default Order
