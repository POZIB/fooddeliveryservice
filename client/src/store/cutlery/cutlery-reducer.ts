import { CutleryAction, CutleryEnum, CutleryState } from './types'

const initialState: CutleryState = {
   quantityCutlery: 1,
}

export default function CutleryReducer(
   state = initialState,
   action: CutleryAction
): CutleryState {
   switch (action.type) {
      case CutleryEnum.SET_QUANTITY_CUTLERY:
         return {
            ...state,
            quantityCutlery: action.payload,
         }

      default:
         return state
   }
}
