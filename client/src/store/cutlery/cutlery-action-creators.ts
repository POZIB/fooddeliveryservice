import { CutleryEnum, SetQuantityCutleryAction } from './types'

export const CutleryActionCreators = {
   setQuantityCutlery: (quantity: number): SetQuantityCutleryAction => ({
      type: CutleryEnum.SET_QUANTITY_CUTLERY,
      payload: quantity,
   }),
}
