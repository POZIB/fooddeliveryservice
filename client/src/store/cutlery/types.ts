export interface CutleryState {
   quantityCutlery: number
}

export enum CutleryEnum {
   SET_QUANTITY_CUTLERY = 'cutlery/SET_QUANTITY_CUTLERY',
}

export interface SetQuantityCutleryAction {
   type: CutleryEnum.SET_QUANTITY_CUTLERY
   payload: number
}

export type CutleryAction = SetQuantityCutleryAction
