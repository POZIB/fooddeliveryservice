import { ICategory } from './../../models/ICategory'

export interface CategoriesState {
   categories: ICategory[]
   isCategoriesLoading: boolean
   categoryError: string
   selectCategory: ICategory | null
}

export enum CategoriesActionEnum {
   SET_CATEGORIES = 'categories/SET_CATEGORIES',
   SET_SELECT = 'categories/SET_SELECT',
   SET_IS_LOADING = 'categories/SET_IS_LOADING',
   SET_ERROR = 'categories/SET_ERROR',
   ADD = 'categories/ADD',
   EDIT = 'categories/EDIT',
   DELETE = 'categories/DELETE',
}

export interface SetCategoriesAction {
   type: CategoriesActionEnum.SET_CATEGORIES
   payload: ICategory[]
}

export interface SetIsCategoriesLoadingAction {
   type: CategoriesActionEnum.SET_IS_LOADING
   payload: boolean
}

export interface SetCategoriesErrorAction {
   type: CategoriesActionEnum.SET_ERROR
   payload: string
}

export interface SetSelectCategoryCategory {
   type: CategoriesActionEnum.SET_SELECT
   payload: ICategory | null
}

export interface AddCategoryCategory {
   type: CategoriesActionEnum.ADD
   payload: ICategory
}

export interface EditCategoryCategory {
   type: CategoriesActionEnum.EDIT
   payload: ICategory
}

export interface DeleteCategoryCategory {
   type: CategoriesActionEnum.DELETE
   payload: number
}

export type CategoriesAction =
   | SetCategoriesAction
   | SetIsCategoriesLoadingAction
   | SetCategoriesErrorAction
   | SetSelectCategoryCategory
   | AddCategoryCategory
   | EditCategoryCategory
   | DeleteCategoryCategory
