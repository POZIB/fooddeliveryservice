import { AxiosError } from 'axios'
import { AppDispatch, RootState } from '../index'
import CategoriesService from '../../services/CategoriesService'
import { ICategory } from './../../models/ICategory'
import {
   CategoriesActionEnum,
   SetCategoriesAction,
   SetIsCategoriesLoadingAction,
   SetCategoriesErrorAction,
   SetSelectCategoryCategory,
   AddCategoryCategory,
   EditCategoryCategory,
   DeleteCategoryCategory,
} from './types'

const ActionCreators = {
   setAll: (payload: ICategory[]): SetCategoriesAction => ({
      type: CategoriesActionEnum.SET_CATEGORIES,
      payload,
   }),

   add: (payload: ICategory): AddCategoryCategory => ({
      type: CategoriesActionEnum.ADD,
      payload,
   }),
   edit: (payload: ICategory): EditCategoryCategory => ({
      type: CategoriesActionEnum.EDIT,
      payload,
   }),
   delete: (payload: number): DeleteCategoryCategory => ({
      type: CategoriesActionEnum.DELETE,
      payload,
   }),

   setLoading: (flag: boolean): SetIsCategoriesLoadingAction => ({
      type: CategoriesActionEnum.SET_IS_LOADING,
      payload: flag,
   }),

   setError: (payload: string): SetCategoriesErrorAction => ({
      type: CategoriesActionEnum.SET_ERROR,
      payload,
   }),

   setSelect: (payload: ICategory | null): SetSelectCategoryCategory => ({
      type: CategoriesActionEnum.SET_SELECT,
      payload,
   }),
}

export const CategoriesActionCreators = {
   getCategories:
      (typeId?: number) => async (dispatch: AppDispatch, getState: () => RootState) => {
         try {
            if (!getState().categories.categories.length) {
               dispatch(ActionCreators.setLoading(true))
            }
            const categories = await CategoriesService.fetchCategories(typeId)
            dispatch(ActionCreators.setAll(categories.data))
         } catch (error) {
            failedRequest(error, dispatch)
         }
      },

   createCategory: (name: string, typeId: number) => async (dispatch: AppDispatch) => {
      try {
         const category = await CategoriesService.create(name, typeId)
         dispatch(ActionCreators.add(category.data))
      } catch (error) {
         failedRequest(error, dispatch)
      }
   },

   editCategory: (newCategory: ICategory) => async (dispatch: AppDispatch) => {
      try {
         const category = await CategoriesService.edit(newCategory)
         dispatch(ActionCreators.edit(category.data))
      } catch (error) {
         failedRequest(error, dispatch)
      }
   },

   deleteCategory: (id: number) => async (dispatch: AppDispatch) => {
      try {
         await CategoriesService.delete(id)
         dispatch(ActionCreators.delete(id))
      } catch (error) {
         failedRequest(error, dispatch)
      }
   },
}

const failedRequest = (error: Error | unknown, dispatch: AppDispatch) => {
   const err = error as AxiosError
   dispatch(ActionCreators.setError(err.response?.data.message))
}
