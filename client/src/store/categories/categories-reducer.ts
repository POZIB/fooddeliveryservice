import { CategoriesAction, CategoriesActionEnum, CategoriesState } from './types'

const initialState: CategoriesState = {
   categories: [],
   isCategoriesLoading: false,
   categoryError: '',
   selectCategory: null,
}

export default function CategoriesReducer(
   state = initialState,
   action: CategoriesAction
): CategoriesState {
   switch (action.type) {
      case CategoriesActionEnum.SET_CATEGORIES:
         return {
            ...state,
            categories: action.payload,
            isCategoriesLoading: false,
            categoryError: '',
         }
      case CategoriesActionEnum.ADD:
         return { ...state, categories: [...state.categories, action.payload] }
      case CategoriesActionEnum.EDIT:
         return {
            ...state,
            categories: state.categories.map(item =>
               item.id === action.payload.id ? { ...action.payload } : item
            ),
         }
      case CategoriesActionEnum.DELETE:
         return {
            ...state,
            categories: state.categories.filter(item => item.id !== action.payload),
         }
      case CategoriesActionEnum.SET_IS_LOADING:
         return { ...state, isCategoriesLoading: action.payload }
      case CategoriesActionEnum.SET_ERROR:
         return { ...state, categoryError: action.payload, isCategoriesLoading: false }
      case CategoriesActionEnum.SET_SELECT:
         return { ...state, selectCategory: action.payload }

      default:
         return state
   }
}
