import { applyMiddleware, combineReducers, compose, createStore } from 'redux'
import thunk from 'redux-thunk'
import reducers from './all-reducers'

declare global {
   interface Window {
      __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose
   }
}
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

const appReducer = combineReducers(reducers)

const rootReducer = (state: any, action: any) => {
   if (action.type === 'auth/LOGOUT') {
      state.auth = undefined
      state.basket = undefined
      state.deliveryAddress = undefined
      state.deliveryTime = undefined
      return appReducer(state, action)
   }

   return appReducer(state, action)
}

export const store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)))

export type RootState = ReturnType<typeof store.getState>

export type AppDispatch = typeof store.dispatch
