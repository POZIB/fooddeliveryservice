import { IType } from './../../models/IType'
export interface TypesState {
   types: IType[]
   isTypesLoading: boolean
   typesError: string
   selectType: IType | null
}

export enum TypesActionEnum {
   SET_TYPES = 'types/SET_TYPES',
   SET_SELECT_TYPE = 'types/SET_SELECT_TYPE',
   SET_IS_LOADING = 'types/SET_IS_LOADING',
   SET_ERROR = 'types/SET_ERROR',
   ADD = 'types/ADD',
   EDIT = 'types/EDIT',
   DELETE = 'types/DELETE',
}

export interface SetSelectTypeAction {
   type: TypesActionEnum.SET_SELECT_TYPE
   payload: IType | null
}

export interface SetTypesAction {
   type: TypesActionEnum.SET_TYPES
   payload: IType[]
}

export interface SetIsTypesLoadingAction {
   type: TypesActionEnum.SET_IS_LOADING
   payload: boolean
}

export interface SetTypesErrorAction {
   type: TypesActionEnum.SET_ERROR
   payload: string
}

export interface AddTypeAction {
   type: TypesActionEnum.ADD
   payload: IType
}

export interface EditTypeAction {
   type: TypesActionEnum.EDIT
   payload: IType
}

export interface DeleteTypeAction {
   type: TypesActionEnum.DELETE
   payload: number
}

export type TypesAction =
   | SetTypesAction
   | SetIsTypesLoadingAction
   | SetTypesErrorAction
   | SetSelectTypeAction
   | AddTypeAction
   | EditTypeAction
   | DeleteTypeAction
