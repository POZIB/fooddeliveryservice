import { IType } from './../../models/IType'
import {
   TypesActionEnum,
   SetTypesAction,
   SetTypesErrorAction,
   SetIsTypesLoadingAction,
   SetSelectTypeAction,
   AddTypeAction,
   EditTypeAction,
   DeleteTypeAction,
} from './types'
import { AxiosError } from 'axios'
import TypesService from '../../services/TypesService'
import { AppDispatch, RootState } from '../index'

const ActionCreators = {
   setAll: (payload: IType[]): SetTypesAction => ({
      type: TypesActionEnum.SET_TYPES,
      payload,
   }),
   add: (payload: IType): AddTypeAction => ({
      type: TypesActionEnum.ADD,
      payload,
   }),

   edit: (payload: IType): EditTypeAction => ({
      type: TypesActionEnum.EDIT,
      payload,
   }),
   delete: (payload: number): DeleteTypeAction => ({
      type: TypesActionEnum.DELETE,
      payload,
   }),

   setSelect: (payload: IType | null): SetSelectTypeAction => ({
      type: TypesActionEnum.SET_SELECT_TYPE,
      payload,
   }),

   setLoading: (flag: boolean): SetIsTypesLoadingAction => ({
      type: TypesActionEnum.SET_IS_LOADING,
      payload: flag,
   }),

   setError: (payload: string): SetTypesErrorAction => ({
      type: TypesActionEnum.SET_ERROR,
      payload,
   }),
}

export const TypesActionCreators = {
   getTypes: () => async (dispatch: AppDispatch, getState: () => RootState) => {
      try {
         if (!getState().types.types.length) {
            dispatch(ActionCreators.setLoading(true))
         }
         const types = await TypesService.fetchTypes()
         dispatch(ActionCreators.setAll(types.data))
      } catch (error) {
         failedRequest(error, dispatch)
      }
   },

   createType: (name: string) => async (dispatch: AppDispatch) => {
      try {
         const type = await TypesService.create(name)
         dispatch(ActionCreators.add(type.data))
      } catch (error) {
         failedRequest(error, dispatch)
      }
   },

   editType: (id: number, newName: string) => async (dispatch: AppDispatch) => {
      try {
         const type = await TypesService.edit(id, newName)
         dispatch(ActionCreators.edit(type.data))
      } catch (error) {
         failedRequest(error, dispatch)
      }
   },

   deleteType: (id: number) => async (dispatch: AppDispatch) => {
      try {
         const type = await TypesService.delete(id)
         dispatch(ActionCreators.delete(id))
      } catch (error) {
         failedRequest(error, dispatch)
      }
   },
}

const failedRequest = (error: Error | unknown, dispatch: AppDispatch) => {
   const err = error as AxiosError
   dispatch(ActionCreators.setError(err.response?.data.message))
}
