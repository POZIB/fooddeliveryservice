import { TypesState, TypesAction, TypesActionEnum } from './types'

const initialState: TypesState = {
   types: [],
   isTypesLoading: false,
   typesError: '',
   selectType: null,
}

export default function typesReducer(
   state = initialState,
   action: TypesAction
): TypesState {
   switch (action.type) {
      case TypesActionEnum.SET_TYPES:
         return { ...state, types: action.payload, isTypesLoading: false, typesError: '' }
      case TypesActionEnum.ADD:
         return { ...state, types: [...state.types, action.payload] }
      case TypesActionEnum.EDIT:
         return {
            ...state,
            types: state.types.map(item =>
               item.id === action.payload.id ? { ...action.payload } : item
            ),
         }
      case TypesActionEnum.DELETE:
         return {
            ...state,
            types: state.types.filter(item => item.id !== action.payload),
         }
      case TypesActionEnum.SET_SELECT_TYPE:
         return { ...state, selectType: action.payload }
      case TypesActionEnum.SET_IS_LOADING:
         return { ...state, isTypesLoading: action.payload }
      case TypesActionEnum.SET_ERROR:
         return { ...state, typesError: action.payload, isTypesLoading: false }

      default:
         return state
   }
}
