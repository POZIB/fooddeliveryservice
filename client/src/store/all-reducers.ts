import auth from './auth/auth-reducer'
import types from './types/types-reducer'
import categories from './categories/categories-reducer'
import products from './products/products-reducer'
import basket from './basket/basket-reducer'
import cutlery from './cutlery/cutlery-reducer'
import deliveryAddress from './deliveryAddress/deliveryAddress-reducer'
import deliveryTime from './deliveryTime/deliveryTime-reducer'

export default {
   auth,
   types,
   categories,
   products,
   basket,
   cutlery,
   deliveryAddress,
   deliveryTime,
}
