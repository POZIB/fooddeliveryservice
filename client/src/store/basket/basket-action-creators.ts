import { AxiosError, AxiosResponse } from 'axios'
import { AppDispatch } from '../index'
import BasketService from '../../services/BasketService'
import {
   BasketActionEnum,
   SetBasketErrorAction,
   SetSumBasketAction,
   SetIsBasketLoadingAction,
   SetProductsInBasketAction,
   AddProductAction,
   DeleteProductAction,
   ClearBasketAction,
   EditQuantityProductAction,
} from './types'
import { IProductBasket } from '../../models/IProductBasket'

const ActionCreators = {
   setProducts: (products: IProductBasket[]): SetProductsInBasketAction => ({
      type: BasketActionEnum.SET_PRODUCTS,
      payload: products,
   }),

   setSumBasket: (counter: number): SetSumBasketAction => ({
      type: BasketActionEnum.SET_SUM_BASKET,
      payload: counter,
   }),

   add: (payload: IProductBasket): AddProductAction => ({
      type: BasketActionEnum.ADD_PRODUCT,
      payload,
   }),
   edit: (payload: IProductBasket): EditQuantityProductAction => ({
      type: BasketActionEnum.EDIT_QUANTITY_PRODUCT,
      payload,
   }),

   delete: (payload: number): DeleteProductAction => ({
      type: BasketActionEnum.DELETE_PRODUCT,
      payload,
   }),

   clear: (): ClearBasketAction => ({
      type: BasketActionEnum.CLEAR_BASKET,
   }),

   setIsLoadingBasket: (flag: boolean): SetIsBasketLoadingAction => ({
      type: BasketActionEnum.SET_IS_LOADING,
      payload: flag,
   }),

   setBasketError: (error: string): SetBasketErrorAction => ({
      type: BasketActionEnum.SET_ERROR,
      payload: error,
   }),
}

export const BasketActionCreators = {
   setProductsInBasket: (products: IProductBasket[]) => async (dispatch: AppDispatch) => {
      dispatch(ActionCreators.setProducts(products))
   },

   getBasket: () => async (dispatch: AppDispatch) => {
      try {
         dispatch(ActionCreators.setIsLoadingBasket(true))

         const res = await BasketService.fetchBasket()
         dispatch(ActionCreators.setProducts(res.data))
      } catch (error) {
         failedRequest(error, dispatch)
      }
   },

   addInBasket: (productId: number) => async (dispatch: AppDispatch) => {
      try {
         dispatch(ActionCreators.setIsLoadingBasket(true))

         const res = await BasketService.add(productId)
         dispatch(ActionCreators.add(res.data))
      } catch (error) {
         failedRequest(error, dispatch)
      }
   },

   editQuantityProduct:
      (productId: number, quantity: number) => async (dispatch: AppDispatch) => {
         try {
            dispatch(ActionCreators.setIsLoadingBasket(true))

            const res = await BasketService.editQuantityProduct(productId, quantity)
            dispatch(ActionCreators.edit(res.data))
         } catch (error) {
            failedRequest(error, dispatch)
         }
      },

   deleteFromBasket: (productId: number) => async (dispatch: AppDispatch) => {
      try {
         dispatch(ActionCreators.setIsLoadingBasket(true))

         await BasketService.delete(productId)
         dispatch(ActionCreators.delete(productId))
      } catch (error) {
         failedRequest(error, dispatch)
      }
   },

   clearBasket: () => async (dispatch: AppDispatch) => {
      try {
         dispatch(ActionCreators.setIsLoadingBasket(true))

         await BasketService.clear()
         dispatch(ActionCreators.clear())
      } catch (error) {
         failedRequest(error, dispatch)
      }
   },
}

const failedRequest = (error: Error | unknown, dispatch: AppDispatch) => {
   const err = error as AxiosError
   dispatch(ActionCreators.setBasketError(err.response?.data.message))
}
