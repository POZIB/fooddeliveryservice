import { IProductBasket } from '../../models/IProductBasket'

export interface BasketState {
   productsInBasket: IProductBasket[]
   sumBasket: number
   isBasketLoading: boolean
   basketError: string
}

export enum BasketActionEnum {
   SET_PRODUCTS = 'basket/SET_PRODUCTS',
   SET_SUM_BASKET = 'basket/SET_SUM_BASKET',
   SET_IS_LOADING = 'basket/SET_IS_LOADING',
   SET_ERROR = 'basket/SET_ERROR',
   ADD_PRODUCT = 'basket/ADD_PRODUCT',
   DELETE_PRODUCT = 'basket/DELETE_PRODUCT',
   EDIT_QUANTITY_PRODUCT = 'basket/EDIT_QUANTITY_PRODUCT',
   CLEAR_BASKET = 'basket/CLEA',
}

export interface SetProductsInBasketAction {
   type: BasketActionEnum.SET_PRODUCTS
   payload: IProductBasket[]
}

export interface SetSumBasketAction {
   type: BasketActionEnum.SET_SUM_BASKET
   payload: number
}

export interface SetIsBasketLoadingAction {
   type: BasketActionEnum.SET_IS_LOADING
   payload: boolean
}

export interface SetBasketErrorAction {
   type: BasketActionEnum.SET_ERROR
   payload: string
}

export interface AddProductAction {
   type: BasketActionEnum.ADD_PRODUCT
   payload: IProductBasket
}

export interface DeleteProductAction {
   type: BasketActionEnum.DELETE_PRODUCT
   payload: number
}

export interface EditQuantityProductAction {
   type: BasketActionEnum.EDIT_QUANTITY_PRODUCT
   payload: IProductBasket
}

export interface ClearBasketAction {
   type: BasketActionEnum.CLEAR_BASKET
}

export type BasketAction =
   | SetProductsInBasketAction
   | SetSumBasketAction
   | SetIsBasketLoadingAction
   | SetBasketErrorAction
   | AddProductAction
   | DeleteProductAction
   | EditQuantityProductAction
   | ClearBasketAction
