import e from 'express'
import { BasketState, BasketAction, BasketActionEnum } from './types'

const initialState: BasketState = {
   productsInBasket: [],
   isBasketLoading: false,
   sumBasket: 0,
   basketError: '',
}

export default function BasketReducer(
   state = initialState,
   action: BasketAction
): BasketState {
   switch (action.type) {
      case BasketActionEnum.SET_PRODUCTS:
         return {
            ...state,
            productsInBasket: action.payload,
            sumBasket: action.payload.reduce(
               (acc, item) => acc + item.quantity * item.price,
               0
            ),
            isBasketLoading: false,
            basketError: '',
         }

      case BasketActionEnum.ADD_PRODUCT:
         return {
            ...state,
            productsInBasket: [
               { ...action.payload, quantity: 1 },
               ...state.productsInBasket,
            ],
            sumBasket: Number(state.sumBasket) + Number(action.payload.price),
            isBasketLoading: false,
            basketError: '',
         }

      case BasketActionEnum.DELETE_PRODUCT:
         const product = state.productsInBasket.find(item => item.id === action.payload)
         const priceProduct = (product?.price || 0) * (product?.quantity || 0)
         return {
            ...state,
            productsInBasket: state.productsInBasket.filter(
               item => item.id !== action.payload
            ),
            sumBasket: Number(state.sumBasket) - Number(priceProduct),
            isBasketLoading: false,
            basketError: '',
         }

      case BasketActionEnum.EDIT_QUANTITY_PRODUCT:
         const products = state.productsInBasket.flatMap(item => {
            if (item.id === action.payload.id) {
               if (action.payload.quantity !== 0) {
                  return { ...action.payload }
               } else return []
            } else {
               return item
            }
         })

         const sum = products.reduce((acc, item) => acc + item.quantity * item.price, 0)

         return {
            ...state,
            sumBasket: sum,
            productsInBasket: products,
            isBasketLoading: false,
            basketError: '',
         }

      case BasketActionEnum.CLEAR_BASKET:
         return {
            ...state,
            sumBasket: 0,
            productsInBasket: [],
            isBasketLoading: false,
            basketError: '',
         }

      case BasketActionEnum.SET_SUM_BASKET:
         return { ...state, sumBasket: action.payload }
      case BasketActionEnum.SET_IS_LOADING:
         return { ...state, isBasketLoading: action.payload }
      case BasketActionEnum.SET_ERROR:
         return { ...state, basketError: action.payload, isBasketLoading: false }

      default:
         return state
   }
}
