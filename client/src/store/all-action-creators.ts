import { AuthActionCreators } from './auth/auth-action-creators'
import { BasketActionCreators } from './basket/basket-action-creators'
import { CategoriesActionCreators } from './categories/categories-action-creators'
import { ProductsActionCreators } from './products/products-action-creators'
import { TypesActionCreators } from './types/types-action-creators'
import { DeliveryAddressActionCreators } from './deliveryAddress/deliveryAddress-action-creators'
import { DeliveryTimeActionCreators } from './deliveryTime/deliveryTime-action-creators'
import { CutleryActionCreators } from './cutlery/cutlery-action-creators'

export const AllActionCreators = {
   ...AuthActionCreators,
   ...TypesActionCreators,
   ...CategoriesActionCreators,
   ...ProductsActionCreators,
   ...BasketActionCreators,
   ...CutleryActionCreators,
   ...DeliveryAddressActionCreators,
   ...DeliveryTimeActionCreators,
}
