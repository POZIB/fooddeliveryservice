import { ProductsAction, ProductsActionEnum, ProductsState } from './types'

const initialState: ProductsState = {
   products: [],
   isProductsLoading: false,
   productsError: '',
   page: undefined,
   limit: undefined,
   totalCount: 0,
   isSubmit: false,
}

export default function ProductsReducer(
   state = initialState,
   action: ProductsAction
): ProductsState {
   switch (action.type) {
      case ProductsActionEnum.SET_PRODUCTS:
         return {
            ...state,
            products: action.payload,
            isProductsLoading: false,
            productsError: '',
         }

      case ProductsActionEnum.ADD:
         return {
            ...state,
            products: [action.payload, ...state.products],
            totalCount: ++state.totalCount,
            isSubmit: true,
         }
      case ProductsActionEnum.EDIT:
         return {
            ...state,
            products: state.products.map(item =>
               item.id === action.payload.id ? { ...action.payload } : item
            ),
            isSubmit: true,
         }
      case ProductsActionEnum.DELETE:
         return {
            ...state,
            products: state.products.filter(item => item.id !== action.payload),
            totalCount: state.totalCount > 0 ? --state.totalCount : 0,
            isSubmit: true,
         }

      case ProductsActionEnum.SET_IS_LOADING:
         return { ...state, isProductsLoading: action.payload }
      case ProductsActionEnum.SET_ERROR:
         return { ...state, productsError: action.payload, isProductsLoading: false }
      case ProductsActionEnum.SET_PAGE:
         return { ...state, page: action.payload }
      case ProductsActionEnum.SET_TOTAL_COUNT:
         return { ...state, totalCount: action.payload }
      case ProductsActionEnum.SET_LIMIT:
         return { ...state, limit: action.payload }
      case ProductsActionEnum.SET_IS_SUBMIT:
         return { ...state, isSubmit: action.payload }
      case ProductsActionEnum.SET_RAITING:
         return {
            ...state,
            products: state.products.map(item =>
               item.id === action.payload.productId
                  ? { ...item, rating: action.payload.raiting }
                  : item
            ),
         }

      default:
         return state
   }
}
