import { AxiosError } from 'axios'
import { AppDispatch, RootState } from '../index'
import ProductsService from '../../services/ProductsService'
import { IProduct } from './../../models/IProduct'
import {
   ProductsActionEnum,
   SetLimitAction,
   SetPageAction,
   SetProductsAction,
   SetProductsErrorAction,
   SetIsProductsLoadingAction,
   SetTotalCountAction,
   AddAction,
   DeleteAction,
   EditAction,
   SetRaitingAction,
   SetIsSubmitAction,
} from './types'

const ActionCreators = {
   setAll: (payload: IProduct[]): SetProductsAction => ({
      type: ProductsActionEnum.SET_PRODUCTS,
      payload,
   }),

   add: (payload: IProduct): AddAction => ({
      type: ProductsActionEnum.ADD,
      payload,
   }),
   edit: (payload: IProduct): EditAction => ({
      type: ProductsActionEnum.EDIT,
      payload,
   }),

   delete: (payload: number): DeleteAction => ({
      type: ProductsActionEnum.DELETE,
      payload,
   }),

   setLoading: (flag: boolean): SetIsProductsLoadingAction => ({
      type: ProductsActionEnum.SET_IS_LOADING,
      payload: flag,
   }),

   setError: (payload: string): SetProductsErrorAction => ({
      type: ProductsActionEnum.SET_ERROR,
      payload,
   }),

   setPage: (payload: number | undefined): SetPageAction => ({
      type: ProductsActionEnum.SET_PAGE,
      payload,
   }),

   setTotalCount: (payload: number): SetTotalCountAction => ({
      type: ProductsActionEnum.SET_TOTAL_COUNT,
      payload,
   }),

   setLimit: (payload: number | undefined): SetLimitAction => ({
      type: ProductsActionEnum.SET_LIMIT,
      payload,
   }),

   setIsSubmitProduct: (payload: boolean): SetIsSubmitAction => ({
      type: ProductsActionEnum.SET_IS_SUBMIT,
      payload,
   }),

   setRaiting: (productId: number, raiting: number): SetRaitingAction => ({
      type: ProductsActionEnum.SET_RAITING,
      payload: { productId, raiting },
   }),
}

export const ProductsActionCreators = {
   setIsSubmitProduct: (payload: boolean) => async (dispatch: AppDispatch) => {
      dispatch(ActionCreators.setIsSubmitProduct(payload))
   },

   setPage: (payload?: number) => async (dispatch: AppDispatch) => {
      dispatch(ActionCreators.setPage(payload))
   },

   setLimit: (payload?: number) => async (dispatch: AppDispatch) => {
      dispatch(ActionCreators.setLimit(payload))
   },

   setTotalCount: (payload: number) => async (dispatch: AppDispatch) => {
      dispatch(ActionCreators.setTotalCount(payload))
   },

   getProducts:
      (typeId?: number, categoryId?: number, page?: number, limit?: number) =>
      async (dispatch: AppDispatch, getState: () => RootState) => {
         try {
            if (!getState().products.products.length) {
               dispatch(ActionCreators.setLoading(true))
            }
            const products = await ProductsService.fetchProducts(
               typeId,
               categoryId,
               page,
               limit
            )
            dispatch(ActionCreators.setAll(products.data.rows))
            dispatch(ActionCreators.setTotalCount(products.data.count))
         } catch (error) {
            failedRequest(error, dispatch)
         }
      },

   createProduct: (product: FormData) => async (dispatch: AppDispatch) => {
      try {
         const newProduct = await ProductsService.create(product)
         dispatch(ActionCreators.add(newProduct.data))
      } catch (error) {
         failedRequest(error, dispatch)
      }
   },

   editProduct: (product: FormData) => async (dispatch: AppDispatch) => {
      try {
         const updateProduct = await ProductsService.edit(product)
         dispatch(ActionCreators.edit(updateProduct.data))
      } catch (error) {
         failedRequest(error, dispatch)
      }
   },

   deleteProduct: (id: number) => async (dispatch: AppDispatch) => {
      try {
         await ProductsService.delete(id)
         dispatch(ActionCreators.delete(id))
      } catch (error) {
         failedRequest(error, dispatch)
      }
   },

   changeRaiting:
      (productId: number, raiting: number) => async (dispatch: AppDispatch) => {
         try {
            const res = await ProductsService.rating(productId, raiting)

            if (res.data) {
               dispatch(ActionCreators.setRaiting(productId, res.data))
            }
         } catch (error) {
            failedRequest(error, dispatch)
         }
      },
}

const failedRequest = (error: Error | unknown, dispatch: AppDispatch) => {
   const err = error as AxiosError
   dispatch(ActionCreators.setError(err.response?.data.message))
}
