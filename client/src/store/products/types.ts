import { IProduct } from './../../models/IProduct'

export interface ProductsState {
   products: IProduct[]
   isProductsLoading: boolean
   productsError: string
   page: number | undefined
   limit: number | undefined
   totalCount: number
   isSubmit: boolean
}

export enum ProductsActionEnum {
   SET_PRODUCTS = 'product/SET_PRODUCTS',
   SET_IS_LOADING = 'product/SET_IS_LOADING',
   SET_ERROR = 'product/SET_ERROR',
   SET_PAGE = 'product/SET_PAGE',
   SET_TOTAL_COUNT = 'product/SET_TOTAL_COUNT',
   SET_LIMIT = 'product/SET_LIMIT',
   ADD = 'product/ADD',
   EDIT = 'product/EDIT',
   DELETE = 'product/DELETE',
   SET_IS_SUBMIT = 'product/SET_IS_SUBMIT',
   SET_RAITING = 'product/SET_RAITING',
}

export interface SetProductsAction {
   type: ProductsActionEnum.SET_PRODUCTS
   payload: IProduct[]
}

export interface SetIsProductsLoadingAction {
   type: ProductsActionEnum.SET_IS_LOADING
   payload: boolean
}

export interface SetProductsErrorAction {
   type: ProductsActionEnum.SET_ERROR
   payload: string
}

export interface SetPageAction {
   type: ProductsActionEnum.SET_PAGE
   payload: number | undefined
}

export interface SetTotalCountAction {
   type: ProductsActionEnum.SET_TOTAL_COUNT
   payload: number
}

export interface SetLimitAction {
   type: ProductsActionEnum.SET_LIMIT
   payload: number | undefined
}

export interface AddAction {
   type: ProductsActionEnum.ADD
   payload: IProduct
}

export interface EditAction {
   type: ProductsActionEnum.EDIT
   payload: IProduct
}

export interface DeleteAction {
   type: ProductsActionEnum.DELETE
   payload: number
}

export interface SetIsSubmitAction {
   type: ProductsActionEnum.SET_IS_SUBMIT
   payload: boolean
}

export interface SetRaitingAction {
   type: ProductsActionEnum.SET_RAITING
   payload: { productId: number; raiting: number }
}

export type ProductsAction =
   | SetProductsAction
   | SetIsProductsLoadingAction
   | SetProductsErrorAction
   | SetPageAction
   | SetTotalCountAction
   | SetLimitAction
   | AddAction
   | EditAction
   | DeleteAction
   | SetIsSubmitAction
   | SetRaitingAction
