import { DeliveryTimeEnum, SetDeliveryTimeAction } from './types'
import { IDeliveryTime } from './../../models/IDeliveryTime'

export const DeliveryTimeActionCreators = {
   setDeliveryTime: (deliveryTime?: IDeliveryTime): SetDeliveryTimeAction => ({
      type: DeliveryTimeEnum.SET_DELIVERY_TIME,
      payload: deliveryTime,
   }),
}
