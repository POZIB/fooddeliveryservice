import { IDeliveryTime } from './../../models/IDeliveryTime'

export interface DeliveryTimeState {
   deliveryTime?: IDeliveryTime
}

export enum DeliveryTimeEnum {
   SET_DELIVERY_TIME = 'deliveryTime/SET_DELIVERY_TIME',
}

export interface SetDeliveryTimeAction {
   type: DeliveryTimeEnum.SET_DELIVERY_TIME
   payload: IDeliveryTime | undefined
}

export type DeliveryTimeAction = SetDeliveryTimeAction
