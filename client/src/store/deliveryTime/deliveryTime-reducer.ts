import { DeliveryTimeAction, DeliveryTimeEnum, DeliveryTimeState } from './types'

const initialState: DeliveryTimeState = {
   deliveryTime: undefined,
}

export default function DeliveryTimeReducer(
   state = initialState,
   action: DeliveryTimeAction
): DeliveryTimeState {
   switch (action.type) {
      case DeliveryTimeEnum.SET_DELIVERY_TIME:
         return {
            ...state,
            deliveryTime: action.payload,
         }

      default:
         return state
   }
}
