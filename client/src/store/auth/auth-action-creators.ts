import { AuthResponse } from '../../models/response/AuthResponse'
import { AxiosError, AxiosResponse } from 'axios'
import { AppDispatch } from '..'
import { IUser } from '../../models/IUser'
import AuthService from '../../services/AuthService'
import {
   SetUserAction,
   SetAuthAction,
   AuthActionEnum,
   SetIsAuthLoadingAction,
   SetAuthErrorAction,
   SetIsOpenModalAuthAction,
} from './types'

export const AuthActionCreators = {
   setIsAuth: (isAuth: boolean): SetAuthAction => ({
      type: AuthActionEnum.SET_IS_AUTH,
      payload: isAuth,
   }),

   setUser: (user: IUser | null): SetUserAction => ({
      type: AuthActionEnum.SET_USER,
      payload: user,
   }),

   setIsLoading: (flag: boolean): SetIsAuthLoadingAction => ({
      type: AuthActionEnum.SET_IS_LOADING,
      payload: flag,
   }),

   setAuthError: (payload: string): SetAuthErrorAction => ({
      type: AuthActionEnum.SET_ERROR,
      payload,
   }),

   setIsOpenModalAuth: (flag: boolean): SetIsOpenModalAuthAction => ({
      type: AuthActionEnum.SET_IS_OPEN_MODAL,
      payload: flag,
   }),

   login: (email: string, password: string) => async (dispatch: AppDispatch) => {
      try {
         const response = await AuthService.login(email, password)
         localStorage.setItem('token', response.data.accessToken)

         successfulRequest(response, dispatch)
      } catch (error) {
         failedRequest(error, dispatch)
      }
   },

   logout: () => async (dispatch: AppDispatch) => {
      try {
         await AuthService.logout()
         localStorage.removeItem('token')

         dispatch({ type: AuthActionEnum.LOGOUT })
      } catch (error) {
         failedRequest(error, dispatch)
      }
   },

   registration: (email: string, password: string) => async (dispatch: AppDispatch) => {
      try {
         const response = await AuthService.registration(email, password)
         localStorage.setItem('token', response.data.accessToken)

         successfulRequest(response, dispatch)
      } catch (error) {
         failedRequest(error, dispatch)
      }
   },

   checkAuth: () => async (dispatch: AppDispatch) => {
      try {
         dispatch(AuthActionCreators.setIsLoading(true))
         if (localStorage.getItem('token')) {
            const response = await AuthService.refresh()
            localStorage.setItem('token', response.data.accessToken)
            successfulRequest(response, dispatch)
         } else {
            dispatch(AuthActionCreators.setIsLoading(false))
         }
      } catch (error) {
         failedRequest(error, dispatch)
      }
   },
}

const successfulRequest = (
   response: AxiosResponse<AuthResponse, any>,
   dispatch: AppDispatch
) => {
   dispatch(AuthActionCreators.setIsAuth(true))
   dispatch(AuthActionCreators.setUser(response.data.user))
   dispatch(AuthActionCreators.setIsLoading(false))
}

const failedRequest = (error: Error | unknown, dispatch: AppDispatch) => {
   const err = error as AxiosError
   dispatch(AuthActionCreators.setAuthError(err.response?.data.message))
}
