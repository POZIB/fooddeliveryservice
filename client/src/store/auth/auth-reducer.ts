import { AuthAction, AuthActionEnum, AuthState } from './types'

const initialState: AuthState = {
   isAuth: false,
   user: null,
   isAuthLoading: true,
   authError: '',
   isOpenModalAuth: false,
}

export default function authReducer(state = initialState, action: AuthAction): AuthState {
   switch (action.type) {
      case AuthActionEnum.SET_IS_AUTH:
         return { ...state, isAuth: action.payload }
      case AuthActionEnum.SET_USER:
         return {
            ...state,
            user: action.payload,
            isAuthLoading: false,
            authError: '',
            isOpenModalAuth: false,
         }
      case AuthActionEnum.SET_IS_LOADING:
         return { ...state, isAuthLoading: action.payload }
      case AuthActionEnum.SET_ERROR:
         return { ...state, authError: action.payload, isAuthLoading: false }
      case AuthActionEnum.SET_IS_OPEN_MODAL:
         return { ...state, isOpenModalAuth: action.payload }
      case AuthActionEnum.LOGOUT:
         return { ...state, isAuthLoading: false }

      default:
         return state
   }
}
