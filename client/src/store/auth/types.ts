import { IUser } from '../../models/IUser'

export interface AuthState {
   isAuth: boolean
   user: IUser | null
   isAuthLoading: boolean
   authError: string
   isOpenModalAuth: boolean
}

export enum AuthActionEnum {
   SET_IS_AUTH = 'auth/SET_IS_AUTH',
   SET_USER = 'auth/SET_USER',
   SET_IS_LOADING = 'auth/SET_IS_LOADING',
   SET_ERROR = 'auth/SET_ERROR',
   SET_IS_OPEN_MODAL = 'auth/SET_IS_OPEN_MODAL',
   LOGOUT = 'auth/LOGOUT',
}

export interface SetAuthAction {
   type: AuthActionEnum.SET_IS_AUTH
   payload: boolean
}

export interface SetUserAction {
   type: AuthActionEnum.SET_USER
   payload: IUser | null
}

export interface SetIsAuthLoadingAction {
   type: AuthActionEnum.SET_IS_LOADING
   payload: boolean
}

export interface SetAuthErrorAction {
   type: AuthActionEnum.SET_ERROR
   payload: string
}

export interface SetIsOpenModalAuthAction {
   type: AuthActionEnum.SET_IS_OPEN_MODAL
   payload: boolean
}

export interface LogoutAction {
   type: AuthActionEnum.LOGOUT
}

export type AuthAction =
   | SetAuthAction
   | SetUserAction
   | SetIsAuthLoadingAction
   | SetAuthErrorAction
   | SetIsOpenModalAuthAction
   | LogoutAction
