import {
   SetDeliveryAddressessAction,
   DeliveryAddressEnum,
   SetDeliveryAddressLoadingAction,
   SetDeliveryAddressErrorAction,
   SetSelectDeliveryAddressAction,
   AddDeliveryAddressAction,
   EditDeliveryAddressAction,
   DeleteDeliveryAddressAction,
} from './types'
import { IDeliveryAddress } from '../../models/IDeliveryAddress'
import { AppDispatch } from '../index'
import DeliveryAddressService from '../../services/DeliveryAddress'
import { AxiosError } from 'axios'

const ActionCreators = {
   setAll: (deliveryAddress: IDeliveryAddress[]): SetDeliveryAddressessAction => ({
      type: DeliveryAddressEnum.SET_DELIVERY_ADDRESSES,
      payload: deliveryAddress,
   }),

   add: (deliveryAddress: IDeliveryAddress): AddDeliveryAddressAction => ({
      type: DeliveryAddressEnum.ADD,
      payload: deliveryAddress,
   }),

   edit: (deliveryAddress: IDeliveryAddress): EditDeliveryAddressAction => ({
      type: DeliveryAddressEnum.EDIT,
      payload: deliveryAddress,
   }),

   delete: (id: number): DeleteDeliveryAddressAction => ({
      type: DeliveryAddressEnum.DELETE,
      payload: id,
   }),

   setLoading: (flag: boolean): SetDeliveryAddressLoadingAction => ({
      type: DeliveryAddressEnum.SET_IS_LOADING,
      payload: flag,
   }),

   setError: (error: string): SetDeliveryAddressErrorAction => ({
      type: DeliveryAddressEnum.SET_ERROR,
      payload: error,
   }),

   setSelect: (deliveryAddress: IDeliveryAddress): SetSelectDeliveryAddressAction => ({
      type: DeliveryAddressEnum.SET_SELECT,
      payload: deliveryAddress,
   }),
}

export const DeliveryAddressActionCreators = {
   setSelectDeliveryAddress:
      (payload: IDeliveryAddress) => async (dispatch: AppDispatch) => {
         dispatch(ActionCreators.setSelect(payload))
      },

   getDeliveryAddressess: () => async (dispatch: AppDispatch) => {
      try {
         dispatch(ActionCreators.setLoading(true))
         const addresses = await DeliveryAddressService.fetchAddresses()

         dispatch(ActionCreators.setAll(addresses.data))
      } catch (error) {
         failedRequest(error, dispatch)
      }
   },

   createDeliveryAddress: (address: string) => async (dispatch: AppDispatch) => {
      try {
         dispatch(ActionCreators.setLoading(true))
         const newAddress = await DeliveryAddressService.create(address)

         dispatch(ActionCreators.add(newAddress.data))
      } catch (error) {
         failedRequest(error, dispatch)
      }
   },

   editDeliveryAddress: (info: IDeliveryAddress) => async (dispatch: AppDispatch) => {
      try {
         dispatch(ActionCreators.setLoading(true))
         const address = await DeliveryAddressService.edit(info)
         dispatch(ActionCreators.edit(address.data))
      } catch (error) {
         failedRequest(error, dispatch)
      }
   },

   deleteDeliveryAddress: (id: number) => async (dispatch: AppDispatch) => {
      try {
         await DeliveryAddressService.delete(id)
         dispatch(ActionCreators.delete(id))
      } catch (error) {
         failedRequest(error, dispatch)
      }
   },
}

const failedRequest = (error: Error | unknown, dispatch: AppDispatch) => {
   const err = error as AxiosError
   dispatch(ActionCreators.setError(err.response?.data.message))
}
