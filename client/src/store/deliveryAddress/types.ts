import { IDeliveryAddress } from '../../models/IDeliveryAddress'

export interface DeliveryAddressState {
   deliveryAddressess: IDeliveryAddress[]
   isDeliveryAddressLoading: boolean
   deliveryAddressError: string
   selectDeliveryAddress: IDeliveryAddress | undefined
}

export enum DeliveryAddressEnum {
   SET_DELIVERY_ADDRESSES = 'delivetyAddress/DELIVERY_ADDRESSES',
   SET_IS_LOADING = 'delivetyAddress/SET_IS_LOADING',
   SET_ERROR = 'delivetyAddress/SET_ERROR',
   SET_SELECT = 'delivetyAddress/SET_SELECT',
   ADD = 'delivetyAddress/ADD',
   EDIT = 'delivetyAddress/EDIT',
   DELETE = 'delivetyAddress/DELETE',
}

export interface SetDeliveryAddressessAction {
   type: DeliveryAddressEnum.SET_DELIVERY_ADDRESSES
   payload: IDeliveryAddress[]
}

export interface SetDeliveryAddressLoadingAction {
   type: DeliveryAddressEnum.SET_IS_LOADING
   payload: boolean
}

export interface SetDeliveryAddressErrorAction {
   type: DeliveryAddressEnum.SET_ERROR
   payload: string
}

export interface SetSelectDeliveryAddressAction {
   type: DeliveryAddressEnum.SET_SELECT
   payload: IDeliveryAddress
}

export interface AddDeliveryAddressAction {
   type: DeliveryAddressEnum.ADD
   payload: IDeliveryAddress
}

export interface EditDeliveryAddressAction {
   type: DeliveryAddressEnum.EDIT
   payload: IDeliveryAddress
}

export interface DeleteDeliveryAddressAction {
   type: DeliveryAddressEnum.DELETE
   payload: number
}

export type DeliveryAddressAction =
   | SetDeliveryAddressessAction
   | SetDeliveryAddressLoadingAction
   | SetDeliveryAddressErrorAction
   | SetSelectDeliveryAddressAction
   | AddDeliveryAddressAction
   | EditDeliveryAddressAction
   | DeleteDeliveryAddressAction
