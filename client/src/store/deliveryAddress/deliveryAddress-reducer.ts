import { DeliveryAddressAction, DeliveryAddressEnum, DeliveryAddressState } from './types'

const initialState: DeliveryAddressState = {
   deliveryAddressess: [],
   isDeliveryAddressLoading: false,
   deliveryAddressError: '',
   selectDeliveryAddress: undefined,
}

export default function DeliveryAddressReducer(
   state = initialState,
   action: DeliveryAddressAction
): DeliveryAddressState {
   switch (action.type) {
      case DeliveryAddressEnum.SET_DELIVERY_ADDRESSES:
         return {
            ...state,
            deliveryAddressess: action.payload,
            isDeliveryAddressLoading: false,
            deliveryAddressError: '',
         }
      case DeliveryAddressEnum.SET_IS_LOADING:
         return { ...state, isDeliveryAddressLoading: action.payload }
      case DeliveryAddressEnum.SET_ERROR:
         return {
            ...state,
            deliveryAddressError: action.payload,
            isDeliveryAddressLoading: false,
         }
      case DeliveryAddressEnum.SET_SELECT:
         return {
            ...state,
            selectDeliveryAddress: action.payload,
            isDeliveryAddressLoading: false,
         }
      case DeliveryAddressEnum.ADD:
         return {
            ...state,
            deliveryAddressess: [...state.deliveryAddressess, action.payload].sort(
               (a, b) => b.id - a.id
            ),
            isDeliveryAddressLoading: false,
         }
      case DeliveryAddressEnum.EDIT:
         return {
            ...state,
            deliveryAddressess: state.deliveryAddressess.map(item =>
               item.id === action.payload.id ? { ...action.payload } : item
            ),
            isDeliveryAddressLoading: false,
         }
      case DeliveryAddressEnum.DELETE:
         return {
            ...state,
            deliveryAddressess: state.deliveryAddressess.filter(
               item => item.id !== action.payload
            ),
         }

      default:
         return state
   }
}
