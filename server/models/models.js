const sequelize = require('../db')
const { DataTypes, NOW } = require('sequelize')

const Users = sequelize.define('users', {
   id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
   firstName: { type: DataTypes.STRING },
   lastName: { type: DataTypes.STRING },
   email: { type: DataTypes.STRING, unique: true, allowNull: false },
   password: { type: DataTypes.STRING, allowNull: false },
   isActivated: { type: DataTypes.BOOLEAN, defaultValue: false },
   activationLink: { type: DataTypes.STRING },
})

const Roles = sequelize.define('roles', {
   id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
   value: { type: DataTypes.STRING, defaultValue: "CLIENT" }
})

const UserRefRole = sequelize.define('userRefRole', {
   id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
   userId: { type: DataTypes.INTEGER, primaryKey: true },
   roleId: { type: DataTypes.INTEGER, primaryKey: true },
})

const Token = sequelize.define('token', {
   id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
   refreshToken: { type: DataTypes.STRING(1000), allowNull: false },
})




const Types = sequelize.define('types', {
   id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
   name: { type: DataTypes.STRING, unique: true, allowNull: false }
})

const Categories = sequelize.define('categories', {
   id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
   name: { type: DataTypes.STRING, unique: true, allowNull: false }
})

const Products = sequelize.define('products', {
   id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
   name: { type: DataTypes.STRING, allowNull: false },
   price: { type: DataTypes.DECIMAL, allowNull: false },
   img: { type: DataTypes.STRING },
   description: { type: DataTypes.STRING },
   composition: { type: DataTypes.STRING },
   weight: { type: DataTypes.STRING },
   rating: { type: DataTypes.FLOAT, defaultValue: 0 }
})

const Rating = sequelize.define('rating', {
   id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
   rate: { type: DataTypes.FLOAT, allowNull: false }
})


const Basket = sequelize.define('basket', {
   id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
})

const BasketDetails = sequelize.define('basketDetails', {
   id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
   quantity: { type: DataTypes.INTEGER, defaultValue: 1 }
})

const Orders = sequelize.define('orders', {
   id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
   date: { type: DataTypes.DATE, defaultValue: NOW },
   total: { type: DataTypes.DECIMAL },
   status: { type: DataTypes.STRING, defaultValue: "В обработке" },
   quantityCutlery: { type: DataTypes.INTEGER, defaultValue: 1 },
   paymentMethod: { type: DataTypes.STRING },
   timeDelivery: { type: DataTypes.STRING, allowNull: false },
   telephone: { type: DataTypes.STRING, allowNull: false },
   addressDelivery: { type: DataTypes.STRING, allowNull: false },
   whereDelivery: { type: DataTypes.STRING, allowNull: false },
   intercom: { type: DataTypes.STRING, allowNull: false },
   comments: { type: DataTypes.STRING },
   courierId: { type: DataTypes.INTEGER },
   courierName: { type: DataTypes.STRING }
})

const OrderDetails = sequelize.define('orderDetails', {
   id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
   quantity: { type: DataTypes.INTEGER, defaultValue: 1 }
})


const DeliveryAddress = sequelize.define('deliveryAddress', {
   id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
   address: { type: DataTypes.STRING, allowNull: false },
   telephoneNumber: { type: DataTypes.STRING },
   apartmentOrHouse: { type: DataTypes.STRING, defaultValue: "Apartment" },
   apartment: { type: DataTypes.STRING },
   storey: { type: DataTypes.STRING },
   entrance: { type: DataTypes.STRING },
   intercom: { type: DataTypes.BOOLEAN, defaultValue: true },
   comments: { type: DataTypes.STRING }
})


Users.hasOne(Basket)
Basket.belongsTo(Users)

Users.hasMany(Orders)
Orders.belongsTo(Users)

Types.hasMany(Products)
Products.belongsTo(Types)

Categories.hasMany(Products)
Products.belongsTo(Categories)

Types.hasOne(Categories)
Categories.belongsTo(Types)

Basket.belongsToMany(Products, { through: BasketDetails })
Products.belongsToMany(Basket, { through: BasketDetails })

Users.hasMany(Rating)
Rating.belongsTo(Users)

Products.hasMany(Rating)
Rating.belongsTo(Products)

Orders.belongsToMany(Products, { through: OrderDetails })
Products.belongsToMany(Orders, { through: OrderDetails })

Users.hasMany(DeliveryAddress)
DeliveryAddress.belongsTo(Users)


Users.hasMany(Token)
Token.belongsTo(Users)

Users.belongsToMany(Roles, { through: UserRefRole, foreignKey: 'userId', otherKey: 'roleId' })
// sequelize.sync({ alter: true });

// sequelize.sync({ force: true })

module.exports = {
   Users, Roles, Token, Types, Categories, Basket, BasketDetails, Products, Orders, OrderDetails, DeliveryAddress, Rating
}