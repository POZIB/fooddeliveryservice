const Router = require('express')
const authMiddleware = require('../../middlewares/authMiddleware')
const checkRoleMiddleware = require('../../middlewares/checkRoleMiddleware')
const AdminController = require('../../controllers/adminController')


const router = new Router()

router.get('/', authMiddleware, checkRoleMiddleware(["ADMIN"]), AdminController.getRoles)
router.post('/', authMiddleware, checkRoleMiddleware(["ADMIN"]), AdminController.createRole)
router.delete('/', authMiddleware, checkRoleMiddleware(["ADMIN"]), AdminController.deleteRole)


module.exports = router
