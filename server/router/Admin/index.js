const Router = require('express')


const usersRouter = require('./UserRouter')
const rolesRouter = require('./RolesRouter')
const typesRouter = require('./TypesRouter')
const categorisRouter = require('./CategoriesRouter')
const productsRouter = require('./ProductsRouter')

const router = new Router()

router.use('/users', usersRouter)
router.use('/roles', rolesRouter)
router.use('/types', typesRouter)
router.use('/categories', categorisRouter)
router.use('/products', productsRouter)


module.exports = router