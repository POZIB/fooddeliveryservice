const Router = require('express')
const authMiddleware = require('../../middlewares/authMiddleware')
const checkRoleMiddleware = require('../../middlewares/checkRoleMiddleware')
const CategoriesController = require('../../controllers/categoriesController')


const router = new Router()

router.post('/', authMiddleware, checkRoleMiddleware(["ADMIN"]), CategoriesController.create)
router.put('/', authMiddleware, checkRoleMiddleware(["ADMIN"]), CategoriesController.edit)
router.delete('/', authMiddleware, checkRoleMiddleware(["ADMIN"]), CategoriesController.delete)


module.exports = router
