const Router = require('express')
const authMiddleware = require('../../middlewares/authMiddleware')
const checkRoleMiddleware = require('../../middlewares/checkRoleMiddleware')
const ProductsController = require('../../controllers/productsController')

const router = new Router()

router.post('/', authMiddleware, checkRoleMiddleware(["ADMIN"]), ProductsController.create)
router.put('/', authMiddleware, checkRoleMiddleware(["ADMIN"]), ProductsController.edit)
router.delete('/', authMiddleware, checkRoleMiddleware(["ADMIN"]), ProductsController.delete)


module.exports = router