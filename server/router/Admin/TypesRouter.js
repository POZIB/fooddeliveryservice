const Router = require('express')
const authMiddleware = require('../../middlewares/authMiddleware')
const checkRoleMiddleware = require('../../middlewares/checkRoleMiddleware')
const TypesController = require('../../controllers/typesController')

const router = new Router()

router.post('/', authMiddleware, checkRoleMiddleware(["ADMIN"]), TypesController.create)
router.put('/', authMiddleware, checkRoleMiddleware(["ADMIN"]), TypesController.edit)
router.delete('/', authMiddleware, checkRoleMiddleware(["ADMIN"]), TypesController.delete)


module.exports = router