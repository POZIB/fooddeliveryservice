const Router = require('express')
const authMiddleware = require('../../middlewares/authMiddleware')
const checkRoleMiddleware = require('../../middlewares/checkRoleMiddleware')
const AdminController = require('../../controllers/adminController')

const router = new Router()

router.get('/', authMiddleware, checkRoleMiddleware(["ADMIN"]), AdminController.getUsers)
router.put('/role', authMiddleware, checkRoleMiddleware(["ADMIN"]), AdminController.editRoleUser)


module.exports = router