const Router = require('express')
const TypesController = require('../controllers/typesController')

const router = new Router()

router.get('/', TypesController.getAll)


module.exports = router