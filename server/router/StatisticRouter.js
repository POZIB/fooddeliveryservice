const Router = require('express')
const authMiddleware = require('../middlewares/authMiddleware')
const checkRoleMiddleware = require('../middlewares/checkRoleMiddleware')
const statisticController = require('../controllers/statisticController')

const router = new Router()

router.get('/orders', authMiddleware, checkRoleMiddleware(["ADMIN", "MANAGER"]), statisticController.getOrders)
router.get('/todaysOrders', authMiddleware, checkRoleMiddleware(["ADMIN", "MANAGER"]), statisticController.getTodaysOrders)
router.get('/couriers', authMiddleware, checkRoleMiddleware(["ADMIN", "MANAGER"]), statisticController.getCouriers)
router.get('/courier', authMiddleware, checkRoleMiddleware(["ADMIN", "MANAGER"]), statisticController.getSatisticCourier)
router.get('/todaysCouriers', authMiddleware, checkRoleMiddleware(["ADMIN", "MANAGER"]), statisticController.getSatisticTodaysCouriers)



module.exports = router