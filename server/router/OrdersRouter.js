const Router = require('express')
const authMiddleware = require('../middlewares/authMiddleware')
const OrdersController = require('../controllers/ordersController')

const router = new Router()

router.get('/', authMiddleware, OrdersController.getAll)
router.post('/', authMiddleware, OrdersController.create)

module.exports = router