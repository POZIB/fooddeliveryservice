const Router = require('express')
const { body } = require('express-validator')
const UserController = require('../controllers/userСontroller')
const authMiddleware = require('../middlewares/authMiddleware')

const router = new Router()


router.post('/registration',
   body('email').isEmail().withMessage('Некорректный формат email'),
   body('password').isLength({ min: 4, max: 32 }).withMessage('Минимальная длинна пароля 4 символа'),
   UserController.registration
)

router.post('/login', UserController.login)
router.post('/logout', UserController.logout)
router.get('/activate/:link', UserController.activate)
router.get('/refresh', UserController.refresh)
router.get('/password_reset', UserController.redirectLinkResetPassword)
router.post('/password_reset', UserController.sendLinkResetPassword)
router.put('/password_reset', UserController.resetPassword)
router.put('/data', authMiddleware, UserController.data)
router.put('/password', authMiddleware, UserController.password)



module.exports = router