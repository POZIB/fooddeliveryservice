const Router = require('express')
const authMiddleware = require('../middlewares/authMiddleware')
const BasketController = require('../controllers/basketController')

const router = new Router()

router.get('/', authMiddleware, BasketController.get)
router.post('/', authMiddleware, BasketController.add)
router.delete('/', authMiddleware, BasketController.delete)
router.put('/', authMiddleware, BasketController.editQuantityProduct)
router.post('/clear', authMiddleware, BasketController.clear)

module.exports = router