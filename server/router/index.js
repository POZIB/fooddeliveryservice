const Router = require('express')
const router = new Router()

const adminRouter = require('./Admin/index')
const managerRouter = require('./Manager/index')
const courierRouter = require('./Courier/index')
const userRouter = require('./UserRouter')

const typesRouter = require('./TypesRouter')
const categorisRouter = require('./CategoriesRouter')
const productsRouter = require('./ProductsRouter')
const basketRouter = require('./BasketRouter')
const deliveryAddressRouter = require('./DeliveryAddress')
const ordersRouter = require('./OrdersRouter')
const statisticRouter = require('./StatisticRouter')


router.use('/', userRouter)
router.use('/admin', adminRouter)
router.use('/manager', managerRouter)
router.use('/courier', courierRouter)

router.use('/types', typesRouter)
router.use('/categories', categorisRouter)
router.use('/products', productsRouter)
router.use('/basket', basketRouter)
router.use('/deliveryAddress', deliveryAddressRouter)
router.use('/orders', ordersRouter)

router.use('/statistics', statisticRouter)

module.exports = router

