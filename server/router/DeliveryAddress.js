const Router = require('express')
const authMiddleware = require('../middlewares/authMiddleware')
const parseUserDataBody = require('../middlewares/parseUserDataBody')
const DeliveryAddressController = require('../controllers/deliveryAddressController')

const router = new Router()

router.get('', parseUserDataBody, DeliveryAddressController.getAll)
router.post('', authMiddleware, DeliveryAddressController.create)
router.put('', authMiddleware, DeliveryAddressController.edit)
router.delete('', authMiddleware, DeliveryAddressController.delete)


module.exports = router