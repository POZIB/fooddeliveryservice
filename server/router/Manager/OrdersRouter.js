const Router = require('express')
const authMiddleware = require('../../middlewares/authMiddleware')
const checkRoleMiddleware = require('../../middlewares/checkRoleMiddleware')
const OrdersController = require('../../controllers/managerController')

const router = new Router()

router.get('/', authMiddleware, checkRoleMiddleware(["MANAGER"]), OrdersController.getOrders)
router.post('/changeStatus', authMiddleware, checkRoleMiddleware(["MANAGER"]), OrdersController.changeStatus)

module.exports = router