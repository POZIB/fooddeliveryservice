const Router = require('express')

const ordersRouter = require('./OrdersRouter')

const router = new Router()

router.use('/orders', ordersRouter)

module.exports = router