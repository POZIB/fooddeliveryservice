const Router = require('express')
const authMiddleware = require('../middlewares/authMiddleware')
const ProductsController = require('../controllers/productsController')

const router = new Router()

router.get('/', ProductsController.getAll)
router.get('/:id', ProductsController.getOne)
router.post('/rating', authMiddleware, ProductsController.rating)

module.exports = router