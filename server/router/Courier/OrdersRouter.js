const Router = require('express')
const authMiddleware = require('../../middlewares/authMiddleware')
const checkRoleMiddleware = require('../../middlewares/checkRoleMiddleware')
const CourierController = require('../../controllers/courierController')

const router = new Router()

router.get('/', authMiddleware, checkRoleMiddleware(["COURIER"]), CourierController.getOrders)
router.get('/takenOrder', authMiddleware, checkRoleMiddleware(["COURIER"]), CourierController.getTakenOrder)
router.put('/acceptanceOrder', authMiddleware, checkRoleMiddleware(["COURIER"]), CourierController.acceptanceOrderForDelivery)

module.exports = router