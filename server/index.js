require('dotenv').config()
const express = require('express')
const { Server } = require('socket.io')
const http = require('http')
const cors = require('cors')
const cookieParser = require('cookie-parser')
const router = require('./router/index')
const path = require('path')
const fileUpload = require('express-fileupload')

const sequelize = require('./db')
const parseUserDataMiddlewaerIO = require('./middlewares/parseUserDataMiddlewaerIO')
const errorMiddleware = require('./middlewares/ErrorMiddleware')
const userJoin = require('./sockets/userJoin')
const userOut = require('./sockets/userOut')



const PORT = process.env.PORT || 5000
const app = express()

app.use(express.json())
app.use(cookieParser())
app.use(fileUpload({}))
app.use(express.static(path.resolve(__dirname, 'static')))

if (process.env.NODE_ENV === 'production') {
   app.use(express.static(path.join(__dirname + '/public')))
}

app.use(cors({
   credentials: true,
   origin: process.env.CLIENT_URL
}))
app.use('/api', router)

if (process.env.NODE_ENV === 'production') {
   app.get('/*', (req, res) => {
      res.sendFile(path.resolve(__dirname + '/public/index.html'))
   })
}

app.use(errorMiddleware)

const server = http.createServer(app)

const io = new Server(server, {
   cors: {
      credentials: true,
      origin: process.env.CLIENT_URL,
   },
})


const onConnection = (socket) => { }

io.use(parseUserDataMiddlewaerIO).on('connection', onConnection)

const start = async () => {
   try {
      await sequelize.authenticate().then(() => {
         console.log('Sequelize: Соединение успешно установлено.');
      }).catch(err => {
         console.error('Sequelize: Не удается подключиться к базе данных', err);
      });

      await sequelize.sync()
      server.listen(PORT, () => console.log(`SERVER START ${PORT}`))
   } catch (error) {
      console.log(error);
   }
}

start()

module.exports = { io } 
