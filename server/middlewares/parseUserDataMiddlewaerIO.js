const tokenService = require("../service/tokenService")



module.exports = function (socket, next) {
   try {
      const token = socket.handshake.auth.token?.split(' ')[1]
      if (!token) {
         next()
      }

      const userData = tokenService.validateAccessToken(token)
      socket.data.userData = userData
      next()
   } catch (error) {
      next()
   }
}
