const ApiError = require('../exeptions/apiError')
const ValidationFields = require('../exeptions/validationFields')

module.exports = function (err, req, res, next) {

   console.log(err);

   if (err instanceof ApiError) {
      return res.status(err.status).json({ message: err.message, errors: err.errors })
   }

   if (err instanceof ValidationFields) {
      return res.status(400).json({ message: err.message })
   }

   return res.status(500).json({ message: 'Непредвиденная ошибка', errors: err.errors })

}