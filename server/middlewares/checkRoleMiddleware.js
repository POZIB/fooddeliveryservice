const ApiError = require("../exeptions/apiError")
const tokenService = require("../service/tokenService")


module.exports = function (roles) {
   return function (req, res, next) {

      try {
         const authHeader = req.headers.authorization
         if (!authHeader) {
            return next(ApiError.UnauthorizedError())
         }

         const accessToken = authHeader.split(' ')[1]
         if (!accessToken) {
            return next(ApiError.UnauthorizedError())
         }

         const userData = tokenService.validateAccessToken(accessToken)
         let hasRole = false

         userData.roles?.forEach(role => {
            if (roles.includes(role.value.toUpperCase())) {
               hasRole = true
            }
         })

         if (!hasRole) {
            return next(ApiError.NoAccess())
         }

         req.user = userData
         next()

      } catch (error) {
         return next(ApiError.NoAccess())
      }


   }

}