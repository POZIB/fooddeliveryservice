const tokenService = require("../service/tokenService")

module.exports = function (
   req, res, next
) {
   try {
      const authHeader = req.headers.authorization

      const userDto = tokenService.validateAccessToken(authHeader?.split(' ')[1])


      req.body.user = userDto ? userDto : ({ id: 0 })
      next()
   } catch (error) {
      return next()
   }
}
