const OrdersService = require("../service/orderService")
class OrdersController {

   async getAll(req, res, next) {
      try {

         const user = req.body.user
         let { page, limit } = req.query
         page = page || 1
         limit = limit || 19
         let offset = page * limit - limit


         if (limit < 0) limit = null
         const orders = await OrdersService.getAll(user.id, limit, offset)

         res.json(orders)
      } catch (error) {
         next(error)
      }
   }


   async create(req, res, next) {
      try {
         const user = req.body.user
         const { order } = req.body
         if (!user || !order) {
            next()
         }
         order.userId = user.id
         order.emailUser = user.email

         const result = await OrdersService.create(order)
         const _order = await OrdersService.getOne(result.id)

         require('../index').io.emit('MANAGER:ORDER:NEW', _order)
         res.status(201).json()
      } catch (error) {
         next(error)
      }
   }
}

module.exports = new OrdersController