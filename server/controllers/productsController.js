const ProductsService = require("../service/productsService")
const path = require('path')
const fs = require('fs');

class ProductsController {

   async getAll(req, res, next) {
      try {
         let { categoryId, typeId, limit, page } = req.query
         page = page
         limit = limit

         let offset = (page && limit) && page * limit - limit

         const products = await ProductsService.getAll(categoryId, typeId, limit, offset)
         res.json(products)
      } catch (error) {
         next(error)
      }
   }

   async getOne(req, res, next) {
      try {
         const { id } = req.query
         if (!id) {
            next()
         }
         const product = await ProductsService.getOne(id)
         res.json(product)
      } catch (error) {

      }
   }


   async create(req, res, next) {
      try {
         const { name, price, description, composition, weight, categoryId, typeId } = req.body

         if (!req.body) {
            next()
         }

         let fileName = ''
         if (req.files) {
            const { img } = req.files
            fileName = img.name

            if (!fs.existsSync(path.resolve(__dirname, '..', 'static'))) {
               fs.mkdirSync(path.resolve(__dirname, '..', 'static'));
            }

            img.mv(path.resolve(__dirname, '..', 'static', fileName))
         }


         const product = await ProductsService.create({ name, price, description, composition, weight, img: fileName, categoryId, typeId })
         res.json(product)
      } catch (error) {
         next(error)
      }
   }


   async edit(req, res, next) {
      try {
         const { id, name, price, description, composition, weight, img, categoryId, typeId } = req.body
         if (!req.body) {
            next()
         }

         let fileName = img ?? ''
         if (req.files) {
            const { img } = req.files
            fileName = img.name

            if (fs.existsSync(path.resolve(__dirname, '..', 'static'))) {
               img.mv(path.resolve(__dirname, '..', 'static', fileName))
            }

         }

         const product = await ProductsService.edit({ id, name, price, description, weight, img: fileName, composition, categoryId, typeId })
         res.json(product)
      } catch (error) {
         next(error)
      }
   }

   async delete(req, res, next) {
      try {
         const { id } = req.query
         if (!id) {
            next()
         }

         await ProductsService.delete(id)
         res.status(200).json()
      } catch (error) {
         next(error)
      }
   }

   async rating(req, res, next) {
      try {
         const user = req.body.user
         const { productId, rate } = req.body
         if (!productId || !rate) {
            next()
         }

         const rating = await ProductsService.rating(user.id, productId, rate)

         res.status(201).json(rating)
      } catch (error) {
         next(error)
      }
   }

}

module.exports = new ProductsController