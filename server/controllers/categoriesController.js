const CategoriesService = require("../service/categoriesService")


class CategoriesController {


   async getAll(req, res, next) {
      try {
         let { typeId } = req.query

         const categories = await CategoriesService.getAll(typeId)
         res.json(categories)
      } catch (error) {
         next(error)
      }
   }


   async create(req, res, next) {
      try {
         const { name, typeId } = req.body
         if (!name || !typeId) {
            next()
         }

         const category = await CategoriesService.create(name, typeId)
         res.json(category)
      } catch (error) {
         next(error)
      }
   }


   async edit(req, res, next) {
      try {
         const { newCategory } = req.body
         if (!newCategory) {
            next()
         }

         const category = await CategoriesService.edit(newCategory)
         res.json(category)
      } catch (error) {
         next(error)
      }
   }


   async delete(req, res, next) {
      try {
         const { id } = req.query
         if (!id) {
            next()
         }

         await CategoriesService.delete(id)
         res.status(200).json()
      } catch (error) {
         next(error)
      }
   }
}

module.exports = new CategoriesController