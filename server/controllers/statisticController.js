const StatisticService = require("../service/statisticService")


class StatisticController {


   async getTodaysOrders(req, res, next) {
      try {
         const orders = await StatisticService.getTodaysOrders()
         return res.json(orders)
      } catch (error) {
         next(error)
      }
   }

   async getOrders(req, res, next) {
      try {
         let { dateInterval, dateTrunc } = req.query

         const orders = await StatisticService.getOrders(dateInterval, dateTrunc)
         return res.json(orders)
      } catch (error) {
         next(error)
      }

   }

   async getCouriers(req, res, next) {
      try {

         const couriers = await StatisticService.getCouriers()
         return res.json(couriers)

      } catch (error) {
         next(error)
      }
   }

   async getSatisticCourier(req, res, next) {
      try {
         let { courierId } = req.query

         if (!courierId) {
            next()
         }

         const statistic = await StatisticService.getSatisticCourier(courierId)
         return res.json(statistic)
      } catch (error) {
         next(error)
      }
   }

   async getSatisticTodaysCouriers(req, res, next) {
      try {
         const statistics = await StatisticService.getSatisticTodaysCouriers()
         return res.json(statistics)
      } catch (error) {
         next(error)
      }
   }

}

module.exports = new StatisticController