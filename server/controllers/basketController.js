const BasketService = require("../service/basketService")



class BasketController {

   async get(req, res, next) {
      try {
         const user = req.body.user

         const productsInBasket = await BasketService.get(user.id)
         res.json(productsInBasket)
      } catch (error) {
         next(error)
      }
   }

   async add(req, res, next) {
      try {
         const user = req.body.user
         const { productId } = req.body
         if (!productId) {
            next()
         }

         const product = await BasketService.add(user.id, productId)
         res.json(product)
      } catch (error) {
         next(error)
      }
   }

   async editQuantityProduct(req, res, next) {
      try {
         const user = req.body.user
         const { productId, quantity } = req.body
         if (!productId) {
            next()
         }

         const product = await BasketService.editQuantityProduct(user.id, productId, quantity)
         res.json(product)
      } catch (error) {
         next(error)
      }
   }

   async delete(req, res, next) {
      try {
         const user = req.body.user
         const { productId } = req.query
         if (!productId) {
            next()
         }

         await BasketService.delete(user.id, productId)
         res.status(200).json()
      } catch (error) {
         next(error)
      }
   }


   async clear(req, res, next) {
      try {
         const user = req.body.user

         await BasketService.clear(user.id)
         res.status(200).json()
      } catch (error) {
         next(error)
      }
   }

}

module.exports = new BasketController