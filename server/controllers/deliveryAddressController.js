const DeliveryAddressService = require("../service/deliveryAddressService")

class DeliveryAddressController {

   async getAll(req, res, next) {
      try {
         const user = req.body.user

         const addresses = await DeliveryAddressService.getAll(user.id)
         res.json(addresses)
      } catch (error) {
         next(error)
      }
   }


   async create(req, res, next) {
      try {
         const user = req.body.user
         const { address } = req.body

         const newAddress = await DeliveryAddressService.create(address, user.id)

         res.json(newAddress)
      } catch (error) {
         next(error)
      }
   }


   async edit(req, res, next) {
      try {
         const { info } = req.body

         const address = await DeliveryAddressService.edit(info)

         res.json(address)
      } catch (error) {
         next(error)
      }
   }


   async delete(req, res, next) {
      try {
         const { id } = req.query
         await DeliveryAddressService.delete(id)

         res.status(200).json()
      } catch (error) {
         next(error)
      }
   }
}

module.exports = new DeliveryAddressController