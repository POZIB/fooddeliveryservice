const TypesService = require("../service/typesService")


class TypesController {

   async getAll(req, res, next) {
      try {
         const types = await TypesService.getAll()
         res.json(types)
      } catch (error) {
         next(error)
      }
   }

   async create(req, res, next) {
      try {
         const { name } = req.body
         if (!name) {
            next()
         }

         const type = await TypesService.create(name)
         res.json(type)
      } catch (error) {
         next(error)
      }
   }

   async edit(req, res, next) {
      try {
         const { id, newName } = req.body
         if (!id || !newName) {
            next()
         }
         const type = await TypesService.edit(id, newName)
         res.json(type)
      } catch (error) {
         next(error)
      }
   }


   async delete(req, res, next) {
      try {
         const { id } = req.query
         if (!id) {
            next()
         }
         await TypesService.delete(id)
         res.status(200).json()
      } catch (error) {
         next(error)
      }
   }
}

module.exports = new TypesController