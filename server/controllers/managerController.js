const ManagerService = require("../service/managerService")


class ManagerController {

   async getOrders(req, res, next) {
      try {
         let { emailUser, statuses, dateInterval, page, limit } = req.query
         page = page || 1
         limit = limit || 19
         let offset = page * limit - limit

         if (limit < 0) limit = null

         const orders = await ManagerService.getOrders(emailUser, statuses, dateInterval, limit, offset)

         res.json(orders)
      } catch (error) {
         next(error)
      }
   }

   async changeStatus(req, res, next) {
      try {
         const { id, status } = req.body

         if (!id || !status) {
            next()
         }

         const order = await ManagerService.changeStatusOrder(id, status)

         require('../index').io.emit('MANAGER:ORDER:CHANGE_STATUS', order)

         if (order.status === "Готов для доставки") {
            console.log(order);
            require('../index').io.emit('COURIER:ORDER:NEW', order)
         }

         res.json(order)
      } catch (error) {
         next(error)
      }
   }
}

module.exports = new ManagerController