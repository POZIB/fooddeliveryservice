const bcrypt = require('bcrypt')
const { validationResult } = require('express-validator')
const userService = require('../service/userService')
const ApiError = require('../exeptions/apiError')
const ValidationFields = require('../exeptions/validationFields')
const tokenService = require('../service/tokenService')
const mailService = require('../service/mailService')
const UserDto = require('../dtos/userDto')

class UserController {

   async registration(req, res, next) {
      try {
         const errors = validationResult(req)
         if (!errors.isEmpty()) {
            return next(ValidationFields.BadRequest(errors.array()))
         }

         const { email, password } = req.body
         if (!email || !password) {
            return next()
         }

         const userData = await userService.registration(email, password)
         res.cookie('refreshToken', userData.refreshToken, { maxAge: 30 * 24 * 60 * 60 * 1000, httpOnly: true })

         return res.json(userData)
      } catch (error) {
         next(error)
      }
   }

   async login(req, res, next) {
      try {
         const { email, password } = req.body
         if (!email || !password) {
            return next()
         }

         const userData = await userService.login(email, password)
         res.cookie('refreshToken', userData.refreshToken, { maxAge: 30 * 24 * 60 * 60 * 1000, httpOnly: true })

         return res.json(userData)
      } catch (error) {
         next(error)
      }
   }

   async logout(req, res, next) {
      try {
         const { refreshToken } = req.cookies
         const token = await userService.logout(refreshToken)
         res.clearCookie('refreshToken')

         return res.status(200).json(token)
      } catch (error) {
         next(error)
      }
   }

   async activate(req, res, next) {
      try {
         const activationLink = req.params.link

         await userService.activate(activationLink)

         return res.redirect(process.env.CLIENT_URL)
      } catch (error) {
         next(error)
      }
   }

   async refresh(req, res, next) {
      try {
         const { refreshToken } = req.cookies
         const userData = await userService.refresh(refreshToken)

         res.cookie('refreshToken', userData.refreshToken, { maxAge: 30 * 24 * 60 * 60 * 1000, httpOnly: true })

         return res.json(userData)
      } catch (error) {
         next(error);
      }
   }

   async sendLinkResetPassword(req, res, next) {
      try {
         const { email } = req.body
         if (!email) {
            next()
         }
         const user = await userService.findByEmail(email)

         if (user) {
            const link = tokenService.generateAccess(
               { email: user.email },
               '5m'
            )
            await mailService.sendLinkResetPassword(user.email, link)
         }

         res.json(!!user)
      } catch (error) {
         next(error)
      }
   }

   async redirectLinkResetPassword(req, res, next) {
      try {
         const link = req.query.link
         if (!link) {
            next()
         }

         const result = tokenService.validateAccessToken(link)
         res.redirect(
            `${process.env.CLIENT_URL}/auth/password_reset/confirmed?email=${result?.email || ''
            }&hash=${result ? link : 'expired'}`
         )
      } catch (error) {
         next(error)
      }
   }

   async resetPassword(req, res, next) {
      try {
         const { hash, password, password2 } = req.body
         if (!hash || password !== password2) {
            next()
         }

         const user = tokenService.validateAccessToken(hash)

         if (!user) {
            next(ApiError.BadRequest('Время изменения пароля истекло. Повторите попытку'))
         } else {
            const newPassword = await bcrypt.hash(password, 3)
            const result = await userService.resetPassword(user.email, newPassword)

            res.json(!!result)
         }
      } catch (error) {
         next(error)
      }
   }

   async data(req, res, next) {
      try {
         const user = req.body.user
         const { payload } = req.body
         if (!payload) {
            next()
         }
         const newData = await userService.data(user.id, payload)
         const userDto = new UserDto(newData)

         res.json(userDto)
      } catch (error) {
         next(error)
      }
   }

   async password(req, res, next) {
      try {
         const user = req.body.user
         const { payload } = req.body
         if (!payload || !payload.old) {
            next()
         }

         if (payload.new !== payload.confirm) {
            throw ApiError.BadRequest('Пароли не совпадают!')
         }

         const result = await userService.password(user.id, payload.old, payload.new)

         res.json(!!result)
      } catch (error) {
         next(error)
      }
   }

}

module.exports = new UserController