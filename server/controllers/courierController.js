const CourierService = require("../service/courierService")


class CourierController {

   async getOrders(req, res, next) {
      try {
         const user = req.body.user
         let { page, limit } = req.query
         page = page || 1
         limit = limit || 19
         let offset = page * limit - limit

         if (limit < 0) limit = null

         const orders = await CourierService.getOrders(user.id, limit, offset)
         res.json(orders)
      } catch (error) {
         next(error)
      }
   }

   async getTakenOrder(req, res, next) {
      try {
         const user = req.body.user

         const order = await CourierService.getTakenOrder(user.id)

         res.json(order)
      } catch (error) {
         next(error)
      }
   }

   async acceptanceOrderForDelivery(req, res, next) {
      try {
         const user = req.body.user
         const { id, status } = req.body

         if (!id || !status) {
            next()
         }

         const order = await CourierService.acceptanceOrderForDelivery(id, user.id, status)

         require('../index').io.emit('COURIER:ORDER:CHANGE_STATUS', order)
         require('../index').io.emit('MANAGER:ORDER:CHANGE_STATUS', order)

         res.json(order)
      } catch (error) {
         next(error)
      }
   }
}

module.exports = new CourierController