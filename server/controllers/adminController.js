const AdminService = require("../service/adminService")


class AdminController {

   async getRoles(req, res, next) {
      try {
         const roles = await AdminService.getRoles()

         res.json(roles)
      } catch (error) {
         next(error)
      }
   }


   async createRole(req, res, next) {
      try {
         const { role } = req.body
         if (!role) {
            next()
         }

         const newRole = await AdminService.createRole(role)
         res.json(newRole)
      } catch (error) {
         next(error)
      }
   }

   async deleteRole(req, res, next) {
      try {
         const { role } = req.query
         if (!role) {
            next()
         }
         await AdminService.deleteRole(role)
         res.status(200).json()
      } catch (error) {
         next(error)
      }
   }


   async editRoleUser(req, res, next) {
      try {
         const { email, roles } = req.body
         if (!email || !roles) {
            next()
         }

         const _roles = await AdminService.editRoleUser(email, roles)
         res.json(_roles)
      } catch (error) {
         next(error)
      }
   }



   async getUsers(req, res, next) {
      try {
         let { page, limit, role } = req.query

         page = page || 1
         limit = limit || 19
         let offset = page * limit - limit

         if (limit < 0) limit = null


         const users = await AdminService.getAllUsers(limit, offset, role)
         res.json(users)
      } catch (error) {
         next(error)
      }
   }



}


module.exports = new AdminController