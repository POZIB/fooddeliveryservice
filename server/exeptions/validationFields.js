module.exports = class ValidationFields extends Error {
   message

   constructor(message) {
      super(message)
      this.message = message
   }


   static BadRequest(errors = []) {
      return new ValidationFields(errors[0].msg)
   }




}