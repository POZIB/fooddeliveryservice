module.exports = class UserDto {
   id
   email
   isActivated
   roles
   firstName
   lastName


   constructor(model) {
      this.email = model.email
      this.id = model.id
      this.isActivated = model.isActivated
      this.roles = model.roles
      this.firstName = model.firstName
      this.lastName = model.lastName
   }

}