const { Categories, Types } = require("../models/models")
const ApiError = require("../exeptions/apiError")

class CategoriesService {


   async getAll(typeId) {
      if (typeId) {
         return await Categories.findAll({
            where: { typeId },
            attributes: { exclude: ['createdAt', 'updatedAt'] },
            order: [["id", "ASC"]]
         })
      }
      return await Categories.findAll({
         attributes: { exclude: ['createdAt', 'updatedAt'] },
         order: [["id", "ASC"]]
      })
   }


   async create(name, typeId) {
      const category = await Categories.findOne({ where: { name } })
      if (category) {
         throw ApiError.BadRequest('Такая категория уже есть!')
      }

      const newCategory = await Categories.create({ name, typeId })
      return newCategory.dataValues
   }


   async edit(newCategory) {
      const category = await Categories.findByPk(newCategory.id)
      if (!category) {
         throw ApiError.BadRequest('Такой категории нет!')
      }

      const updateCategory = await category.update({ name: newCategory.name, typeId: newCategory.typeId })
      return updateCategory.dataValues
   }


   async delete(id) {
      const category = await Categories.findByPk(id)
      if (!category) {
         throw ApiError.BadRequest('Такой категории нет!')
      }

      await category.destroy()
      return
   }
}

module.exports = new CategoriesService