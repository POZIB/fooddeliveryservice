const ApiError = require("../exeptions/apiError")
const sequelize = require('sequelize')
const { Basket, Products, BasketDetails } = require("../models/models")


class BasketService {

   async get(userId) {
      let basket = await Basket.findOne({ where: { userId } })

      if (!basket) {
         basket = await Basket.create({ userId })
      }

      const products = await basket.getProducts({
         attributes: ['id', 'name', 'price', 'description', 'composition', 'weight', 'img',
            'categoryId', 'typeId', 'basketDetails.quantity', [sequelize.literal('"basketDetails"."id"'), "idDetails"]],
         raw: true,
         joinTableAttributes: [],
         order: [["id", "ASC"]]
      })

      return products
   }


   async add(userId, productId) {
      const basket = await Basket.findOne({ where: { userId } })

      if (!basket) {
         basket = await Basket.create({ userId })
      }

      const basketProduct = await basket.getProducts({ where: { id: productId } })

      if (basketProduct[0]) {
         const quantity = basketProduct[0].basketDetails.quantity + 1
         await basketProduct[0].basketDetails.update({ quantity: quantity })
         basketProduct[0].dataValues.quantity = quantity
         return basketProduct[0].dataValues
      } else {
         const product = await Products.findByPk(productId)
         await basket.addProduct(product)
         return product.dataValues
      }

   }

   async editQuantityProduct(userId, productId, quantity) {
      const basket = await Basket.findOne({ where: { userId } })
      if (!basket) {
         basket = await Basket.create({ userId })
      }

      const basketProduct = await basket.getProducts({ where: { id: productId } })
      basketProduct[0].dataValues.quantity = quantity

      if (quantity == 0) {
         await this.delete(userId, productId)
      } else {
         await basketProduct[0].basketDetails.update({ quantity: quantity })
      }

      return basketProduct[0].dataValues
   }

   async delete(userId, productId) {
      const basket = await Basket.findOne({ where: { userId } })
      const product = await Products.findByPk(productId)

      if (!basket) {
         throw ApiError.BadRequest('Ошибка корзины!')
      }

      await basket.removeProduct(product)

      return
   }

   async clear(userId) {
      let basket = await Basket.findOne({ where: { userId } })
      if (!basket) {
         throw ApiError.BadRequest('Ошибка корзины!')
      }
      await basket.setProducts([])

      return
   }
}

module.exports = new BasketService