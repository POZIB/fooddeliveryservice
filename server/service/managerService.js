const moment = require('moment')
const sequelize = require('sequelize')
const { Op } = require("sequelize");
const { Orders, Products, Users } = require("../models/models");
const ApiError = require('../exeptions/apiError');

class ManagerService {

   async getOrders(emailUser, statuses, date, limit, offset) {
      const _user = emailUser ? { email: { [Op.like]: `${emailUser}%` } } : ''
      const _statuses = statuses ? { status: statuses } : ''
      const _date = date && date[0] && date[1] ? {
         createdAt: {
            [Op.gt]: moment(date[0]).format('YYYY-MM-DD'),
            [Op.lt]: moment(date[1]).format('YYYY-MM-DD')
         }
      } : ''

      const orders = await Orders.findAndCountAll({
         limit,
         offset,
         distinct: true,
         order: [["id", "DESC"]],
         where: {
            ..._statuses,
            ..._date
         },
         attributes: {
            include: [['createdAt', 'date'], [sequelize.literal('"user"."email"'), "emailUser"]], exclude: ['createdAt', 'updatedAt']
         },
         include: [{
            model: Products,
            attributes: ['id', 'name', 'img', 'price', 'description', 'composition',
               [sequelize.literal('"products->orderDetails"."quantity"'), "quantity"]
            ],
            through: { attributes: [] }
         },
         {
            model: Users,
            where: { ..._user },
         }
         ]
      })


      return orders
   }


   async changeStatusOrder(id, status) {
      const order = await Orders.findByPk(id, {
         attributes: {
            include: [['createdAt', 'date'], [sequelize.literal('"user"."email"'), "emailUser"]], exclude: ['createdAt', 'updatedAt']
         },
         include: [{
            model: Products,
            attributes: ['id', 'name', 'img', 'price', 'description', 'composition',
               [sequelize.literal('"products->orderDetails"."quantity"'), "quantity"]
            ],
            through: { attributes: [] }
         },
         { model: Users }
         ]
      })

      if (order) {
         await order.update({ status: status })
      } else {
         throw ApiError.BadRequest('Такого заказа нет!')
      }

      return order
   }
}

module.exports = new ManagerService