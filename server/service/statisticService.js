const moment = require('moment')
const sequelize = require('sequelize')
const { Op } = require("sequelize");
const ApiError = require("../exeptions/apiError")
const { Orders, Users, Roles } = require("../models/models")


class StatisticService {


   async getTodaysOrders() {
      const order = await Orders.findOne({
         where: {
            date: {
               [Op.gt]: moment().startOf('day')
            }
         },
         group: [sequelize.fn('date_trunc', 'day', sequelize.col('date'))],
         attributes: [
            [sequelize.literal('COUNT(*)'), 'count'],
            [sequelize.fn('sum', sequelize.col('total')), 'total'],
         ]
      })

      return order
   }

   async getOrders(dateInterval, dateTrunc) {
      const _dateTrunc = dateTrunc || 'day'
      const _dateInterval = dateInterval && dateInterval[0] && dateInterval[1] ? {
         createdAt: {
            [Op.gt]: moment(dateInterval[0]).format('YYYY-MM-DD'),
            [Op.lt]: moment(dateInterval[1]).format('YYYY-MM-DD')
         }
      } : {
         createdAt: {
            [Op.gt]: moment().subtract(17, 'd').format('YYYY-MM-DD'),
            [Op.lt]: moment().format('YYYY-MM-DD')
         }
      }

      const orders = await Orders.findAll({
         where: {
            ..._dateInterval
         },
         order: [[sequelize.fn('date_trunc', _dateTrunc, sequelize.col('date')), 'DESC']],
         group: [sequelize.fn('date_trunc', _dateTrunc, sequelize.col('date'))],
         attributes: [
            [sequelize.literal('COUNT(*)'), 'count'],
            [sequelize.fn('sum', sequelize.col('total')), 'total'],
            [sequelize.fn('to_char', sequelize.fn('date_trunc', _dateTrunc, sequelize.col('date')), 'dd-mm-yyyy'), 'date'],
         ],

      })

      return orders
   }


   async getCouriers() {
      const coriers = await Users.findAll({
         attributes: ['id', 'firstName', 'lastName', 'email'],
         include: [{
            model: Roles,
            where: { value: 'COURIER' },
            attributes: [],
            through: { attributes: [] },
         }],
      })

      return coriers
   }


   async getSatisticCourier(courierId) {

      const statistic = await Orders.findAll({
         where: {
            status: 'Доставленный',
            courierId: courierId
         },
         order: [[sequelize.fn('date_trunc', 'day', sequelize.col('date')), 'DESC']],
         group: [sequelize.fn('date_trunc', 'day', sequelize.col('date'))],
         attributes: [
            [sequelize.literal('COUNT(*)'), 'ordersCount'],
            [sequelize.fn('to_char', sequelize.fn('date_trunc', 'day', sequelize.col('date')), 'dd-mm-yyyy'), 'date'],
         ],
      })

      return statistic
   }


   async getSatisticTodaysCouriers() {

      const statisticts = await Orders.findAll({
         where: {
            status: 'Доставленный',
            date: {
               [Op.gt]: moment().startOf('day')
            }
         },
         group: 'courierName',
         attributes: [
            ['courierName', 'name'],
            [sequelize.cast(sequelize.fn('COUNT', '*'), 'INTEGER'), 'ordersCount']
         ]
      })

      return statisticts
   }



}

module.exports = new StatisticService