const sequelize = require('sequelize')
const { Op } = require('sequelize')
const { Orders, Products, Users } = require("../models/models");
const ApiError = require('../exeptions/apiError');

class CourierService {

   async getOrders(courierId, limit, offset) {
      const _user = ''
      const atr = {
         [Op.or]: [{ status: ['Готов для доставки'] }, { [Op.and]: [{ courierId: courierId }, { status: 'В доставке' }] }]
      }

      const orders = await Orders.findAndCountAll({
         limit,
         offset,
         distinct: true,
         order: [["id", "DESC"]],
         where: {
            ...atr
         },
         attributes: {
            include: [['createdAt', 'date'], [sequelize.literal('"user"."email"'), "emailUser"]], exclude: ['createdAt', 'updatedAt']
         },
         include: [{
            model: Products,
            attributes: ['id', 'name', 'img', 'price', 'description', 'composition',
               [sequelize.literal('"products->orderDetails"."quantity"'), "quantity"]
            ],
            through: { attributes: [] }
         },
         {
            model: Users,
            where: { ..._user },
            attributes: []
         }
         ]
      })

      return orders
   }

   async getTakenOrder(courierId) {
      const _user = ''

      const orders = await Orders.findOne({
         distinct: true,
         where: {
            courierId: courierId,
            status: "В доставке"
         },
         attributes: {
            include: [['createdAt', 'date'], [sequelize.literal('"user"."email"'), "emailUser"]], exclude: ['createdAt', 'updatedAt']
         },
         include: [{
            model: Products,
            attributes: ['id', 'name', 'img', 'price', 'description', 'composition',
               [sequelize.literal('"products->orderDetails"."quantity"'), "quantity"]
            ],
            through: { attributes: [] }
         },
         {
            model: Users,
            where: { ..._user },
            attributes: []
         }
         ]
      })

      return orders
   }


   async acceptanceOrderForDelivery(id, courierId, status) {
      const courier = await Users.findByPk(courierId)

      const order = await Orders.findByPk(id, {
         attributes: {
            include: [['createdAt', 'date'], [sequelize.literal('"user"."email"'), "emailUser"]], exclude: ['createdAt', 'updatedAt']
         },
         include: [{
            model: Products,
            attributes: ['id', 'name', 'img', 'price', 'description', 'composition',
               [sequelize.literal('"products->orderDetails"."quantity"'), "quantity"]
            ],
            through: { attributes: [] }
         }, {
            model: Users,
            attributes: []
         }]
      })

      if (order) {
         await order.update({ status: status, courierId: courierId, courierName: `${courier.firstName} ${courier.lastName}` })
      } else {
         throw ApiError.BadRequest('Такого заказа нет!')
      }

      return order
   }
}

module.exports = new CourierService