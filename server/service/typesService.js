const { Types } = require("../models/models")
const ApiError = require("../exeptions/apiError")

class TypesService {


   async getAll() {
      return await Types.findAll({
         attributes: { exclude: ['createdAt', 'updatedAt'] }
      })
   }

   async create(name) {
      const type = await Types.findOne({ where: { name } })
      if (type) {
         throw ApiError.BadRequest('Такой тип уже есть!')
      }
      const newType = await Types.create({ name })
      return newType.dataValues
   }


   async edit(id, newName) {
      const type = await Types.findByPk(id)
      if (!type) {
         throw ApiError.BadRequest('Такого типа нет!')
      }

      type.name = newName
      await type.save()
      return type
   }


   async delete(id) {
      const type = await Types.findByPk(id)
      if (!type) {
         throw ApiError.BadRequest('Такого типа нет!')
      }

      await type.destroy()
      return
   }

}

module.exports = new TypesService


