const nodemailer = require('nodemailer')
const ApiError = require("../exeptions/apiError")

class MailService {
   constructor() {
      this.transporter = nodemailer.createTransport({
         host: process.env.SMTP_HOST,
         port: process.env.SMTP_PORT,
         secure: false,
         auth: {
            user: process.env.SMTP_USER,
            pass: process.env.SMTP_PASSWORD
         }
      })
   }

   async sendActivationMail(to, link) {
      try {
         await this.transporter.sendMail({
            from: process.env.SMTP_USER,
            to,
            subject: "Faster'S. Активация аккаунта моей доставки еды",
            html: `<div>Для активации аккаунта перейдите по ссылке <a href=${link}>${link}</a><div>`,
         })
      } catch (error) {
         throw ApiError.BadRequest('Проблемы с почтой!')
      }
   }


   async sendOrderIsAccepted(order) {
      if (!order) return

      let sms = `<div>Заказ №${order.id} `
      sms += `<div>Доставка ${order.addressDelivery} ${order.whereDelivery}  ${order.timeDelivery}</div>`
      sms += `<div>Телефон для связи: ${order.telephone}</div>`

      if (order.comments)
         sms += `<div>Комментарий к заказу: ${order.comments}</div>`

      for (let product of order.productsInBasket) {
         sms += `<div>${product.quantity} x ${product.name} ${product.quantity * product.price}р</div>`
      }
      sms += `<div>Приборов:  ${order.quantityCutlery}шт</div>`
      sms += `<div>Итоговая сумма заказа ${order.total}р</div></div>`

      try {
         await this.transporter.sendMail({
            from: process.env.SMTP_USER,
            to: order.emailUser,
            subject: "Faster'S. Ваш заказ принят в обработку",
            html: sms

         })
      } catch (error) {
         throw ApiError.BadRequest('Проблемы с почтой!')
      }
   }

   async sendLinkResetPassword(toEmail, link) {
      try {
         await this.transporter.sendMail({
            from: process.env.SMTP_USER,
            to: toEmail,
            subject: "Faster'S. Запрос на сброс пароля",
            html: `<div>Если вы запросили сброс пароля для ${toEmail}, перейдите по <a href=${process.env.API_URL}/password_reset?link=${link}>ссылке</a>, чтобы завершить процесс.
             Если вы не отправляли этот запрос, проигнорируйте это письмо.<div>`,
         })
      } catch (error) {
         throw ApiError.BadRequest('Неудачная отправка сообщения сброса пароля на почту')
      }
   }


}

module.exports = new MailService