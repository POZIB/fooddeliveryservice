const sequelize = require('sequelize')
const mailService = require('./mailService')
const { Orders, OrderDetails, Products, Users } = require("../models/models")
const ApiError = require("../exeptions/apiError")
const BasketService = require("../service/basketService")


class OrdersService {
   async getAll(userId, limit, offset) {
      if (!userId) {
         throw ApiError.BadRequest('Какая то ошибка с заказами')
      }

      const orders = await Orders.findAndCountAll({
         limit,
         offset,
         distinct: true,
         order: [["id", "DESC"]],
         where: { userId },
         attributes: { include: [['createdAt', 'date']], exclude: ['createdAt', 'updatedAt'] },
         include: [{
            model: Products,
            attributes: ['id', 'name', 'img', 'price', 'description', 'composition',
               [sequelize.literal('"products->orderDetails"."quantity"'), "quantity"]
            ],
            through: { attributes: [] }
         }]
      })

      return orders
   }


   async getOne(id) {
      const order = await Orders.findOne({
         where: { id },
         attributes: { include: [['createdAt', 'date']], exclude: ['createdAt', 'updatedAt'] },
         include: [{
            model: Products,
            attributes: ['id', 'name', 'img', 'price', 'description', 'composition',
               [sequelize.literal('"products->orderDetails"."quantity"'), "quantity"]
            ],
            through: { attributes: [] }
         },
         { model: Users }
         ]
      })

      return order.dataValues
   }

   async create(order) {
      const productsInBasket = await BasketService.get(order.userId)
      order.productsInBasket = productsInBasket
      order.total = productsInBasket.reduce((acc, product) => acc + (product.price * product.quantity), 0)

      const newOrder = await Orders.create(order)
      order.id = newOrder.id

      for (let product of productsInBasket) {
         await OrderDetails.create({ orderId: newOrder.id, productId: product.id, quantity: product.quantity })
      }

      await mailService.sendOrderIsAccepted(order)

      return newOrder.dataValues
   }
}

module.exports = new OrdersService