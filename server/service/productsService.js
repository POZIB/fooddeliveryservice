const { Products, Rating } = require("../models/models")
const ApiError = require("../exeptions/apiError")

class ProductsService {


   async getAll(categoryId, typeId, limit, offset) {
      let products
      if (!categoryId && !typeId) {
         products = await Products.findAndCountAll({
            limit, offset, order: [["id", "DESC"]], attributes: { exclude: ['createdAt', 'updatedAt'] }
         })
      }
      if (categoryId && !typeId) {
         products = await Products.findAndCountAll({
            where: { categoryId }, limit, offset, order: [["id", "DESC"]], attributes: { exclude: ['createdAt', 'updatedAt'] }
         })
      }
      if (!categoryId && typeId) {
         products = await Products.findAndCountAll({
            where: { typeId }, limit, offset, order: [["id", "DESC"]], attributes: { exclude: ['createdAt', 'updatedAt'] }
         })
      }
      if (categoryId && typeId) {
         products = await Products.findAndCountAll({
            where: { categoryId, typeId }, limit, offset, order: [["id", "DESC"]], attributes: { exclude: ['createdAt', 'updatedAt'] }
         })
      }
      return products
   }

   async getOne(id) {
      return await Products.findByPk(id, { attributes: { exclude: ['createdAt', 'updatedAt'] } })
   }


   async create(product) {
      const prod = await Products.findOne({ where: { name: product.name } })
      if (prod) {
         throw ApiError.BadRequest('Такой продукт уже есть')
      }

      const newProduct = await Products.create(product)
      return newProduct.dataValues
   }


   async edit(product) {
      const _product = await Products.findByPk(product.id)
      if (!_product) {
         throw ApiError.BadRequest('Такого продукта нет!')
      }
      const updateProduct = await _product.update(product)
      return updateProduct.dataValues
   }

   async delete(id) {
      const product = await Products.findByPk(id)
      if (!product) {
         throw ApiError.BadRequest('Такого продукта нет!')
      }

      await product.destroy()
      return
   }

   async rating(userId, productId, rate) {
      const rating = await Rating.findOne({ where: { userId, productId } })
      if (rating) {
         return
      }

      await Rating.create({ userId, productId, rate })

      const allRatings = await Rating.findAll({ where: { productId } })

      const averageRating = allRatings.reduce((acc, r) => acc + r.rate, 0) / allRatings.length
      if (averageRating) {
         const product = await this.getOne(productId)
         await product.update({ rating: averageRating })
      }
      return averageRating
   }

}


module.exports = new ProductsService