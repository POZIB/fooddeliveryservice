const sequelize = require('sequelize')
const { Op } = require("sequelize");
const ApiError = require("../exeptions/apiError")
const { Roles, Users } = require("../models/models")


class AdminService {
   async getRoles() {
      const roles = await Roles.findAll({ attributes: ['value'] })
      const str_roles = roles.map(role => role.value)
      return str_roles
   }

   async createRole(role) {
      role.toUpperCase()
      const candidate = await Roles.findOne({ where: { value: role } })

      if (candidate) {
         throw ApiError.BadRequest('Уже есть такая роль')
      }

      const newRole = await Roles.create({ value: role })
      return newRole.getDataValue('value')
   }

   async deleteRole(role) {
      role.toUpperCase()
      const candidate = await Roles.findOne({ where: { value: role } })

      if (!candidate) {
         throw ApiError.BadRequest('Такой роли нет!')
      }

      await Roles.destroy({ where: { value: role } })
      return
   }

   async editRoleUser(email, roles) {
      const user = await Users.findOne({ where: { email } })
      if (!user) {
         throw ApiError.BadRequest('Такого пользователя нет!')
      }
      await user.setRoles([])

      for (let role of roles) {
         const _role = await Roles.findOne({ where: { value: role.value } })
         await user.addRole(_role)
      }
      await user.save()

      return roles
   }


   async getAllUsers(limit, offset, role) {

      const users = await Users.findAndCountAll({
         limit,
         offset,
         order: [["id", "DESC"]],
         attributes: {
            exclude: ['createdAt', 'updatedAt']
         },
         include: [{
            model: Roles,
            where: { value: role },
            attributes: {
               exclude: ['id', 'createdAt', 'updatedAt']
            },
            through: { attributes: [] },
         }],
      })

      return users
   }


}


module.exports = new AdminService



