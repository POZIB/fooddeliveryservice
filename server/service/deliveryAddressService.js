const { DeliveryAddress } = require("../models/models")
const ApiError = require("../exeptions/apiError")

class DeliveryAddressService {

   async getAll(userId) {
      if (!userId) {
         throw ApiError.BadRequest('Какая то ошибка с адресами!')
      }

      return await DeliveryAddress.findAll({
         where: { userId },
         order: [['id', 'DESC']],
         attributes: { exclude: ['createdAt', 'updatedAt'] },
         group: "id"
      })
   }


   async create(address, userId) {
      const newAddress = await DeliveryAddress.create({ address, userId })
      return newAddress.dataValues
   }


   async edit(data) {
      const address = await DeliveryAddress.findByPk(data.id)
      if (!address) {
         throw ApiError.BadRequest('Такого адреса нет!')
      }

      const updateAddress = await address.update(data)
      return updateAddress.dataValues
   }

   async delete(id) {
      const address = await DeliveryAddress.findByPk(id)
      if (!address) {
         throw ApiError.BadRequest('Такого адреса нет!')
      }

      await address.destroy()
      return
   }
}

module.exports = new DeliveryAddressService