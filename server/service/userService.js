const bcrypt = require('bcrypt')
const uuid = require('uuid')
const mailService = require('./mailService')
const tokenService = require('./tokenService')
const UserDto = require('../dtos/userDto')
const { Users, Roles } = require('../models/models')
const ApiError = require('../exeptions/apiError')


class UserService {
   async registration(email, password) {
      const candidate = await Users.findOne({ where: { email } })

      if (candidate) {
         throw ApiError.BadRequest(`Пользователь с таким email уже существует`)
      }

      const hashPassword = await bcrypt.hash(password, 3)
      const activationLink = uuid.v4()

      const user = await Users.create({ email, password: hashPassword, activationLink })
      const [role, created] = await Roles.findOrCreate({ where: { value: "CLIENT" }, defaults: { value: "CLIENT" } })
      await user.addRole(role)

      await mailService.sendActivationMail(email, `${process.env.API_URL}/activate/${activationLink}`)

      const userDto = new UserDto(user)
      const tokens = tokenService.generateTokens({ ...userDto })
      await tokenService.saveToken(userDto.id, tokens.refreshToken)

      return { ...tokens, user: userDto }
   }


   async activate(activationLink) {
      const user = await Users.findOne({ where: { activationLink } })
      if (!user) {
         throw ApiError.BadRequest('Неккоректная ссылка активации')
      }
      user.isActivated = true
      await user.save()
   }


   async login(email, password) {
      const user = await Users.findOne({
         where: { email },
         include: [{
            model: Roles,
            attributes: {
               exclude: ['id', 'createdAt', 'updatedAt']
            },
            through: { attributes: [] },
         }],
      })

      if (!user) {
         throw ApiError.BadRequest(`Пользователь с таким email не найден`)
      }
      const isPassEquals = await bcrypt.compare(password, user.password)
      if (!isPassEquals) {
         throw ApiError.BadRequest(`Логин или пароль не верны`)
      }

      const userDto = new UserDto(user)
      const tokens = tokenService.generateTokens({ ...userDto })

      await tokenService.saveToken(userDto.id, tokens.refreshToken)

      return { ...tokens, user: userDto }
   }


   async logout(refreshToken) {
      const token = await tokenService.removeToken(refreshToken)
      return token
   }



   async refresh(refreshToken) {
      if (!refreshToken) {
         throw ApiError.UnauthorizedError()
      }

      const userData = tokenService.validateRefreshToken(refreshToken)
      const tokenFromDb = await tokenService.findToken(refreshToken)

      if (!userData || !tokenFromDb) {
         throw ApiError.UnauthorizedError()
      }

      const user = await Users.findByPk(userData.id, {
         include: [{
            model: Roles,
            attributes: {
               exclude: ['id', 'createdAt', 'updatedAt']
            },
            through: { attributes: [] },
         }],
      })

      const userDto = new UserDto(user)
      const tokens = tokenService.generateTokens({ ...userDto })

      await tokenService.saveToken(userDto.id, tokens.refreshToken)

      return { ...tokens, user: userDto }
   }

   async findByEmail(email) {
      const user = await Users.findOne({ where: { email } })
      return user.dataValues
   }

   async resetPassword(email, password) {
      const user = await Users.update({ password: password }, { where: { email: email } })

      return user
   }

   async data(id, payload) {
      const user = await Users.update({ ...payload },
         {
            where: { id },
            returning: true,
            plain: true
         })

      return user[1]
   }

   async password(id, oldPassword, newPassword) {
      const user = await Users.findByPk(id)

      if (!user) {
         throw ApiError.BadRequest('Пользователь не найден!')
      }

      const isPassEquals = await bcrypt.compare(oldPassword, user.password)

      if (!isPassEquals) {
         throw ApiError.BadRequest('Неверный текущий пароль!')
      }

      const newPass = await bcrypt.hash(newPassword, 3)

      user.password = newPass
      await user.save()

      return user
   }


}

module.exports = new UserService